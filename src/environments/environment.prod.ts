export const environment = {
  production: true,
  apiUrl: 'https://app.pubnews.com.br/',
  apiUrlArquivo: 'https://app.pubnews.com.br/uploads/',
  storagePatch: 'file:///storage/emulated/0/Pubnews/',
  AndroidDataPatch: 'file:///storage/emulated/0/Android/data/com.masmidia.myionicfacebook/files/Pubnews/',
  storage: 'file:///storage/emulated/0/'
};
