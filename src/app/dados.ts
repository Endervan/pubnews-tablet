export interface Dados {
}

//
export interface Parceiro {
    nome: string;
    email: string;
    cpf: string;
    celular: string;
    foto: string;
    tipo: string;
}


export interface Anuncios {
    token: number;
    arquivo: string;
    distancia: number;
    url_arquivo: string;

}

