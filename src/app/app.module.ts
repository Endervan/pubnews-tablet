import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy, Platform} from '@ionic/angular';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {Facebook} from '@ionic-native/facebook/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';
import {IonicSelectableModule} from 'ionic-selectable';
// Font Awesome
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {faDonate, faPlayCircle, fas, faSignOutAlt, faUser, faUserEdit} from '@fortawesome/free-solid-svg-icons';
import {Downloader} from '@ionic-native/downloader/ngx';
import {VideoPlayer} from '@ionic-native/video-player/ngx';

import {ScreenOrientation} from '@ionic-native/screen-orientation/ngx';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import {AndroidPermissions} from '@ionic-native/android-permissions/ngx';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {BackgroundMode} from '@ionic-native/background-mode/ngx';
import {Insomnia} from '@ionic-native/insomnia/ngx';
import {File} from '@ionic-native/file/ngx';


library.add(fas);
library.add(faDonate);
library.add(faPlayCircle);
library.add(faUser);
library.add(faUserEdit);
library.add(faSignOutAlt);

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [BrowserModule, IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        IonicSelectableModule,
        FontAwesomeModule,
    ],
    providers: [
        StatusBar,
        BackgroundMode,
        Insomnia,
        Platform,
        Facebook,
        File,
        GooglePlus,
        HttpClientModule,
        Downloader,
        VideoPlayer,
        ScreenOrientation,
        WebView,
        AndroidPermissions,
        Geolocation,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
