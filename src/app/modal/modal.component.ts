import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {WebView} from '@ionic-native/ionic-webview/ngx';

@Component({
    selector: 'app-modal',
    template: `
        <ion-content fullscreen  style="--background: transparent">
            <ion-col class="ion-no-padding" size="12" size-md="12" size-xs="12" size-lg="12">
                <ion-img id="modal_image" [src]="this.pathForImage(fileImage)"></ion-img>
            </ion-col>
        </ion-content>
    `,
})
export class ModalComponent implements OnInit {
    @Input() fileImage: any;
    @Input() time: number;

    constructor(public modalCtrl: ModalController, public webview: WebView) {

    }

    // resolve caminho images dispositivo
    pathForImage(img) {
        if ( img === null ) {
            return '';
        } else {
            // return this.webview.convertFileSrc(img);
            // tava assim antes
            return this.webview.convertFileSrc(img);
        }
    }

    async delay(ms: number) {
        return new Promise(async resolve => setTimeout(resolve, ms));
    }

    ngOnInit() {
        this.delay(this.time).then(async () => {
            await this.dismiss();
        });
    }

    async dismiss() {
        await this.modalCtrl.dismiss({
            dismissed: true,
        });
    }


}
