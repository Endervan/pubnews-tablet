import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {UtilService} from './services/util/util.service';

const routes: Routes = [
  { path: '', redirectTo: 'spash', pathMatch: 'full' },
  {
    path: '',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./parceiro/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'cadastro',
    loadChildren: () => import('./parceiro/cadastro/cadastro.module').then( m => m.CadastroPageModule)
  },
  {
    path: 'esqueceu-senha',
    loadChildren: () => import('./parceiro/esqueceu-senha/esqueceu-senha.module').then( m => m.EsqueceuSenhaPageModule)
  },
  {
    path: 'pagina-branco',
    loadChildren: () => import('./pagina-branco/pagina-branco.module').then(m => m.PaginaBrancoPageModule), canActivate: [UtilService]
  },
  {
    path: 'spash',
    loadChildren: () => import('./parceiro/spash/spash.module').then( m => m.SpashPageModule)
  }


];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
