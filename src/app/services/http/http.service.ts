import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    constructor(private  http: HttpClient) {
    }

    // get dados
    getUsuario(token: any): Observable<any> {
        const url = environment.apiUrl + '/parceiro/getusuario/' + token;
        console.log(url);
        const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
        return this.http.get(url, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
        return;
    }

    // get categoria parceiro
    getCategoriasAnuncios(): Observable<any> {
        const url = environment.apiUrl + '/parceiro/getCategoriasAnuncios/';
        console.log(url);
        const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
        return this.http.get(url, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
        return;
    }


    // busca pra fazer downloader
    getArquivosAnuncios(latitude, longitude, tokenParceiro): Observable<any> {
        const url = environment.apiUrl + 'parceiro/getArquivosAnuncios/' + latitude + '/' + longitude + '/' + tokenParceiro;
        console.log(url);
        const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
        return this.http.get(url, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
        return;
    }

    // busca pra fazer downloader
    cadastraLicenca(tokenParceiro, codLicenca): Observable<any> {
        const url = environment.apiUrl + 'parceiro/cadastraLicenca/' + tokenParceiro + '/' + codLicenca;
        console.log(url);
        const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
        return this.http.get(url, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
        // return;
    }

    // verifica licença
    getVerificaLicenca(tokenParceiro, tokenLicenca): Observable<any> {
        const url = environment.apiUrl + 'parceiro/verificalicenca/' + tokenParceiro + '/' + tokenLicenca;
        const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
        return this.http.get(url, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
    }

    // busca vericar se ja tei celular
    getAnuncios(latitude, longitude, tokenParceiro): Observable<any> {
        const url = environment.apiUrl + 'parceiro/getAnuncios/' + latitude + '/' + longitude + '/' + tokenParceiro;
        console.log(url);
        const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
        return this.http.get(url, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
    }

    // Busca somente imagens
    getAnunciosDadosMoveis(latitude, longitude, tokenParceiro): Observable<any> {
        const url = environment.apiUrl + 'parceiro/getAnunciosDadosMoveis/' + latitude + '/' + longitude + '/' + tokenParceiro;
        console.log(url);
        const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
        return this.http.get(url, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
    }

    // Busca somente  anuncio empresa pubnews
    getAnunciosPubNews(): Observable<any> {
        const url = environment.apiUrl + 'parceiro/getAnunciosPubNews/'
        console.log(url);
        const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
        return this.http.get(url, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
    }

    // get busca qnt anuncios exibidos
    armazenaExibicaoAnuncio(latitude, longitude, tokenParceiro, tokenAnuncio, tokenLicenca): Observable<any> {
        const url = environment.apiUrl + 'parceiro/armazenaExibicaoAnuncio/' + latitude + '/' + longitude + '/' + tokenParceiro + '/' + tokenAnuncio + '/' + tokenLicenca;
        const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
        return this.http.get(url, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
    }

    // get busca saldos anuncios exibidos
    getSaldoParceiro(tokenParceiro?): Observable<any> {
        const url = environment.apiUrl + 'parceiro/getSaldoParceiro/' + tokenParceiro;
        // console.log(url);
        const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
        return this.http.get(url, httpOptions)
            .pipe(map(this.extractData),
                catchError(this.handleError));
        return;
    }


    // get generico
    // get(url) {
    //     const url = environment.apiUrl + `${metodo}/${tokenParceiro}`;
    //     console.log(url);
    //     const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
    //
    //     return this.http.get(url, httpOptions)
    //         .pipe(map(this.extractData),
    //             catchError(this.handleError));
    // }

    // cadastrar , login  e Recupera senha via post
    post(serviceName: string, data: any) {
        const headers = new HttpHeaders();
        const options = {header: headers, withCredentials: false};
        // criando url global em environment
        const url = environment.apiUrl + serviceName;
        console.log('service --  HttpService ' + url);
        return this.http.post(url, JSON.stringify(data), options);
    }


    // exibindo erros personalizados
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('Erro Ocorrido em :', error.error.message);
        } else {
            console.error(
                `Codigo Retornado Backend ${error.status}, ` +
                `body era: ${error.error}`);
        }
        return throwError('Erros inesperado tente novamente.');
    }

    private extractData(res: Response) {
        const body = res;
        return body || {};
    }


}
