import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpService} from '../http/http.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private httpService: HttpService) {
    }


    // buscando dados api para login
    login(dados: any): Observable<any> {
        return this.httpService.post('parceiro/efetualogin', dados);
    }

    // cadastrando parceiro
    signup(dados: any): Observable<any> {
        return this.httpService.post('parceiro/insertparceiro', dados);
    }

    // recuperar senha
    recuperaSenha(dados: any): Observable<any> {
        return this.httpService.post('parceiro/recuperasenha', dados);
    }
}
