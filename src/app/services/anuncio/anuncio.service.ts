import {Injectable} from '@angular/core';
import {Plugins} from '@capacitor/core';
import {BackgroundMode} from '@ionic-native/background-mode/ngx';
import {Downloader, DownloadRequest} from '@ionic-native/downloader/ngx';
import {File} from '@ionic-native/file/ngx';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import {ScreenOrientation} from '@ionic-native/screen-orientation/ngx';
import {VideoPlayer} from '@ionic-native/video-player/ngx';
import {ModalController, NavController, Platform} from '@ionic/angular';
import {environment} from '../../../environments/environment';
import {ModalComponent} from '../../modal/modal.component';
import {GenericaService} from '../generica/generica.service';
import {HttpService} from '../http/http.service';
import {StorageService} from '../storage/storage.service';

const {Storage, Filesystem, Geolocation, Network} = Plugins;


@Injectable({
    providedIn: 'root',
})
export class AnuncioService {
    request: DownloadRequest;
    storageAnuncio: any;
    location: boolean;
    //////// DADOS ANUNCIOS VIA STORAGE CAPACITOR OFFLINE///////////
    tokenAnuncio: any;
    latitude: any;
    longitude: any;
    tokenStorage: any;
    arquivoAnunciojson: any;
    arquivoAnuncioStorage: any;
    urlAnuncio: any;
    arquivoIsImage: any;
    timeAnuncio: any;
    tipoAnuncio: any;
    // //// DADOS saldos e exibicoes anuncios///////////
    saldoHoje: any;

    //////// DADOS ANUNCIOS VIA STORAGE CAPACITOR OFFLINE///////////
    saldoMes: any;
    exibicoesHoje: any;
    exibicoesMes: any;
    playAnuncio: any;
    public time: any;
    paraAnuncio: boolean;
    retornoFile: any;
    contadorDownloader: number;
    qtd: any = 0;
    total = 0;
    velocidade: any = 0;
    //////// DADOS saldos e exibicoes anuncios///////////
    newStatus: any;
    statusAnuncioNow = false;
    atualizado = false;
    patchFile: any;
    anuncioStorage: any;
    anuncioAparelho: any;
    public item1: any;
    private volume: any = 0.0;
    private ExisteDisposito: any;


    constructor(public videoPlayerIonic: VideoPlayer, public screenOrientation: ScreenOrientation, public file: File,
                public modalController: ModalController, public webview: WebView, public httpService: HttpService,
                public navCtrl: NavController, public downloader: Downloader, public platform: Platform,
                public backgroundMode: BackgroundMode, public storageService: StorageService,
                public genericaService: GenericaService) {

        // botao volta para anuncios fazendo 2 click
        this.storageService.setStorageAnuncio(false);
        this.platform.backButton.subscribeWithPriority(10, () => {
            this.storageService.setStorageAnuncio(true);
            this.storageService.voltaPagina('dashboard').then();
        });
    }

    // buscando velocidade geolocation speed ja convertido por km/h
    watchPosition() {
        Geolocation.watchPosition({enableHighAccuracy: true}, (position) => {
            const novoValor = (position.coords.speed) * 3.6;
            this.velocidade = Math.round(novoValor);
        });
    }


    async buscaAnuncioArea(lat: any, long: any, token: any) {
        const loading = await this.genericaService.loading({
            translucent: true,
            message: 'Buscando Anuncios, Por favor, aguarde ....',
            showBackdrop: false,
        });
        this.tipoConexao().then(async () => {
            // se existir conexao
            if ( this.newStatus.connected === true ) {
                // veiculo em movimento
                this.watchPosition();
                console.log(this.velocidade, 'km/h');
                if ( this.velocidade >= 4  ) {
                    this.httpService.getAnuncios(lat, long, token).subscribe(async (data) => {
                        await this.playEdit(lat, long, token, data);
                        await loading.dismiss();
                    }, err => {
                        loading.dismiss();
                        console.log('nao achou dados getAnuncios', err);
                    });
                    // somente pelo celular
                } else if ( this.velocidade <= 3 ) {
                    this.httpService.getAnunciosDadosMoveis(lat, long, token).subscribe(async (data) => {
                        await loading.dismiss();
                        await this.playEdit(lat, long, token, data);
                    }, err => {
                        loading.dismiss();
                        console.log('nao achou dados getAnunciosPubNews', err);
                    });
                }
                // offline
            } else {
                await this.getTipoAnuncioPubnews();
            }
        });
        await loading.dismiss();
    }

    // play de acordo
    async playEdit(lat: any, long: any, token: any, data) {
        let totalTempo = 0;
        if ( data.status !== false ) {
            for ( let i = 0; i < Object.keys(data).length; i++ ) {
                const statusAnuncio = await this.storageService.getParaAnuncio();
                // pause botao volta dispositivo
                if ( statusAnuncio === true ) {
                    break;
                }
                // pegando dados storage
                this.anuncioStorage = await this.storageService.vericaArquivoStorage(data[ i ].token);
                // pegando dados Dispositivo
                this.ExisteDisposito = await this.checandoFileExiste(environment.AndroidDataPatch, data[ i ].arquivo);
                totalTempo = +totalTempo + data[ i ].tempo_duracao;

                if ( this.anuncioStorage || this.ExisteDisposito ) {
                    await this.exibirAnuncio(data[ i ]);
                    await this.armazenaExibicaoAnuncio(lat, long, token, data[ i ].token);
                }

            }
            if ( totalTempo ) {
                await this.BuscarNovamente();
            }
        } else {
            console.log('aquii');
            await this.getTipoAnuncioPubnews();
        }
    }

    async exibirAnuncio(dados) {
        // caminho dispositivo completo no dispositivo e storage
        if ( this.ExisteDisposito === true && this.anuncioStorage ) {
            this.patchFile = environment.AndroidDataPatch + dados.arquivo;
            this.pathForImage(this.patchFile);
        } else {
            // monta imagem vindo servidor
            this.patchFile = environment.apiUrlArquivo + dados.arquivo;
        }
        this.time = dados.tempo_duracao; // pega time video vem json
        dados.arquivo_is_image === 0
            ? (console.log(this.patchFile, 'play  tipo==', dados.arquivo_is_image),
                await this.playVideo(this.patchFile),
                await this.delay(this.time),
                await this.closeVideo())
            : (console.log(this.patchFile, ' images tipo==', dados.arquivo_is_image),
                await this.presentModal(this.patchFile, this.time),
                await this.delay(this.time));
    }

    // checando se existe anuncios no dispositivo
    async checandoFileExiste(path, arquivo): Promise<any> {
        this.file.checkFile(path, arquivo)
            .then(async (result) => {
                this.retornoFile = result;
                console.log(this.retornoFile, 'arquivo existe dispositivo');
                // await this.storageService.setAnunciosStorage(token, arquivo, isImage, token, time, tipo);

            }).catch(async err => {
            if ( err.message === 'NOT_FOUND_ERR' ) {
                this.retornoFile = false;
                console.log(this.retornoFile, 'arquivo nao existe dispositivo');

                await this.DonwloadAnuncios(environment.apiUrlArquivo + arquivo, arquivo);
                // await this.storageService.setAnunciosStorage(token, arquivo, isImage, token, time, tipo);
            }

        });
        return this.retornoFile;
    }

    getFileAndroid(patch, arquivo) {
        this.file.getFile(patch, arquivo, {}).then((result) => {
            console.log(result);
        }).catch((err) => console.log('file nao existe', err));
    }

    async AnunciosParceiroDownloder(lat: any, long: any, token: any) {
        this.storageAnuncio = token;
        if ( token ) {
            this.httpService.getArquivosAnuncios(lat, long, token)
                .subscribe(async (data) => {
                    if ( data.status !== false ) {
                        for ( let i = 0; i < Object.keys(data).length; i++ ) {
                            // //////// dados pra donwload //////////
                            this.arquivoAnunciojson = data[ i ].arquivo;
                            this.urlAnuncio = data[ i ].url_arquivo;
                            this.arquivoIsImage = data[ i ].arquivo_is_image;
                            this.tokenAnuncio = data[ i ].token;
                            this.tipoAnuncio = data[ i ].tipo_anuncio;
                            this.timeAnuncio = data[ i ].tempo_duracao;
                            // verifica arquivos storage antes de downloader
                            await this.vericarArquivos();
                        }
                        this.qtd = false;
                        this.atualizado = true;

                    } else {
                        this.statusAnuncioNow = true;
                        await this.BuscarNovamente();
                        console.log('Nenhum anúncio disponível.');
                    }
                }, (err) => {
                    console.log('nao achou dados', err);
                });
        } else {
            console.log('error token invalido');
        }
    }

    // funcao downloader native
    async DonwloadAnuncios(url: string, arquivo: any): Promise<any> {
        this.request = {
            uri: url,
            title: arquivo,
            description: '',
            mimeType: '',
            visibleInDownloadsUi: false, // define se pode ou nao ser exibido interface android
            // notificationVisibility: NotificationVisibility.Visible, // define se mostra ou nao notificação
            // destinationInExternalPublicDir: {dirType: 'Pubnews', subPath: arquivo},
            destinationInExternalFilesDir: {dirType: 'Pubnews', subPath: arquivo},
            destinationUri: 'Pubnews',
        };
        this.downloader.download(this.request).then(async (location: string) => {
            console.log('Baixando download em: ', location);
        }).catch((error: any) => {
            console.log('ERRO NO download', error);
            return error;
        });
    }

    // verifica   storage e no dispositivo
    async vericarArquivos() {
        this.arquivoAnuncioStorage = await this.storageService.getItemAnuncio(this.tokenAnuncio);
        this.ExisteDisposito = await this.checandoFileExiste(environment.AndroidDataPatch, this.arquivoAnunciojson);

        if ( this.arquivoAnuncioStorage !== this.arquivoAnunciojson ) {
            console.log('ativando downloder  storage !== json ', this.arquivoAnuncioStorage === this.arquivoAnunciojson);
            this.qtd = this.qtd + 1;
            await this.delay(this.timeAnuncio);
            console.log(`contador de donwloder`, this.qtd);
            // arquivos baixados celular dinamicamente
            this.tipoConexao().then(async () => {
                if ( this.newStatus.connectionType === 'wifi' && this.ExisteDisposito === false ) {
                    await this.DonwloadAnuncios(this.urlAnuncio, this.arquivoAnunciojson);

                    // conexao apenas celular so baixar se tipo for somente imagem
                } else if ( this.newStatus.connectionType === '4g' || this.newStatus.connectionType === '3g'
                    && this.arquivoIsImage === '1' && this.ExisteDisposito === false ) {
                    console.log('somente baixa se for image para cellular');
                    await this.DonwloadAnuncios(this.urlAnuncio, this.arquivoAnunciojson);
                }
            });
            await this.storageService.setAnunciosStorage(this.tokenAnuncio, this.arquivoAnunciojson,
                this.arquivoIsImage, this.tokenAnuncio, this.timeAnuncio, this.tipoAnuncio);
        } else {
            console.log('nao baixar storage == json', this.arquivoAnuncioStorage === this.arquivoAnunciojson);
        }
    }


    async deletarArquivosCelular(patch) {
        await Filesystem.deleteFile({
            path: `${this.pathForImage(patch)}`,
            // directory: patch
        }).then((result) => console.log(result))
            .catch(e => console.log(e));
    }

    // resolve caminho images dispositivo
    pathForImage(img) {
        if ( img === null ) {
            return '';
        } else {
            // return this.webview.convertFileSrc(img);
            // tava assim antes
            return this.webview.convertFileSrc(img);
        }
    }

    async presentModal(fileImage1, time1) {
        const modal = await this.modalController.create({
            component: ModalComponent,
            cssClass: 'my-custom-class-modal',
            backdropDismiss: true,
            componentProps: {
                fileImage: fileImage1,
                time: time1,
            }
        });
        return await modal.present();
    }

    ///// Storage /////////////////////////
    async delay(ms: number) {
        return new Promise(async resolve => setTimeout(resolve, ms));
    }

    orientacaoPaisagem() {
        // orientacao paisagem
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE).then(() => {
        }).catch((e) => console.log(e));
    }

    orientacaoMobile() {
        // destrava paisagem
        this.screenOrientation.unlock();
        console.log('ativado orientacaoMobile');
    }


    async playVideo(url) {
        const options = {volume: this.volume};
        this.videoPlayerIonic.play(url, options)
            .then((r) => console.log('video completed', r))
            .catch(err => console.log(err, 'erro fazer play'));
    }

    MutePlayVideo() {
        this.volume = 0.0; // mute
    }

    async closeVideo() {
        this.videoPlayerIonic.close();
    }

    // anuncio exibido
    async armazenaExibicaoAnuncio(lat: any, long: any, tokenStorage: any, tokenAnuncio: any) {
        this.storageService.getTokenLicenca('tokenLicenca').then((licenca) => {
            this.httpService.armazenaExibicaoAnuncio(lat, long, tokenStorage, tokenAnuncio, licenca)
                .subscribe(async (data) => {
                    if ( data.status === true ) {
                        console.log('anuncio contabilizado');
                    }
                }, err => console.log(err));
        });
    }

    async backgroundModeNative() {
        this.backgroundMode.enable();

    }


    //////////////////////////// retornando some arquivos offline sistema empresa \\\\\\\\\\\\\\\\\|||||||||||||||||||||||||||||||||
    async getTipoAnuncioPubnews() {
        let totalTempo = 0;
        const {keys} = await Storage.keys();
        for ( let i = 0; i < (keys).length; i++ ) {
            const statusAnuncio = await this.storageService.getParaAnuncio();
            // pause botao volta dispositivo
            if ( statusAnuncio === true ) {
                break;
            }
            const dado = await Storage.get({key: keys[ i ]});
            const dados = JSON.parse(dado.value);
            if ( dados.tipo_anuncio === '2' ) {
                totalTempo = +totalTempo + dados.tempo_duracao;
                // caminho dispositivo completo
                const patchFile = environment.AndroidDataPatch + dados.arquivo;
                this.pathForImage(patchFile);
                dados.tipoArquivo === 0 ? (console.log(patchFile, 'play offline ---- video tipo==', dados.tipoArquivo),
                        await this.playVideo(patchFile),
                        await this.delay(dados.tempo_duracao),
                        await this.closeVideo())
                    : (console.log(patchFile, '  play offline =----- images tipo==', dados.tipoArquivo),
                        await this.presentModal(patchFile, dados.tempo_duracao),
                        await this.delay(dados.tempo_duracao));
            }
        }

        console.log('acabou loop offline', totalTempo);
        if ( totalTempo ) {
            // pegando novas posicoes geolocation
            await this.BuscarNovamente();
        }
    }

    // geoloaction Com Capacitor
    async getPositicao() {
        try {
            const coordinates = await Geolocation.getCurrentPosition({enableHighAccuracy: true});
            this.latitude = coordinates.coords.latitude;
            this.longitude = coordinates.coords.longitude;
            // return coordinates
        } catch (e) {
            console.log('erro a busca coordenadas,', e);
            await this.getPositicao();
        }
    }


    pauseAnuncio() {
        this.playAnuncio.unsubscribe();
    }

    async tipoConexao() {
        const handler = Network.addListener('networkStatusChange', (status) => {
            this.newStatus = status;
            console.log(' mudando para conexao  --anuncio.service networkStatusChange = ' + this.newStatus.connectionType);
        });
        this.newStatus = await Network.getStatus();
        console.log(' conexao atual --anuncio.service getStatus = ' + this.newStatus.connectionType);
    }

    async BuscarNovamente() {
        this.tokenStorage = await this.storageService.getTokenStorage('Parceiro');
        await this.getPositicao();
        console.log('novas coordenadas', this.latitude, this.longitude, 'token ', this.tokenStorage);
        await this.buscaAnuncioArea(this.latitude, this.longitude, this.tokenStorage);
    }


}
