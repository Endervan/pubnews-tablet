import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {Plugins} from '@capacitor/core';
import {AndroidPermissions} from '@ionic-native/android-permissions/ngx';
import {BackgroundMode} from '@ionic-native/background-mode/ngx';
import {NavController} from '@ionic/angular';
import {AnuncioService} from '../anuncio/anuncio.service';
import {GenericaService} from '../generica/generica.service';
import {HttpService} from '../http/http.service';
import {StorageService} from '../storage/storage.service';


const {Network, Storage, Geolocation} = Plugins;

export interface Parceiro {
    dadosNome: string;
    dadosEmail: string;
    dadosCpf: number;
    dadosCelular: number;
    dadosFoto: string;
    tipoParceiro: string;
}

@Injectable({
    providedIn: 'root'
})
export class UtilService implements CanActivate {
    // network Capacitor
    public request;


    // coords
    public longitude;
    public latitude;
    nomeParceiro: any;
    celularParceiro: any;
    emailParceiro: any;

    constructor(public navCtrl: NavController,
                private backgroundMode: BackgroundMode, public httpService: HttpService, public storageService: StorageService,
                private androidPermissions: AndroidPermissions, public anuncioService: AnuncioService,
                public genericaService: GenericaService
    ) {
    }


    // guarda route usuario autenticados token storage armazenado
    async canActivate(route: ActivatedRouteSnapshot) {
        // this.storageService.tokenStorage = await this.storageService.getTokenStorage('Parceiro');
        this.storageService.tokenStorage = true;
        if ( this.storageService.tokenStorage ) {
            return true;
        } else {
            await this.storageService.rootPagina(`login`);
            return false;
        }
    }

    ///// Storage /////////////////////////
    // setando dados array


    // geoloaction Com Capacitor
    async getPositicao() {
        try {
            const coordinates = await Geolocation.getCurrentPosition({enableHighAccuracy: true});
            this.latitude = coordinates.coords.latitude;
            this.longitude = coordinates.coords.longitude;
            // return coordinates
        } catch (e) {
            console.log('erro a busca coordenadas,', e);
            if ( e.code === 3 ) {
                this.getPositicao().then(async () => {
                    await this.solicitaPermissao();
                });
            }
        }
    }

    async tipoConexao() {
        const handler = Network.addListener('networkStatusChange', (status) => {
            console.log(status.connectionType);
            // if (status.connectionType === 'wifi') {
            console.log('rede mudando para wi fi');
            this.getPositicao().then(async () => {
                console.log('coods', this.latitude, this.longitude);
                await this.anuncioService.AnunciosParceiroDownloder(this.latitude, this.longitude, this.storageService.tokenStorage);
            }).catch(() => console.log('error ao conectar wi fi  '));

        });
        const status = await Network.getStatus();
        // if (status.connectionType === 'wifi' || status.connectionType === 'cellular') {
        console.log(' conexao atual  = ' + status.connectionType);
        this.getPositicao().then(async () => {
            await this.anuncioService.AnunciosParceiroDownloder(this.latitude, this.longitude, this.storageService.tokenStorage);
            console.log('coords getstatus', this.latitude, this.longitude);
        }).catch(() => console.log('error ao conectar wi fi  '));
        // }
    }

    async verificaLicenca() {
        this.storageService.tokenStorage = await this.storageService.getTokenStorage('Parceiro');
        // verifica licenca
        const licenca = await this.storageService.getTokenLicenca('tokenLicenca');
        this.httpService.getVerificaLicenca(this.storageService.tokenStorage, licenca).subscribe((data) => {
            if ( data.status === false ) {
                console.log('sem licenca');
                this.storageService.voltaPagina('licenca');
            } else if ( data.status === true || this.storageService.tokenStorage ) {
                console.log('autenticado  com token e licenca');
                this.storageService.irPagina('dashboard');
            } else {
                console.log('usuario sem acesso');
                this.storageService.voltaPagina('login');
            }
        });
    }

    // ativando backgroundMode
    async nativeSegundoPlano() {
        this.backgroundMode.enable();
        this.backgroundMode.on('activate').subscribe(() => {
            const array = [ 1, 2 ];
            for ( const a of array ) {
                console.log(a);
            }
        });
    }

    async solicitaPermissao() {
        const permissao1 = this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
            .then(result => {
                    this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
                },
                err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE));
        const permissao2 = this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)
            .then(result => {
                    this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION);
                },
                err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION));
        Promise.all([ permissao1, permissao2 ]).then();

    }

}
