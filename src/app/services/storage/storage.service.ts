import {Injectable} from '@angular/core';
import {Plugins} from '@capacitor/core';
import {NavController} from '@ionic/angular';
import {HttpService} from '../http/http.service';

const { Storage } = Plugins;



@Injectable({
    providedIn: 'root'
})
export class StorageService {


    // dados parceiros
    nomeParceiro?: string = null;
    cpfParceiro?: number = null;
    celularParceiro?: number = null;
    emailParceiro?: string = null;
    fotoParceiro?: string = null;
    tipoParceiro?: string = null;
    //////////// ==== //////////////
    tokenStorage: any;  // vem token storage
    parceiroStorage = null; // vem dados storage
    tokenJson: any; // vem api
    timeImage: any = null;

    constructor(public navCtrl: NavController, public httpService: HttpService) { }

    async getParaAnuncio() {
        const dado = await Storage.get({ key: 'paraAnuncio' });
        const dados = JSON.parse(dado.value);
        if (dados) {
            return dados.paraAnuncio;
        } else {
            console.log('nao existe  anuncio celular');
            return false;
        }
    }

    async setStorageAnuncio(valor) {
        await Storage.set({
            key: 'paraAnuncio',
            value: JSON.stringify({
                paraAnuncio: valor
            })
        });

    }

    async setAnunciosStorage(keyAnuncio, arquivoAnuncio, tipo, tokenAnuncio, time, tipoAnuncio) {
        await Storage.set({
            key: keyAnuncio,
            value: JSON.stringify({
                arquivo: arquivoAnuncio,
                tipoArquivo: tipo,
                token: tokenAnuncio,
                tempo_duracao: time,
                tipo_anuncio: tipoAnuncio
            })
        });
    }

    // pegando key e value
    async getItemAnuncio(chave) {
        const dado = await Storage.get({ key: chave });
        const dados = JSON.parse(dado.value);
        if (dados) {
            return dados.arquivo;
        } else {
            console.log('nao existe  anuncio celular');
            return false;
        }
    } // pegando somente itens tenha tipoAnuncio storage


    async vericaArquivoStorage(arquivo) {
        const arquivoStorage = await this.getItemAnuncio(arquivo);
        if (arquivoStorage) {
            return arquivoStorage;
        } else {
            return false;
        }
    }


    async getTokenLicenca(chave) {
        const dado = await Storage.get({ key: chave });
        const dados = JSON.parse(dado.value);
        if (dados) {
            return dados.licenca;
        }else {
            return false;
        }
    }


    async setParceiroStorage(tokenParceiro, nomeParceiro?, cpfParceiro?, celularParceiro?, emailParceiro?, fotoParceiro?, tipoParceiro?) {
        await Storage.set({
            key: 'Parceiro',
            value: JSON.stringify({
                token: tokenParceiro,
                nome: nomeParceiro,
                cpf: cpfParceiro,
                celular: celularParceiro,
                email: emailParceiro,
                foto: fotoParceiro,
                tipo_Parceiro: tipoParceiro,
            })
        });
    }

    async setLicencaStorage(tokenLicenca, token) {
        await Storage.set({
            key: tokenLicenca,
            value: JSON.stringify({
                licenca: token,
            })
        });
    }

    // buscando dados array somente token
    async getTokenStorage(chave) {
        const dado = await Storage.get({ key: chave });
        const dados = JSON.parse(dado.value);
        if (dados) {
            return dados.token;
        } else {
            console.log('sem token valido');
        }
    }

    async getParceiroStorage(chave) {
        const dado = await Storage.get({ key: chave });
        const dados = JSON.parse(dado.value);
        if (dados) {
            return dados;
        } else {
            console.log('nao exite dados offline');
            // todo retira dps  qnd volta back
            // await this.voltaPagina('login');
            return false;
        }
    }

    // set dado individual
    async setItem(dadoKey, dadoValue): Promise<any> {
        await Storage.set({ key: dadoKey, value: dadoValue });
    }

    // pegando dado
    async getItem(dadoKey): Promise<string> {
        const dados = await Storage.get({ key: dadoKey });
        const dadosStorage = dados.value;
        return dadosStorage;
    }


    // removendo dado
    async removeItem(dado) {
        await Storage.remove({ key: dado });
    }

    // limpando  Tudo Storage
    async clearStorage() {
        await Storage.clear();
    }




    // anuncio exibido json
    async getAnuncios(lat, long, token) {
        this.httpService.getAnuncios(lat, long, token)
            .subscribe(async (data) => {
                console.log(data);
            }, err => console.log(err));

    }

    // pegando dado somente pubnews tipo anuncio 2 uso rede movel
    async getItemPubnews(dadoKey): Promise<string> {
        const dados = await Storage.get({ key: dadoKey });
        const dadosStorage: any = JSON.parse(dados.value);
        console.log(dadosStorage.tipo_anuncio);
        return dadosStorage.tipo_anuncio;
    }

    async keys() {
        const { keys } = await Storage.keys();
        for (let i = 0; i < Object.keys.length; i++) {
            const teste = await this.getItem(keys[i]);

            console.log(`keys aqui `, teste);

        }
    }


    // sair aplicativo
    async logout() {
        await Storage.clear();
    }


    // ###########################################################################################################
    // desmonta pilha e volta  pagina
    async voltaPagina(pagina) {
        await this.navCtrl.navigateBack(pagina);
    }

    // desmonta pilha
    async popPagina(pagina) {
        await this.navCtrl.navigateBack(pagina);
    }

    // monta pilha e vai proxima pagina
    async irPagina(pagina) {
        await this.navCtrl.navigateForward(pagina);
    }

    async Pop() {
        await this.navCtrl.pop();
    }

    //  vai  pagina root
    async rootPagina(pagina) {
        await this.navCtrl.navigateRoot(pagina);
        return;
    }


}
