import { TestBed } from '@angular/core/testing';

import { GenericaService } from './generica.service';

describe('GenericaService', () => {
  let service: GenericaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenericaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
