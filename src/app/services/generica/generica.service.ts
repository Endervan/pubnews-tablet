import {Injectable} from '@angular/core';
import {AlertController, LoadingController, ToastController} from '@ionic/angular';
import {AlertOptions, LoadingOptions, ToastOptions} from '@ionic/core';


@Injectable({
    providedIn: 'root'
})
export class GenericaService {

    constructor(private toastController: ToastController,
                public alertCtrl: AlertController, public loadingController: LoadingController) {
    }

    // mensagem de alerta atributos facultativo
    async alert(options?: AlertOptions): Promise<HTMLIonAlertElement> {
        const alert = await this.alertCtrl.create(options);
        await alert.present();
        return alert;
    }

    // loading processos
    async loading(options?: LoadingOptions): Promise<HTMLIonLoadingElement> {
        const loading = await this.loadingController.create({
            message: 'Carregando...',
            ...options, // passa todos paramentos complementares
        });
        await loading.present();
        return loading;
    }

    // Toast processos
    async toast(options?: ToastOptions): Promise<HTMLIonToastElement> {
        const toast = await this.toastController.create({
            position: 'bottom',
            duration: 3000,
            keyboardClose: true,
            ...options, // passa todos paramentos complementares facultativos
        });
        await toast.present();
        return toast;
    }
}
