import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ValidadorService } from "../../services/validador/validador.service";
import { AuthService } from "../../services/auth/auth.service";
import { HttpService } from "../../services/http/http.service";
import { take } from "rxjs/operators";
import { IonicSelectableComponent } from "ionic-selectable";
import { GenericaService } from 'src/app/services/generica/generica.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { NavController } from '@ionic/angular';
import { UtilService } from 'src/app/services/util/util.service';


class Categoria {
    public token: any;
    public titulo: string;
}

@Component({
    selector: 'app-edit-parceiro',
    templateUrl: './edit-parceiro.page.html',
    styleUrls: ['./edit-parceiro.page.scss'],
})
export class EditParceiroPage extends StorageService implements OnInit {
    categorianegocios: Categoria[];
    categorianegocio: Categoria;

    form: FormGroup;
    // metodo pra alterna entre inputs do login e criar mesma pagina
    configs = {
        isSignIn: true,
        action: 'Carro',
        actionChange: 'Empresa'
    };
    getCategorias: any;
    // adicionando campo dinamicamente depedendo da acao
    private nameControl = new FormControl('', [Validators.required]);


    constructor(public navCtrl: NavController, public formBuilder: FormBuilder, private val: ValidadorService, private authService: AuthService,
        public genericaService: GenericaService, public util: UtilService, public httpService: HttpService) {
        super(navCtrl, httpService);
    }

    async ionViewWillEnter() {

    }

    async ngOnInit() {
        this.createForm();
        this.getDadosCategorias();
    }

    portChange(event: {
        component: IonicSelectableComponent,
        value: any
    }) {
        console.log('categorianegocio id :', event.value);
    }


    // Cadastrar parceiro
    async AtualizarDados() {
        console.log(this.form.value)
        this.authService.signup(this.form.value).subscribe((data: any) => {
            if (data.status) {
                // this.util.toast({message: data.mensagem, buttons: ['ok']}); //  msg de success aplicativo
                // this.util.setParceiroStorage(data.token);
                this.irPagina(`dashboard/perfil`);
            } else {
                this.genericaService.alert({ message: data.mensagem, buttons: ['OK'] }); //  msg erro aplicativo
                console.log(data.mensagem);
            }
        }, err => {
            this.genericaService.alert({ message: err.message, buttons: ['OK'] }); //  msg de erro api
            console.log('aqui', err);
        });
    }

    // dados parceiro
    async getDadosParceiro() {
        this.httpService.getUsuario(this.tokenStorage)
            .subscribe((data: any) => {
                this.nomeParceiro = data.nome;
                this.emailParceiro = data.email;
                this.cpfParceiro = data.cpf;
                this.celularParceiro = data.celular;
                this.fotoParceiro = data.foto;
                this.tipoParceiro = data.tipo_parceiro;
                this.tokenJson = data.token;

                // verifica se token storage == token json
                console.log('storage -> ', this.tokenStorage, '===', 'json ->', this.tokenJson);
                if (this.tokenStorage != this.tokenJson || !this.tokenStorage) {
                    this.rootPagina(`login`);
                }
            }, err => {
                console.log(err);
            });
    }

    // OcultaInputs(): void {
    //     this.configs.isSignIn = false;
    //     const {isSignIn} = this.configs;
    //     // verifica se e pra remover ou add inputs nameControl
    //     !isSignIn ? this.form.addControl('categorianegocio', this.nameControl) : this.form.removeControl('categorianegocio');
    // }
    //
    // voltaInputs(): void {
    //     this.configs.isSignIn = true;
    //     const {isSignIn} = this.configs;
    //     // verifica se e pra remover ou add inputs nameControl
    //     !isSignIn ? this.form.addControl('categorianegocio', this.nameControl) : this.form.removeControl('categorianegocio');
    // }

    // categorianegocios parceiro
    getDadosCategorias() {
        this.httpService.getCategoriasAnuncios()
            .pipe(take(1))
            .subscribe((data: any) => {
                this.categorianegocios = data;
                // console.log('categoria', this.categorianegocios);
            }, err => {
                console.log(err);
            });
    }

    private createForm(): void {
        //  campos do builder para controle
        this.form = this.formBuilder.group({
            nome: ['', Validators.required, this.val.nameValid],
            celular: ['', Validators.compose([Validators.minLength(14), Validators.required])],
            email: ['', Validators.required, this.val.emailValid],
            // tipo_parceiro: ['', Validators.required],
            // senha: ['', Validators.compose([Validators.minLength(6), Validators.required])],
        });
    }

}
