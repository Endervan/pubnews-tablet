import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditParceiroPageRoutingModule } from './edit-parceiro-routing.module';

import { EditParceiroPage } from './edit-parceiro.page';
import {BrMaskerModule} from "br-mask";
import {IonicSelectableModule} from "ionic-selectable";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        EditParceiroPageRoutingModule,
        ReactiveFormsModule,
        BrMaskerModule,
        IonicSelectableModule
    ],
  declarations: [EditParceiroPage]
})
export class EditParceiroPageModule {}
