import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditParceiroPage } from './edit-parceiro.page';

const routes: Routes = [
  {
    path: '',
    component: EditParceiroPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditParceiroPageRoutingModule {}
