import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditParceiroPage } from './edit-parceiro.page';

describe('EditParceiroPage', () => {
  let component: EditParceiroPage;
  let fixture: ComponentFixture<EditParceiroPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditParceiroPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditParceiroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
