import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import { Anuncio } from './anuncio.page';

describe('Anuncio', () => {
  let component: Anuncio;
  let fixture: ComponentFixture<Anuncio>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Anuncio],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(Anuncio);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
