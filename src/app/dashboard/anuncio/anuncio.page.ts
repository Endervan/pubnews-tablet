import {Component} from '@angular/core';
import {UtilService} from '../../services/util/util.service';
import {AnuncioService} from '../../services/anuncio/anuncio.service';
import {HttpService} from '../../services/http/http.service';
import {PaginaBrancoPage} from '../../pagina-branco/pagina-branco.page';
import {GenericaService} from 'src/app/services/generica/generica.service';
import {StorageService} from 'src/app/services/storage/storage.service';


@Component({
    selector: 'app-tab1',
    templateUrl: 'anuncio.page.html',
    styleUrls: [ 'anuncio.page.scss' ],
})

export class Anuncio {
    loading1: any;

    constructor(public util: UtilService, public anuncioService: AnuncioService, public storageService: StorageService,
                public httpService: HttpService, public pagina: PaginaBrancoPage,
                public genericaService: GenericaService) {
    }


    async ionViewWillEnter() {
        this.storageService.parceiroStorage = await this.storageService.getParceiroStorage('Parceiro');
        this.storageService.nomeParceiro = this.storageService.parceiroStorage.nome;
        await this.pagina.modoDesligaInsomia();
        await this.getExibicoes();
    }

    async ionViewDidEnter() {
        // await this.util.verificaLicenca();
        await this.util.tipoConexao();
    }


    async getExibicoes() {
        this.httpService.getSaldoParceiro(this.storageService.tokenStorage)
            .subscribe(async (result) => {
                this.anuncioService.exibicoesHoje = result.views_dia;
                this.anuncioService.exibicoesMes = result.views_mes;
                if ( !this.anuncioService.exibicoesHoje ||
                    !this.anuncioService.exibicoesMes ) {
                    this.loading1 = await this.genericaService.loading({
                        translucent: true,
                        message: 'Por favor, aguarde ....',
                        showBackdrop: false,
                    });
                    await this.loading1.dismiss();
                }
            }, err => {
                this.loading1.dismiss();
                console.log(err);
            });
    }


}
