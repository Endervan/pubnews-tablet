import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Anuncio} from './anuncio.page';
import {ExploreContainerComponentModule} from '../../explore-container/explore-container.module';
import {AnuncioRoutingModule} from './anuncio-routing.module';
import {VideoPlayer} from '@ionic-native/video-player/ngx';
import {PaginaBrancoPage} from '../../pagina-branco/pagina-branco.page';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ExploreContainerComponentModule,
        AnuncioRoutingModule,
    ],
    declarations: [Anuncio],
    providers: [VideoPlayer, PaginaBrancoPage],
    exports: []
})
export class AnuncioModule {
}
