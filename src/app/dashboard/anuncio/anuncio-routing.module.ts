import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Anuncio } from './anuncio.page';

const routes: Routes = [
  {
    path: '',
    component: Anuncio,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnuncioRoutingModule {}
