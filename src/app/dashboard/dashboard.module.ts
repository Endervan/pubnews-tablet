import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {DashboardPage} from './dashboard.page';
import {DashboardPageRoutingModule} from "./dashboard-routing.module";
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        DashboardPageRoutingModule,
        FontAwesomeModule
    ],
    declarations: [DashboardPage]
})
export class DashboardPageModule {
}
