import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardPage} from './dashboard.page';
import {UtilService} from "../services/util/util.service";

const routes: Routes = [
    {
        path: 'dashboard',
        component: DashboardPage,
        children: [
            {
                path: 'anuncio',
                loadChildren: () => import('./anuncio/anuncio.module').then(m => m.AnuncioModule),
            },
            {
                path: 'faturamento',
                loadChildren: () => import('./faturamento/faturamento.module').then(m => m.FaturamentoModule),
            },
            {
                path: 'perfil',
                loadChildren: () => import('./perfil/perfil.module').then(m => m.PerfilPageModule),

            },
            {
                path: 'edit-parceiro',
                loadChildren: () => import('./edit-parceiro/edit-parceiro.module').then(m => m.EditParceiroPageModule),
            },
            {
                path: '',
                redirectTo: '/dashboard/anuncio',
                pathMatch: 'full'
            }
        ], canActivate: [UtilService]
    },
    {
        path: '',
        redirectTo: '/dashboard/anuncio',
        pathMatch: 'full'
    },
    {
        path: 'licenca',
        loadChildren: () => import('./licenca/licenca.module').then(m => m.LicencaPageModule),canActivate: [UtilService]
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardPageRoutingModule {
}
