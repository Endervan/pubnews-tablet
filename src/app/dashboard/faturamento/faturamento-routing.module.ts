import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Faturamento } from './faturamento.page';

const routes: Routes = [
  {
    path: '',
    component: Faturamento,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FaturamentoRoutingModule {}
