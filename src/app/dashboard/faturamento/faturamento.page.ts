import {Component} from '@angular/core';
import {UtilService} from '../../services/util/util.service';
import {HttpService} from '../../services/http/http.service';
import {AnuncioService} from '../../services/anuncio/anuncio.service';
import {GenericaService} from 'src/app/services/generica/generica.service';
import {StorageService} from 'src/app/services/storage/storage.service';

@Component({
    selector: 'app-tab2',
    templateUrl: 'faturamento.page.html',
    styleUrls: [ 'faturamento.page.scss' ]
})
export class Faturamento {
    private loading: any;

    constructor(public genericaService: GenericaService, public storageService: StorageService, public util: UtilService,
                public httpService: HttpService, public anuncioService: AnuncioService) {
    }

    async ionViewWillEnter() {
        await this.getSaldo();
    }

    async getSaldo() {
        this.httpService.getSaldoParceiro(this.storageService.tokenStorage).subscribe(async (data) => {
            this.anuncioService.saldoHoje = data.saldo_dia;
            this.anuncioService.saldoMes = data.saldo_mes;
            if ( this.anuncioService.saldoHoje !== data.saldo_dia || this.anuncioService.saldoMes !== data.saldo_mes ) {
                this.loading = await this.genericaService.loading({
                    translucent: true,
                    message: 'Por favor, aguarde ....',
                    showBackdrop: false,
                });
                await this.loading.dismiss();
            }
        }, err => {
            console.log(err);
            this.loading.dismiss();
        });
    }


}
