import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import { Faturamento } from './faturamento.page';

describe('Faturamento', () => {
  let component: Faturamento;
  let fixture: ComponentFixture<Faturamento>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Faturamento],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(Faturamento);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
