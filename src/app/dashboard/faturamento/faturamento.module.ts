import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Faturamento } from './faturamento.page';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import { FaturamentoRoutingModule } from './faturamento-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    FaturamentoRoutingModule
  ],
  declarations: [Faturamento]
})
export class FaturamentoModule {}
