import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LicencaPageRoutingModule } from './licenca-routing.module';

import { LicencaPage } from './licenca.page';
import {BrMaskerModule} from 'br-mask';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        LicencaPageRoutingModule,
        ReactiveFormsModule,
        BrMaskerModule
    ],
  declarations: [LicencaPage]
})
export class LicencaPageModule {}
