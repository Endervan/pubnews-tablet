import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LicencaPage } from './licenca.page';

describe('LicencaPage', () => {
  let component: LicencaPage;
  let fixture: ComponentFixture<LicencaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicencaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LicencaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
