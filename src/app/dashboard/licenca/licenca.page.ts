import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericaService} from 'src/app/services/generica/generica.service';
import {StorageService} from 'src/app/services/storage/storage.service';
import {HttpService} from '../../services/http/http.service';
import {UtilService} from '../../services/util/util.service';

@Component({
    selector: 'app-licenca',
    templateUrl: './licenca.page.html',
    styleUrls: [ './licenca.page.scss' ],
})
export class LicencaPage implements OnInit {
    form: FormGroup;

    constructor(public formBuilder: FormBuilder, public storageService: StorageService,
                private httpService: HttpService, public genericaService: GenericaService, private util: UtilService) {

        //  campos do builder para controle
        this.form = formBuilder.group({
            licenca: [ '', [ Validators.required, Validators.minLength(9) ] ],
        });
    }

    async ionViewWillEnter() {
        await this.util.tipoConexao();
    }

    async ngOnInit() {

    }

    async cadastraLicenca() {
        const cod = this.form.value;
        await this.httpService.cadastraLicenca(this.storageService.tokenStorage, cod.licenca)
            .subscribe(async (data) => {
                if ( data.status === false ) {
                    await this.genericaService.toast({message: data.mensagem + ' Tente outro Usúario'});
                } else {
                    await this.storageService.setLicencaStorage('tokenLicenca', data.token);
                    await this.storageService.irPagina('dashboard');
                    await this.genericaService.toast({message: data.mensagem, position: 'top'});
                }
            }, err => {
                console.log(err);
            });
    }

    async limpaUser() {
        await this.storageService.removeItem('Parceiro');
    }

}
