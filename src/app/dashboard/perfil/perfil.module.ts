import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';
import {PerfilPage} from "./perfil.page";
import {PerfilPageRoutingModule} from "./perfil-routing.module";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {LogoutComponent} from "../../logout/logout.component";


@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ExploreContainerComponentModule,
        RouterModule.forChild([{path: '', component: PerfilPage}]),
        PerfilPageRoutingModule,
        FontAwesomeModule,
    ],
    declarations: [PerfilPage, LogoutComponent],
    exports:[LogoutComponent]
})
export class PerfilPageModule {}
