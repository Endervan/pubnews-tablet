import {Component, OnInit} from '@angular/core';
import {UtilService} from '../../services/util/util.service';
import {HttpService} from '../../services/http/http.service';
import {faUserEdit} from '@fortawesome/free-solid-svg-icons';
import {StorageService} from 'src/app/services/storage/storage.service';

@Component({
    selector: 'app-perfil',
    templateUrl: 'perfil.page.html',
    styleUrls: [ 'perfil.page.scss' ]
})
export class PerfilPage implements OnInit {

    faUserEdit = faUserEdit

    constructor(public util: UtilService, private httpService: HttpService, public storageService: StorageService) {
    }

    // verifica se usuario esta logado
    async ngOnInit() {
        this.storageService.parceiroStorage = await this.storageService.getParceiroStorage('Parceiro');
        // populando dados storage
        this.storageService.nomeParceiro = this.storageService.parceiroStorage.nome;
        this.storageService.emailParceiro = this.storageService.parceiroStorage.email;
        this.storageService.cpfParceiro = this.storageService.parceiroStorage.cpf;
        this.storageService.tipoParceiro = this.storageService.parceiroStorage.tipo_Parceiro;
        this.storageService.celularParceiro = this.storageService.parceiroStorage.celular;
    }


    async editarParceiro() {
        await this.storageService.irPagina(`dashboard/edit-parceiro`);
    }
}
