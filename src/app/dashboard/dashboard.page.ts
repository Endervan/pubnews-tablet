import {Component} from '@angular/core';
import {faDonate, faPlayCircle, faUser} from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-tabs',
    templateUrl: 'dashboard.page.html',
    styleUrls: ['dashboard.page.scss']
})
export class DashboardPage {
    faDonate = faDonate;
    faPlayCircle = faPlayCircle;
    faUser = faUser;
    constructor() {
    }

}
