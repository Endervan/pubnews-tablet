import {Component} from '@angular/core';
import {UtilService} from '../services/util/util.service';
import {faSignOutAlt} from '@fortawesome/free-solid-svg-icons';
import {GenericaService} from '../services/generica/generica.service';
import {StorageService} from '../services/storage/storage.service';

@Component({
    selector: 'app-logout',
    template: `
        <ion-fab id="logout" vertical="top" horizontal="end" slot="fixed">
            <ion-fab-button>
                <fa-icon (click)="logout()" [icon]="faSignOutAlt" size="lg"></fa-icon>
            </ion-fab-button>
        </ion-fab>
    `,
})
export class LogoutComponent {

    faSignOutAlt = faSignOutAlt;

    constructor(public util: UtilService, public storageService: StorageService, public genericaService: GenericaService) {
    }

    async logout(): Promise<void> {
        await this.genericaService.alert({
            message: 'Deseja Sair Aplicação ? ',
            buttons: [
                {
                    text: 'Sim',
                    handler: async () => {
                        // removendo dados parceiro offline
                        await this.storageService.removeItem('Parceiro');
                        await this.storageService.voltaPagina('/login');
                    }
                },
                'Nao'
            ],
        });
    }

}
