import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PaginaBrancoPageRoutingModule } from './pagina-branco-routing.module';

import { PaginaBrancoPage } from './pagina-branco.page';
import {ModalComponent} from '../modal/modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaginaBrancoPageRoutingModule
  ],
  declarations: [PaginaBrancoPage, ModalComponent],
  exports: [ModalComponent]
})
export class PaginaBrancoPageModule {}
