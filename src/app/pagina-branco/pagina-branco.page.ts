import {Component, OnInit} from '@angular/core';
import {UtilService} from '../services/util/util.service';
import {Insomnia} from '@ionic-native/insomnia/ngx';

import {Plugins} from '@capacitor/core';
import {AnuncioService} from '../services/anuncio/anuncio.service';
import {Subscription} from 'rxjs';
import {GenericaService} from '../services/generica/generica.service';
import {StorageService} from '../services/storage/storage.service';

const {Storage} = Plugins;


@Component({
    selector: 'app-pagina-branco',
    templateUrl: './pagina-branco.page.html',
    styleUrls: [ './pagina-branco.page.scss' ],
})
export class PaginaBrancoPage implements OnInit {
    customBackActionSubscription: Subscription;

    constructor(public util: UtilService, public genericaService: GenericaService,
                public anuncioService: AnuncioService, private insomnia: Insomnia, public storageService: StorageService) {
    }


    async ionViewDidEnter() {
        this.modoTelaAtivado().then();
        Storage.set({
            key: 'paraAnuncio',
            value: JSON.stringify({
                paraAnuncio: false
            })
        }).then();

    }

    async ngOnInit() {
        this.customBackActionSubscription = this.anuncioService.platform.backButton.subscribe(() => {
            this.genericaService.toast({
                cssClass: 'cssVoltar',
                message: 'click 2 vezes para sair ',
                position: 'middle',
                duration: 800
            });

        });
        Storage.set({
            key: 'paraAnuncio',
            value: JSON.stringify({
                paraAnuncio: false
            })
        }).then();
    }


    // sair exibicoes anuncio volta modo insomia desligado
    async ionViewDidLeave() {
        if ( this.customBackActionSubscription ) {
            this.customBackActionSubscription.unsubscribe();
        }
        await this.modoDesligaInsomia();
    }

    async ionViewWillLeave() {
        if ( this.customBackActionSubscription ) {
            this.customBackActionSubscription.unsubscribe();
        }
    }


    async ionViewWillEnter() {
        await this.modoTelaAtivado();
        this.storageService.tokenStorage = await this.storageService.getTokenStorage('Parceiro');
        this.anuncioService.getPositicao().then(async () => {
            await this.anuncioService.buscaAnuncioArea(this.anuncioService.latitude,
                this.anuncioService.longitude, this.storageService.tokenStorage);
        });
    }

    async modoTelaAtivado() {
        this.insomnia.keepAwake()
            .then(
                () => console.log('success'),
                () => console.log('error')
            );

    }

    async modoDesligaInsomia() {
        this.insomnia.allowSleepAgain()
            .then(
                () => console.log('success'),
                () => console.log('error')
            );
    }

}

