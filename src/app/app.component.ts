import {Component} from '@angular/core';

import {Platform} from '@ionic/angular';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {UtilService} from './services/util/util.service';
import {AnuncioService} from './services/anuncio/anuncio.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: [ 'app.component.scss' ]
})
export class AppComponent {
    constructor(
        private platform: Platform,
        private statusBar: StatusBar,
        private util: UtilService,
        private anuncioService: AnuncioService,
        public router: Router
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(async () => {
            this.statusBar.styleDefault();
            this.router.navigateByUrl('spash');
            this.anuncioService.orientacaoPaisagem();
            await this.util.solicitaPermissao();
            // await this.util.tipoConexao();
            //  await this.anuncioService.backgroundModeNative()
        });
    }


}
