import {Component, OnInit} from '@angular/core';
import {UtilService} from '../../services/util/util.service';
import {StorageService} from "../../services/storage/storage.service";

@Component({
    selector: 'app-spash',
    templateUrl: './spash.page.html',
    styleUrls: [ './spash.page.scss' ],
})
export class SpashPage implements OnInit {

    constructor(public util: UtilService,public storage: StorageService) {
        setTimeout(async () => {
            await this.storage.voltaPagina('login');
            // await this.util.verificaLicenca();
        }, 3000);
    }

     ngOnInit() {
    }

}
