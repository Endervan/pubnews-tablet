import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UtilService} from "../../services/util/util.service";
import {ValidadorService} from "../../services/validador/validador.service";
import {AuthService} from "../../services/auth/auth.service";
import { GenericaService } from 'src/app/services/generica/generica.service';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-esqueceu-senha',
  templateUrl: './esqueceu-senha.page.html',
  styleUrls: ['./esqueceu-senha.page.scss'],
})
export class EsqueceuSenhaPage implements OnInit {

  form: FormGroup;

  constructor(public util: UtilService, public genericaService: GenericaService,public formBuilder: FormBuilder,
              private val: ValidadorService, private authService: AuthService,public storageService:StorageService,) {
    //  campos do builder para controle
    this.form = formBuilder.group({
      email: ['', Validators.required, this.val.emailValid],
    });
  }

  ngOnInit() {
  }

  // Recupera Senha
  async Recuperar() {
    const loading = await this.genericaService.loading();
    this.authService.recuperaSenha(this.form.value).subscribe((data: any) => {

      try {
        if (data.status === true) {
          this.genericaService.alert({message: data.mensagem, buttons: ['OK']});  //  msg de sucesso
          this.storageService.irPagina('/login');
        } else {
          this.genericaService.alert({message: data.mensagem, buttons: ['OK']});  //  erro dados invalidos
        }
      } catch (err) {
        this.genericaService.toast({message: err.mensagem, buttons: ['OK']}); //  msg de erro
        console.log(err);
      } finally {
        loading.dismiss();
      }
    });
  }


}
