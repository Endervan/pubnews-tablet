import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth/auth.service';
import {UtilService} from '../../services/util/util.service';
import {ValidadorService} from '../../services/validador/validador.service';
import {take} from 'rxjs/operators';
import {HttpService} from '../../services/http/http.service';
import {IonicSelectableComponent} from 'ionic-selectable';
import {GenericaService} from 'src/app/services/generica/generica.service';
import {StorageService} from 'src/app/services/storage/storage.service';

class Categoria {
    public token: any;
    public titulo: string;
}

@Component({
    selector: 'app-cadastro',
    templateUrl: './cadastro.page.html',
    styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {
    categorianegocios: Categoria[];
    categorianegocio: Categoria;
    //  instanciando  formgroup
    form: FormGroup;

    // metodo pra alterna entre inputs do login e criar mesma pagina
    configs = {
        isSignIn: true,
        action: 'Carro',
        actionChange: 'Empresa'
    };
    getCategorias: any;
    // adicionando campo dinamicamente depedendo da acao
    private nameControl = new FormControl('', [Validators.required]);

    constructor(public util: UtilService, public formBuilder: FormBuilder,
        private val: ValidadorService, private authService: AuthService,
        private httpService: HttpService,public storageService:StorageService, public genericaService: GenericaService) {
    }


    portChange(event: {
        component: IonicSelectableComponent,
        value: any
    }) {
        console.log('categorianegocio id :', event.value);
    }

    ngOnInit() {
        this.createForm();
        this.getDadosCategorias();
    }

    // Cadastrar parceiro
    async Cadastrar() {
        console.log(this.form.value);
        this.authService.signup(this.form.value).subscribe(async (data: any) => {
            if (data.status === true) {
                await this.storageService.setParceiroStorage(data.token, data.nome, data.cpf, data.celular, data.email, data.tipo_parceiro);
                await this.storageService.voltaPagina('login');
                // await this.util.verificaLicenca();
            } else {
                await this.genericaService.alert({ message: data.mensagem, buttons: ['OK'] }); //  msg erro aplicativo
                console.log(data.mensagem);
            }
        }, err => {
            this.genericaService.alert({ message: err.mensagem, buttons: ['OK'] }); //  msg de erro api
        });
    }

    OcultaInputs(): void {
        this.configs.isSignIn = false;
        const { isSignIn } = this.configs;
        // verifica se e pra remover ou add inputs nameControl
        !isSignIn ? this.form.addControl('categorianegocio', this.nameControl) : this.form.removeControl('categorianegocio');
    }

    voltaInputs(): void {
        this.configs.isSignIn = true;
        const { isSignIn } = this.configs;
        // verifica se e pra remover ou add inputs nameControl
        !isSignIn ? this.form.addControl('categorianegocio', this.nameControl) : this.form.removeControl('categorianegocio');
    }

    // categorianegocios parceiro
    getDadosCategorias() {
        this.httpService.getCategoriasAnuncios()
            .pipe(take(1))
            .subscribe((data: any) => {
                this.categorianegocios = data;
                // console.log('categoria', this.categorianegocios);
            }, err => {
                console.log(err);
            });
    }

    private createForm(): void {
        //  campos do builder para controle
        this.form = this.formBuilder.group({
            nome: ['', Validators.required, this.val.nameValid],
            celular: ['', Validators.compose([Validators.minLength(14), Validators.required])],
            email: ['', Validators.required, this.val.emailValid],
            tipo_parceiro: ['', Validators.required],
            senha: ['', Validators.compose([Validators.minLength(6), Validators.required])],
        });
    }
}
