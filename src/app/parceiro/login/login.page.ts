import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ValidadorService} from '../../services/validador/validador.service';
import {AuthService} from '../../services/auth/auth.service';
import {UtilService} from '../../services/util/util.service';
import {Facebook} from '@ionic-native/facebook/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';
import {AnuncioService} from '../../services/anuncio/anuncio.service';
import {GenericaService} from 'src/app/services/generica/generica.service';
import {StorageService} from 'src/app/services/storage/storage.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
    providers: [ValidadorService]
})
export class LoginPage implements OnInit {
//  instanciando  formgroup
    form: FormGroup;

    // face develops dados api
    // isLoggedIn = false;
    // users = {id: '', name: '', email: '', picture: {data: {url: ''}}};

    constructor(private fb: Facebook, private googlePlus: GooglePlus, public formBuilder: FormBuilder,
                private val: ValidadorService, private authService: AuthService, public util: UtilService,
                public storageService: StorageService, public anuncioService: AnuncioService, public genericaService: GenericaService) {

        // verificando status conexão com face
        // fb.getLoginStatus()
        //     .then(res => {
        //         console.log(res.status);
        //         if (res.status === 'connect') {
        //             this.isLoggedIn = true;
        //         } else {
        //             this.isLoggedIn = false;
        //         }
        //     })
        //     .catch(e => console.log(e));

        //  campos do builder para controle
        this.form = formBuilder.group({
            email: ['', Validators.required, this.val.emailValid],
            senha: ['', [Validators.required, Validators.minLength(6)]],
        });

    }

    async ngOnInit() {
        await this.util.solicitaPermissao();
        // await this.util.verificaLicenca();
        await this.util.tipoConexao();
        this.anuncioService.delay(100).then(() => {
            // this.storageService.voltaPagina('login');
        });
    }


    // login normal
    async loginAction() {
        this.authService.login(this.form.value).subscribe((data: any) => {
            if (data.status === false) {
                this.genericaService.toast({message: data.mensagem, buttons: ['OK'], position: 'top'}); //  msg de erro retorno api
            } else {
                // setando dados Parceiro offline storage
                this.storageService.setParceiroStorage(
                    data.token, data.nome, data.cpf, data.celular, data.email, data.foto, data.tipo_parceiro);
                this.storageService.irPagina('dashboard');
                this.form.reset();
                // this.util.verificaLicenca();
            }
        }, err => {
            this.genericaService.toast({message: 'OffLine', buttons: ['OK'], position: 'top'}); //  msg de erro
            console.log(err);
        });
    }


    // login facebook
    // fbLogin() {
    //     this.fb.login(['public_profile', 'user_friends', 'email'])
    //         .then(async res => {
    //             if (res.status === 'connected') {
    //                 this.isLoggedIn = true;
    //                 this.getUserDetail(res.authResponse.userID);
    //                 await this.util.verificaLicenca();
    //             } else {
    //                 this.isLoggedIn = false;
    //             }
    //         })
    //         .catch(e => console.log('Error ao conectar no Facebook', e));
    // }

    // buscando dados assicronos FACEBOOK
    // getUserDetail(userid: any) {
    //     this.fb.api('/' + userid + '/?fields=id,email,name,picture', ['public_profile'])
    //         .then(res => {
    //             console.log(res);
    //             this.users = res;
    //         })
    //         .catch(e => {
    //             console.log(e);
    //         });
    // }

    // funcao sair e resetar cache login FACEBOOK
    // logoutFace() {
    //     this.fb.logout()
    //         .then(res => this.isLoggedIn = false)
    //         .catch(e => console.log('Error ao Sair do Facebook', e));
    // }


// login google plus
//     googleLogin() {
//         this.googlePlus.login({}).then(async res => {
//             alert('dados api google' + res);
//             // await this.util.verificaLicenca()
//         })
//             .catch(err => {
//                 alert('erros ao busca dados google ' + err);
//             });
//     }
//
//     async googleLogout() {
//         await this.googlePlus.logout();
//     }


}

