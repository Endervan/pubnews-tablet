(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["parceiro-login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/parceiro/login/login.page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/parceiro/login/login.page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n    <ion-toolbar id=\"azul-login\">\r\n        <div class=\"ion-text-center\">\r\n            <img class=\"pt20\" src=\"../../../assets/logo1.png\">\r\n        </div>\r\n    </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"inputLogin ion-padding aviso-erro\">\r\n    <ion-grid>\r\n        <ion-row class=\"ion-justify-content-center\">\r\n            <ion-col size-md=\"12\">\r\n                <form id=\"erro\" [formGroup]=\"form\" (ngSubmit)=\"loginAction()\">\r\n                    <ion-item>\r\n                        <ion-label position=\"stacked\">Email</ion-label>\r\n                        <ion-input class=\"inputLogin\"\r\n                                   name=\"email\"\r\n                                   type=\"email\"\r\n                                   value=\"endvan@gmail.com\"\r\n                                   formControlName=\"email\"\r\n                                   clearInput clearOnEdit=\"false\">\r\n                        </ion-input>\r\n                    </ion-item>\r\n                    <div *ngIf=\"form.controls['email'].dirty && !form.controls['email'].valid\">\r\n                        <p *ngIf=\"form.controls['email'].errors.InvalidEmail\">* Email Invalido</p>\r\n                    </div>\r\n\r\n                    <ion-item>\r\n                        <ion-label position=\"stacked\">Senha</ion-label>\r\n                        <ion-input\r\n                                name=\"senha\"\r\n                                formControlName=\"senha\"\r\n                                type=\"password\"\r\n                                value=\"teste01\"\r\n                                clinliearInput clearOnEdit=\"true\">\r\n                        </ion-input>\r\n                    </ion-item>\r\n                    <div *ngIf=\"form.controls['senha'].dirty && !form.controls['senha'].valid\">\r\n                        <p *ngIf=\"form.controls['senha'].errors\">* Mínimo 6 caracteres</p>\r\n                    </div>\r\n                    <ion-row class=\"ion-justify-content-center social\">\r\n                        <ion-col size=\"4\" class=\"ion-text-center mt15 \">\r\n                            <ion-button shape=\"round\" (ng-click)=\"loginAction()\" type=\"submit\" [disabled]=\"!form.valid\" expand=\"block\">\r\n                                LOGIN\r\n                            </ion-button >\r\n                            <ion-input type=\"submit\" style=\"position: absolute; left: -9999px; width: 1px; height: 1px;\" enterkeyhint=\"go\"></ion-input>\r\n                        </ion-col>\r\n                    </ion-row>\r\n                </form>\r\n                <ion-row class=\"ion-justify-content-center social\">\r\n                    <ion-col size=\"5\" class=\"ion-text-center mt15 \">\r\n                        <h6><a routerLink='/esqueceu-senha' routerDirection=\"back\">Esqueceu Senha ?</a></h6>\r\n                        <h6> Não tem uma conta ? <a routerLink='/cadastro'>Cadastre-se</a></h6>\r\n<!--                        <h6>-->\r\n<!--                            <hr/>-->\r\n<!--                            OU teste -->\r\n<!--                            <hr/>-->\r\n<!--                        </h6>-->\r\n\r\n<!--                        <h6>Entre com a rede social</h6>-->\r\n\r\n<!--                        <ion-button id=\"face\" color=\"primary\" shape=\"round\" (click)=\"fbLogin()\">-->\r\n<!--                            Facebook-->\r\n<!--                            <ion-icon slot=\"start\" name=\"logo-facebook\"></ion-icon>-->\r\n<!--                        </ion-button>-->\r\n<!--                        <ion-button id=\"google\" color=\"danger\" shape=\"round\" (click)=\"googleLogin()\">-->\r\n<!--                            Google-->\r\n<!--                            <ion-icon slot=\"start\" name=\"logo-google\"></ion-icon>-->\r\n<!--                        </ion-button>-->\r\n\r\n                    </ion-col>\r\n                </ion-row>\r\n            </ion-col>\r\n        </ion-row>\r\n\r\n    </ion-grid>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/parceiro/login/login-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/parceiro/login/login-routing.module.ts ***!
  \********************************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "./src/app/parceiro/login/login.page.ts");




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ "./src/app/parceiro/login/login.module.ts":
/*!************************************************!*\
  !*** ./src/app/parceiro/login/login.module.ts ***!
  \************************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/parceiro/login/login-routing.module.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/parceiro/login/login.page.ts");







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/parceiro/login/login.page.scss":
/*!************************************************!*\
  !*** ./src/app/parceiro/login/login.page.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhcmNlaXJvL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/parceiro/login/login.page.ts":
/*!**********************************************!*\
  !*** ./src/app/parceiro/login/login.page.ts ***!
  \**********************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_validador_validador_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/validador/validador.service */ "./src/app/services/validador/validador.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_util_util_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/util/util.service */ "./src/app/services/util/util.service.ts");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "./node_modules/@ionic-native/facebook/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/google-plus/ngx */ "./node_modules/@ionic-native/google-plus/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _services_anuncio_anuncio_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/anuncio/anuncio.service */ "./src/app/services/anuncio/anuncio.service.ts");
/* harmony import */ var src_app_services_generica_generica_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/generica/generica.service */ "./src/app/services/generica/generica.service.ts");
/* harmony import */ var src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/storage/storage.service */ "./src/app/services/storage/storage.service.ts");











let LoginPage = class LoginPage {
    // face develops dados api
    // isLoggedIn = false;
    // users = {id: '', name: '', email: '', picture: {data: {url: ''}}};
    constructor(fb, googlePlus, formBuilder, val, authService, util, storageService, anuncioService, genericaService) {
        // verificando status conexão com face
        // fb.getLoginStatus()
        //     .then(res => {
        //         console.log(res.status);
        //         if (res.status === 'connect') {
        //             this.isLoggedIn = true;
        //         } else {
        //             this.isLoggedIn = false;
        //         }
        //     })
        //     .catch(e => console.log(e));
        this.fb = fb;
        this.googlePlus = googlePlus;
        this.formBuilder = formBuilder;
        this.val = val;
        this.authService = authService;
        this.util = util;
        this.storageService = storageService;
        this.anuncioService = anuncioService;
        this.genericaService = genericaService;
        //  campos do builder para controle
        this.form = formBuilder.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, this.val.emailValid],
            senha: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)]],
        });
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.util.solicitaPermissao();
            // await this.util.verificaLicenca();
            yield this.util.tipoConexao();
            this.anuncioService.delay(100).then(() => {
                // this.storageService.voltaPagina('login');
            });
        });
    }
    // login normal
    loginAction() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.authService.login(this.form.value).subscribe((data) => {
                if (data.status === false) {
                    this.genericaService.toast({ message: data.mensagem, buttons: ['OK'], position: 'top' }); //  msg de erro retorno api
                }
                else {
                    // setando dados Parceiro offline storage
                    this.storageService.setParceiroStorage(data.token, data.nome, data.cpf, data.celular, data.email, data.foto, data.tipo_parceiro);
                    this.storageService.irPagina('dashboard');
                    // this.util.verificaLicenca();
                }
            }, err => {
                this.genericaService.toast({ message: 'OffLine', buttons: ['OK'], position: 'top' }); //  msg de erro
                console.log(err);
            });
        });
    }
};
LoginPage.ctorParameters = () => [
    { type: _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_6__["Facebook"] },
    { type: _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_7__["GooglePlus"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_validador_validador_service__WEBPACK_IMPORTED_MODULE_3__["ValidadorService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _services_util_util_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"] },
    { type: src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_10__["StorageService"] },
    { type: _services_anuncio_anuncio_service__WEBPACK_IMPORTED_MODULE_8__["AnuncioService"] },
    { type: src_app_services_generica_generica_service__WEBPACK_IMPORTED_MODULE_9__["GenericaService"] }
];
LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/parceiro/login/login.page.html")).default,
        providers: [_services_validador_validador_service__WEBPACK_IMPORTED_MODULE_3__["ValidadorService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./login.page.scss */ "./src/app/parceiro/login/login.page.scss")).default]
    })
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=parceiro-login-login-module-es2015.js.map