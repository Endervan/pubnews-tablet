(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pagina-branco-pagina-branco-module"],{

/***/ "./src/app/pagina-branco/pagina-branco-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pagina-branco/pagina-branco-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: PaginaBrancoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginaBrancoPageRoutingModule", function() { return PaginaBrancoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _pagina_branco_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pagina-branco.page */ "./src/app/pagina-branco/pagina-branco.page.ts");




const routes = [
    {
        path: '',
        component: _pagina_branco_page__WEBPACK_IMPORTED_MODULE_3__["PaginaBrancoPage"]
    }
];
let PaginaBrancoPageRoutingModule = class PaginaBrancoPageRoutingModule {
};
PaginaBrancoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PaginaBrancoPageRoutingModule);



/***/ }),

/***/ "./src/app/pagina-branco/pagina-branco.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pagina-branco/pagina-branco.module.ts ***!
  \*******************************************************/
/*! exports provided: PaginaBrancoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginaBrancoPageModule", function() { return PaginaBrancoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _pagina_branco_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pagina-branco-routing.module */ "./src/app/pagina-branco/pagina-branco-routing.module.ts");
/* harmony import */ var _pagina_branco_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pagina-branco.page */ "./src/app/pagina-branco/pagina-branco.page.ts");
/* harmony import */ var _modal_modal_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../modal/modal.component */ "./src/app/modal/modal.component.ts");








let PaginaBrancoPageModule = class PaginaBrancoPageModule {
};
PaginaBrancoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _pagina_branco_routing_module__WEBPACK_IMPORTED_MODULE_5__["PaginaBrancoPageRoutingModule"]
        ],
        declarations: [_pagina_branco_page__WEBPACK_IMPORTED_MODULE_6__["PaginaBrancoPage"], _modal_modal_component__WEBPACK_IMPORTED_MODULE_7__["ModalComponent"]],
        exports: [_modal_modal_component__WEBPACK_IMPORTED_MODULE_7__["ModalComponent"]]
    })
], PaginaBrancoPageModule);



/***/ })

}]);
//# sourceMappingURL=pagina-branco-pagina-branco-module-es2015.js.map