function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == typeof h && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw new Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(typeof e + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw new Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : String(i); }

function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["anuncio-anuncio-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/anuncio/anuncio.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/anuncio/anuncio.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppDashboardAnuncioAnuncioPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n    <ion-toolbar id=\"azul-login-page\">\r\n        <div class=\"ion-text-center\">\r\n            <img class=\"pt8\" src=\"../../../assets/logo1.png\">\r\n        </div>\r\n    </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content [fullscreen]=\"true\">\r\n    <ion-grid>\r\n        <ion-row class=\"home\">\r\n            <ion-col>\r\n                <ion-row>\r\n                    <ion-col size=\"6\">\r\n                        <ion-text id=\"text\">\r\n                            Olá, <strong>{{ storageService.nomeParceiro   }}</strong> ! Seja bem vindo.\r\n                        </ion-text>\r\n                    </ion-col>\r\n                    <ion-col class=\"ion-text-center\" size=\"6\" *ngIf=\"anuncioService.qtd ; else total \">\r\n                        Atualizando anúncio {{ anuncioService.qtd}} aguarde ....\r\n                        <ion-progress-bar type=\"indeterminate\" buffer=\"0.15\"></ion-progress-bar>\r\n                    </ion-col>\r\n                    <ng-template #total>\r\n                        <ion-col size=\"6\" class=\"ion-text-end\">\r\n                            <ng-container *ngIf=\"anuncioService.statusAnuncioNow ;else atualizado \">\r\n                                SEM ANÚNCIOS\r\n                            </ng-container>\r\n                        </ion-col>\r\n                    </ng-template>\r\n                    <ng-template #atualizado>\r\n                        ANÚNCIOS ATUALIZADOS\r\n                    </ng-template>\r\n                </ion-row>\r\n                <div class=\"ion-text-center\">\r\n                    <h3>Anúncios exibidos</h3>\r\n                    <hr>\r\n                </div>\r\n                <ion-item class=\"\" lines=\"no-lines\">\r\n                    <ion-label>Hoje</ion-label>\r\n                    <ion-badge *ngIf=\"this.anuncioService.exibicoesHoje;else vazio\"\r\n                               slot=\"end\">{{this.anuncioService.exibicoesHoje }}</ion-badge>\r\n                    <ng-template #vazio slot=\"end\">\r\n                        <ion-badge slot=\"end\">0</ion-badge>\r\n                    </ng-template>\r\n                </ion-item>\r\n\r\n                <ion-item class=\"mt10\" lines=\"no-lines\">\r\n                    <ion-label>Mês</ion-label>\r\n                    <ion-badge *ngIf=\"this.anuncioService.exibicoesMes ;else vazio\"\r\n                               slot=\"end\">{{this.anuncioService.exibicoesMes }}</ion-badge>\r\n                    <ng-template #vazio slot=\"end\">\r\n                        <ion-badge slot=\"end\">0</ion-badge>\r\n                    </ng-template>\r\n                </ion-item>\r\n            </ion-col>\r\n        </ion-row>\r\n\r\n\r\n        <ion-row class=\"ion-justify-content-center social\">\r\n            <ion-col size=\"7\" class=\"ion-text-center\" *ngIf=\"anuncioService.statusAnuncioNow;else iniciar\">\r\n                <b>Não existe anúncios neste local ? Buscando novamente....</b>\r\n            </ion-col>\r\n            <ng-template #iniciar>\r\n                <ion-col size=\"7\" class=\"ion-text-center\">\r\n                    <ion-button shape=\"round\" (click)=\"this.storageService.irPagina('pagina-branco')\" expand=\"block\"\r\n                                [disabled]=\"anuncioService.qtd \">\r\n                        Iniciar Anuncios\r\n                    </ion-button>\r\n                </ion-col>\r\n            </ng-template>\r\n        </ion-row>\r\n\r\n    </ion-grid>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/dashboard/anuncio/anuncio-routing.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/dashboard/anuncio/anuncio-routing.module.ts ***!
    \*************************************************************/

  /*! exports provided: AnuncioRoutingModule */

  /***/
  function srcAppDashboardAnuncioAnuncioRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AnuncioRoutingModule", function () {
      return AnuncioRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _anuncio_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./anuncio.page */
    "./src/app/dashboard/anuncio/anuncio.page.ts");

    var routes = [{
      path: '',
      component: _anuncio_page__WEBPACK_IMPORTED_MODULE_3__["Anuncio"]
    }];

    var AnuncioRoutingModule = /*#__PURE__*/_createClass(function AnuncioRoutingModule() {
      _classCallCheck(this, AnuncioRoutingModule);
    });

    AnuncioRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AnuncioRoutingModule);
    /***/
  },

  /***/
  "./src/app/dashboard/anuncio/anuncio.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/dashboard/anuncio/anuncio.module.ts ***!
    \*****************************************************/

  /*! exports provided: AnuncioModule */

  /***/
  function srcAppDashboardAnuncioAnuncioModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AnuncioModule", function () {
      return AnuncioModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _anuncio_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./anuncio.page */
    "./src/app/dashboard/anuncio/anuncio.page.ts");
    /* harmony import */


    var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../explore-container/explore-container.module */
    "./src/app/explore-container/explore-container.module.ts");
    /* harmony import */


    var _anuncio_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./anuncio-routing.module */
    "./src/app/dashboard/anuncio/anuncio-routing.module.ts");
    /* harmony import */


    var _ionic_native_video_player_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/video-player/ngx */
    "./node_modules/@ionic-native/video-player/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _pagina_branco_pagina_branco_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../pagina-branco/pagina-branco.page */
    "./src/app/pagina-branco/pagina-branco.page.ts");

    var AnuncioModule = /*#__PURE__*/_createClass(function AnuncioModule() {
      _classCallCheck(this, AnuncioModule);
    });

    AnuncioModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
      imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"], _anuncio_routing_module__WEBPACK_IMPORTED_MODULE_7__["AnuncioRoutingModule"]],
      declarations: [_anuncio_page__WEBPACK_IMPORTED_MODULE_5__["Anuncio"]],
      providers: [_ionic_native_video_player_ngx__WEBPACK_IMPORTED_MODULE_8__["VideoPlayer"], _pagina_branco_pagina_branco_page__WEBPACK_IMPORTED_MODULE_9__["PaginaBrancoPage"]],
      exports: []
    })], AnuncioModule);
    /***/
  },

  /***/
  "./src/app/dashboard/anuncio/anuncio.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/dashboard/anuncio/anuncio.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppDashboardAnuncioAnuncioPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content {\n  --background: transparent !important;\n}\n\n.home ion-text strong {\n  color: #286ef2;\n  font-size: 15px;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL2FudW5jaW8vQzpcXFdTXFxQdWJuZXdzXFxwdWJuZXdzLXRhYmxldC9zcmNcXGFwcFxcZGFzaGJvYXJkXFxhbnVuY2lvXFxhbnVuY2lvLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZGFzaGJvYXJkL2FudW5jaW8vYW51bmNpby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxvQ0FBQTtBQ0NGOztBREdBO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0FGIiwiZmlsZSI6InNyYy9hcHAvZGFzaGJvYXJkL2FudW5jaW8vYW51bmNpby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudHtcclxuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcblxyXG4uaG9tZSBpb24tdGV4dCBzdHJvbmd7XHJcbiAgY29sb3I6ICMyODZlZjI7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4iLCJpb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbn1cblxuLmhvbWUgaW9uLXRleHQgc3Ryb25nIHtcbiAgY29sb3I6ICMyODZlZjI7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/dashboard/anuncio/anuncio.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/dashboard/anuncio/anuncio.page.ts ***!
    \***************************************************/

  /*! exports provided: Anuncio */

  /***/
  function srcAppDashboardAnuncioAnuncioPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Anuncio", function () {
      return Anuncio;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _services_util_util_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/util/util.service */
    "./src/app/services/util/util.service.ts");
    /* harmony import */


    var _services_anuncio_anuncio_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/anuncio/anuncio.service */
    "./src/app/services/anuncio/anuncio.service.ts");
    /* harmony import */


    var _services_http_http_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../services/http/http.service */
    "./src/app/services/http/http.service.ts");
    /* harmony import */


    var _pagina_branco_pagina_branco_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../pagina-branco/pagina-branco.page */
    "./src/app/pagina-branco/pagina-branco.page.ts");
    /* harmony import */


    var src_app_services_generica_generica_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/services/generica/generica.service */
    "./src/app/services/generica/generica.service.ts");
    /* harmony import */


    var src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/services/storage/storage.service */
    "./src/app/services/storage/storage.service.ts");

    var Anuncio = /*#__PURE__*/function () {
      function Anuncio(util, anuncioService, storageService, httpService, pagina, genericaService) {
        _classCallCheck(this, Anuncio);

        this.util = util;
        this.anuncioService = anuncioService;
        this.storageService = storageService;
        this.httpService = httpService;
        this.pagina = pagina;
        this.genericaService = genericaService;
      }

      _createClass(Anuncio, [{
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  _context.next = 2;
                  return this.storageService.getParceiroStorage('Parceiro');

                case 2:
                  this.storageService.parceiroStorage = _context.sent;
                  this.storageService.nomeParceiro = this.storageService.parceiroStorage.nome;
                  _context.next = 6;
                  return this.pagina.modoDesligaInsomia();

                case 6:
                  _context.next = 8;
                  return this.getExibicoes();

                case 8:
                case "end":
                  return _context.stop();
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
            return _regeneratorRuntime().wrap(function _callee2$(_context2) {
              while (1) switch (_context2.prev = _context2.next) {
                case 0:
                  _context2.next = 2;
                  return this.util.tipoConexao();

                case 2:
                case "end":
                  return _context2.stop();
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "getExibicoes",
        value: function getExibicoes() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee4() {
            var _this = this;

            return _regeneratorRuntime().wrap(function _callee4$(_context4) {
              while (1) switch (_context4.prev = _context4.next) {
                case 0:
                  this.httpService.getSaldoParceiro(this.storageService.tokenStorage).subscribe(function (result) {
                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee3() {
                      return _regeneratorRuntime().wrap(function _callee3$(_context3) {
                        while (1) switch (_context3.prev = _context3.next) {
                          case 0:
                            this.anuncioService.exibicoesHoje = result.views_dia;
                            this.anuncioService.exibicoesMes = result.views_mes;

                            if (!(!this.anuncioService.exibicoesHoje || !this.anuncioService.exibicoesMes)) {
                              _context3.next = 8;
                              break;
                            }

                            _context3.next = 5;
                            return this.genericaService.loading({
                              translucent: true,
                              message: 'Por favor, aguarde ....',
                              showBackdrop: false
                            });

                          case 5:
                            this.loading1 = _context3.sent;
                            _context3.next = 8;
                            return this.loading1.dismiss();

                          case 8:
                          case "end":
                            return _context3.stop();
                        }
                      }, _callee3, this);
                    }));
                  }, function (err) {
                    _this.loading1.dismiss();

                    console.log(err);
                  });

                case 1:
                case "end":
                  return _context4.stop();
              }
            }, _callee4, this);
          }));
        }
      }]);

      return Anuncio;
    }();

    Anuncio.ctorParameters = function () {
      return [{
        type: _services_util_util_service__WEBPACK_IMPORTED_MODULE_2__["UtilService"]
      }, {
        type: _services_anuncio_anuncio_service__WEBPACK_IMPORTED_MODULE_3__["AnuncioService"]
      }, {
        type: src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_7__["StorageService"]
      }, {
        type: _services_http_http_service__WEBPACK_IMPORTED_MODULE_4__["HttpService"]
      }, {
        type: _pagina_branco_pagina_branco_page__WEBPACK_IMPORTED_MODULE_5__["PaginaBrancoPage"]
      }, {
        type: src_app_services_generica_generica_service__WEBPACK_IMPORTED_MODULE_6__["GenericaService"]
      }];
    };

    Anuncio = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-tab1',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./anuncio.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/anuncio/anuncio.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./anuncio.page.scss */
      "./src/app/dashboard/anuncio/anuncio.page.scss"))["default"]]
    })], Anuncio);
    /***/
  }
}]);
//# sourceMappingURL=anuncio-anuncio-module-es5.js.map