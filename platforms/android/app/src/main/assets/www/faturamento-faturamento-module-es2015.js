(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["faturamento-faturamento-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/faturamento/faturamento.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/faturamento/faturamento.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n    <ion-toolbar id=\"azul-login-page\">\r\n        <div class=\"ion-text-center\">\r\n            <img class=\"pt10\" src=\"../../../assets/logo1.png\">\r\n        </div>\r\n    </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n<ion-content [fullscreen]=\"true\">\r\n    <ion-grid>\r\n        <ion-row class=\"home\">\r\n            <ion-col class=\"mt20\">\r\n                <ion-text id=\"text\">\r\n                    Saldo\r\n                    <hr>\r\n                </ion-text>\r\n                <ion-item class=\"mt10 ion-margin-bottom\" lines=\"no-lines\">\r\n                    <ion-label>Dia</ion-label>\r\n                    <ion-badge  *ngIf=\"this.anuncioService.saldoHoje;else vazio\" slot=\"end\">{{this.anuncioService.saldoHoje }}</ion-badge>\r\n                    <ng-template #vazio slot=\"end\"><ion-badge slot=\"end\">0,00</ion-badge></ng-template>\r\n\r\n                </ion-item>\r\n                <ion-item class=\"mt10 ion-margin-bottom\" lines=\"no-lines\">\r\n                    <ion-label>Mês</ion-label>\r\n                    <ion-badge  *ngIf=\"this.anuncioService.saldoMes;else vazio\" slot=\"end\">{{this.anuncioService.saldoMes }}</ion-badge>\r\n                    <ng-template #vazio slot=\"end\"><ion-badge slot=\"end\">0,00</ion-badge></ng-template>\r\n                </ion-item>\r\n\r\n                <ion-text id=\"text1\">\r\n                    Pagamentos efetuados\r\n                    <hr>\r\n                </ion-text>\r\n                <ion-item class=\"faturamento mt10\" color=\"success\" lines=\"no-lines\">\r\n                    <ion-label>03 de janeiro de 2020</ion-label>\r\n                    <ion-badge slot=\"end\">R$ 1.500,00</ion-badge>\r\n                </ion-item>\r\n            </ion-col>\r\n        </ion-row>\r\n    </ion-grid>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/dashboard/faturamento/faturamento-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/dashboard/faturamento/faturamento-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: FaturamentoRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaturamentoRoutingModule", function() { return FaturamentoRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _faturamento_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./faturamento.page */ "./src/app/dashboard/faturamento/faturamento.page.ts");




const routes = [
    {
        path: '',
        component: _faturamento_page__WEBPACK_IMPORTED_MODULE_3__["Faturamento"],
    }
];
let FaturamentoRoutingModule = class FaturamentoRoutingModule {
};
FaturamentoRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], FaturamentoRoutingModule);



/***/ }),

/***/ "./src/app/dashboard/faturamento/faturamento.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/dashboard/faturamento/faturamento.module.ts ***!
  \*************************************************************/
/*! exports provided: FaturamentoModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaturamentoModule", function() { return FaturamentoModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _faturamento_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./faturamento.page */ "./src/app/dashboard/faturamento/faturamento.page.ts");
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../explore-container/explore-container.module */ "./src/app/explore-container/explore-container.module.ts");
/* harmony import */ var _faturamento_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./faturamento-routing.module */ "./src/app/dashboard/faturamento/faturamento-routing.module.ts");








let FaturamentoModule = class FaturamentoModule {
};
FaturamentoModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"],
            _faturamento_routing_module__WEBPACK_IMPORTED_MODULE_7__["FaturamentoRoutingModule"]
        ],
        declarations: [_faturamento_page__WEBPACK_IMPORTED_MODULE_5__["Faturamento"]]
    })
], FaturamentoModule);



/***/ }),

/***/ "./src/app/dashboard/faturamento/faturamento.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/dashboard/faturamento/faturamento.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9mYXR1cmFtZW50by9mYXR1cmFtZW50by5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/dashboard/faturamento/faturamento.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/dashboard/faturamento/faturamento.page.ts ***!
  \***********************************************************/
/*! exports provided: Faturamento */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Faturamento", function() { return Faturamento; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_util_util_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/util/util.service */ "./src/app/services/util/util.service.ts");
/* harmony import */ var _services_http_http_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/http/http.service */ "./src/app/services/http/http.service.ts");
/* harmony import */ var _services_anuncio_anuncio_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/anuncio/anuncio.service */ "./src/app/services/anuncio/anuncio.service.ts");
/* harmony import */ var src_app_services_generica_generica_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/generica/generica.service */ "./src/app/services/generica/generica.service.ts");
/* harmony import */ var src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/storage/storage.service */ "./src/app/services/storage/storage.service.ts");







let Faturamento = class Faturamento {
    constructor(genericaService, storageService, util, httpService, anuncioService) {
        this.genericaService = genericaService;
        this.storageService = storageService;
        this.util = util;
        this.httpService = httpService;
        this.anuncioService = anuncioService;
    }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.getSaldo();
        });
    }
    getSaldo() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.httpService.getSaldoParceiro(this.storageService.tokenStorage).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                this.anuncioService.saldoHoje = data.saldo_dia;
                this.anuncioService.saldoMes = data.saldo_mes;
                if (this.anuncioService.saldoHoje !== data.saldo_dia || this.anuncioService.saldoMes !== data.saldo_mes) {
                    this.loading = yield this.genericaService.loading({
                        translucent: true,
                        message: 'Por favor, aguarde ....',
                        showBackdrop: false,
                    });
                    yield this.loading.dismiss();
                }
            }), err => {
                console.log(err);
                this.loading.dismiss();
            });
        });
    }
};
Faturamento.ctorParameters = () => [
    { type: src_app_services_generica_generica_service__WEBPACK_IMPORTED_MODULE_5__["GenericaService"] },
    { type: src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_6__["StorageService"] },
    { type: _services_util_util_service__WEBPACK_IMPORTED_MODULE_2__["UtilService"] },
    { type: _services_http_http_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"] },
    { type: _services_anuncio_anuncio_service__WEBPACK_IMPORTED_MODULE_4__["AnuncioService"] }
];
Faturamento = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab2',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./faturamento.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/faturamento/faturamento.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./faturamento.page.scss */ "./src/app/dashboard/faturamento/faturamento.page.scss")).default]
    })
], Faturamento);



/***/ })

}]);
//# sourceMappingURL=faturamento-faturamento-module-es2015.js.map