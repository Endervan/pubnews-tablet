(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["edit-parceiro-edit-parceiro-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/edit-parceiro/edit-parceiro.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/edit-parceiro/edit-parceiro.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/dashboard/perfil\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Editar - {{ util.nomeParceiro }}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n<ion-content class=\"inputLogin ion-padding aviso-erro\">\r\n  <ion-grid>\r\n    <ion-row class=\"ion-justify-content-center\">\r\n      <ion-col size-md=\"12\">\r\n        <form id=\"erro\" [formGroup]=\"form\" (ngSubmit)=\"AtualizarDados()\">\r\n          <ion-item>\r\n            <ion-label position=\"stacked\">Nome</ion-label>\r\n            <ion-input name=\"nome\" value=\"{{ util.nomeParceiro }}\"\r\n                       formControlName=\"nome\">\r\n            </ion-input>\r\n          </ion-item>\r\n          <div *ngIf=\"form.controls['nome'].dirty && !form.controls['nome'].valid\">\r\n            <p *ngIf=\"form.controls['nome'].errors\">* Apenas Letras</p>\r\n          </div>\r\n\r\n          <ion-item>\r\n            <ion-label position=\"stacked\">Telefone/Celular</ion-label>\r\n            <ion-input type=\"tel\"\r\n                       formControlName=\"celular\"\r\n                       value=\"{{ util.celularParceiro }}\"\r\n                       [brmasker]=\"{phone: true}\">\r\n            </ion-input>\r\n          </ion-item>\r\n          <div *ngIf=\"form.controls['celular'].dirty && !form.controls['celular'].valid\">\r\n            <p *ngIf=\"form.controls['celular'].errors\">* Número Inválido</p>\r\n          </div>\r\n\r\n          <ion-item>\r\n            <ion-label position=\"stacked\">Email</ion-label>\r\n            <ion-input\r\n                    name=\"email\"\r\n                    type=\"email\"\r\n                    value=\"{{ util.emailParceiro }}\"\r\n                    formControlName=\"email\">\r\n            </ion-input>\r\n          </ion-item>\r\n          <div *ngIf=\"form.controls['email'].dirty && !form.controls['email'].valid\">\r\n            <p *ngIf=\"form.controls['email'].errors.InvalidEmail\">* Email Invalido</p>\r\n          </div>\r\n\r\n<!--          <ion-item>-->\r\n<!--            <ion-label position=\"stacked\">Senha</ion-label>-->\r\n<!--            <ion-input required name=\"senha\"-->\r\n<!--                       formControlName=\"senha\"-->\r\n<!--                       type=\"password\">-->\r\n<!--            </ion-input>-->\r\n<!--          </ion-item>-->\r\n\r\n<!--          <div *ngIf=\"form.controls['senha'].dirty && !form.controls['senha'].valid\">-->\r\n<!--            <p *ngIf=\"form.controls['senha'].errors\">* Minimo 6 caracteres</p>-->\r\n<!--          </div>-->\r\n\r\n\r\n          <ion-row>\r\n<!--            <ion-col>-->\r\n<!--              <h5>Quero exibir os anúncios em</h5>-->\r\n<!--              <ion-item>-->\r\n<!--                <ion-segment formControlName=\"tipo_parceiro\">-->\r\n<!--                  <ion-segment-button value=\"movel\" (click)=\"voltaInputs()\">-->\r\n<!--                    <img src=\"../../../assets/icon-car.png\">-->\r\n<!--                    <ion-label>Meu Carro</ion-label>-->\r\n<!--                  </ion-segment-button>-->\r\n\r\n<!--                  <ion-segment-button value=\"fixo\" (click)=\"OcultaInputs()\">-->\r\n<!--                    <img src=\"../../../assets/icon-map1.png\">-->\r\n<!--                    <ion-label>Minha Empresa</ion-label>-->\r\n<!--                  </ion-segment-button>-->\r\n<!--                </ion-segment>-->\r\n<!--              </ion-item>-->\r\n<!--              <div *ngIf=\"form.controls['tipo_parceiro'].dirty && !form.controls['tipo_parceiro'].valid\">-->\r\n<!--                <p *ngIf=\"form.controls['tipo_parceiro'].errors\">* Obrigatório</p>-->\r\n<!--              </div>-->\r\n\r\n<!--              <ng-container *ngIf=\"!configs.isSignIn\">-->\r\n<!--                <ion-row>-->\r\n<!--                  <ion-col size=\"12\">-->\r\n<!--                    <h5 class=\"mt20\">-->\r\n<!--                      Meu negócio é do tipo-->\r\n<!--                    </h5>-->\r\n<!--                  </ion-col>-->\r\n<!--                  <ion-col size=\"12\" class=\"ion-no-padding\">-->\r\n\r\n<!--                    <ion-item>-->\r\n<!--                      <ion-label>Categorias</ion-label>-->\r\n<!--                      <ionic-selectable-->\r\n<!--                              formControlName=\"categorianegocio\"-->\r\n<!--                              [(ngModel)]=\"categorianegocio\"-->\r\n<!--                              [items]=\"categorianegocios\"-->\r\n<!--                              itemValueField=\"token\"-->\r\n<!--                              itemTextField=\"titulo\"-->\r\n<!--                              closeButtonText=\"Sair\"-->\r\n<!--                              searchPlaceholder=\"Pesquisa Categoria\"-->\r\n<!--                              [searchFailText]=\"'Não Encontrado ! Pesquise Novamente.'\"-->\r\n<!--                              [canSearch]=\"true\"-->\r\n<!--                              [shouldStoreItemValue]=\"true\"-->\r\n<!--                              (onChange)=\"portChange($event)\">-->\r\n<!--                      </ionic-selectable>-->\r\n<!--                    </ion-item>-->\r\n\r\n<!--                    <div *ngIf=\"form.controls['categorianegocio'].dirty && !form.controls['categorianegocio'].valid\">-->\r\n<!--                      <p *ngIf=\"form.controls['categorianegocio'].errors\">* Obrigatório</p>-->\r\n<!--                    </div>-->\r\n<!--                  </ion-col>-->\r\n<!--                </ion-row>-->\r\n<!--              </ng-container>-->\r\n\r\n<!--            </ion-col>-->\r\n          </ion-row>\r\n\r\n\r\n          <ion-row class=\"ion-justify-content-center social\">\r\n            <ion-col size=\"4\" class=\"ion-text-center mt15 \">\r\n              <ion-button shape=\"round\" type=\"submit\" [disabled]=\"!form.valid\" expand=\"block\"\r\n                          size=\"large\">Atualizar Dados\r\n              </ion-button>\r\n            </ion-col>\r\n          </ion-row>\r\n        </form>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/dashboard/edit-parceiro/edit-parceiro-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/dashboard/edit-parceiro/edit-parceiro-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: EditParceiroPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditParceiroPageRoutingModule", function() { return EditParceiroPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _edit_parceiro_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./edit-parceiro.page */ "./src/app/dashboard/edit-parceiro/edit-parceiro.page.ts");




const routes = [
    {
        path: '',
        component: _edit_parceiro_page__WEBPACK_IMPORTED_MODULE_3__["EditParceiroPage"]
    }
];
let EditParceiroPageRoutingModule = class EditParceiroPageRoutingModule {
};
EditParceiroPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EditParceiroPageRoutingModule);



/***/ }),

/***/ "./src/app/dashboard/edit-parceiro/edit-parceiro.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/dashboard/edit-parceiro/edit-parceiro.module.ts ***!
  \*****************************************************************/
/*! exports provided: EditParceiroPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditParceiroPageModule", function() { return EditParceiroPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _edit_parceiro_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit-parceiro-routing.module */ "./src/app/dashboard/edit-parceiro/edit-parceiro-routing.module.ts");
/* harmony import */ var _edit_parceiro_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edit-parceiro.page */ "./src/app/dashboard/edit-parceiro/edit-parceiro.page.ts");
/* harmony import */ var br_mask__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! br-mask */ "./node_modules/br-mask/__ivy_ngcc__/dist/index.js");
/* harmony import */ var ionic_selectable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ionic-selectable */ "./node_modules/ionic-selectable/__ivy_ngcc__/esm2015/ionic-selectable.min.js");









let EditParceiroPageModule = class EditParceiroPageModule {
};
EditParceiroPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _edit_parceiro_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditParceiroPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            br_mask__WEBPACK_IMPORTED_MODULE_7__["BrMaskerModule"],
            ionic_selectable__WEBPACK_IMPORTED_MODULE_8__["IonicSelectableModule"]
        ],
        declarations: [_edit_parceiro_page__WEBPACK_IMPORTED_MODULE_6__["EditParceiroPage"]]
    })
], EditParceiroPageModule);



/***/ }),

/***/ "./src/app/dashboard/edit-parceiro/edit-parceiro.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/dashboard/edit-parceiro/edit-parceiro.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-segment-button {\n  --background: #e7ebed;\n  margin-bottom: 0;\n}\n\nion-segment-button.segment-button-checked {\n  --background: #e7ebed;\n  --background-checked: #286ef2;\n  --background-hover: #286ef2;\n  --color-checked: #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL2VkaXQtcGFyY2Vpcm8vQzpcXFdTXFxQdWJuZXdzXFxwdWJuZXdzLXRhYmxldC9zcmNcXGFwcFxcZGFzaGJvYXJkXFxlZGl0LXBhcmNlaXJvXFxlZGl0LXBhcmNlaXJvLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZGFzaGJvYXJkL2VkaXQtcGFyY2Vpcm8vZWRpdC1wYXJjZWlyby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDRSxxQkFBQTtFQUNBLGdCQUFBO0FDQUY7O0FER0E7RUFDRSxxQkFBQTtFQUNBLDZCQUFBO0VBQ0EsMkJBQUE7RUFDQSxxQkFBQTtBQ0FGIiwiZmlsZSI6InNyYy9hcHAvZGFzaGJvYXJkL2VkaXQtcGFyY2Vpcm8vZWRpdC1wYXJjZWlyby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuaW9uLXNlZ21lbnQtYnV0dG9uIHtcclxuICAtLWJhY2tncm91bmQ6ICNlN2ViZWQ7XHJcbiAgbWFyZ2luLWJvdHRvbTogMDtcclxufVxyXG5cclxuaW9uLXNlZ21lbnQtYnV0dG9uLnNlZ21lbnQtYnV0dG9uLWNoZWNrZWQge1xyXG4gIC0tYmFja2dyb3VuZDogI2U3ZWJlZDtcclxuICAtLWJhY2tncm91bmQtY2hlY2tlZDogIzI4NmVmMjtcclxuICAtLWJhY2tncm91bmQtaG92ZXI6ICMyODZlZjI7XHJcbiAgLS1jb2xvci1jaGVja2VkOiAjZmZmO1xyXG59XHJcbiIsImlvbi1zZWdtZW50LWJ1dHRvbiB7XG4gIC0tYmFja2dyb3VuZDogI2U3ZWJlZDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cblxuaW9uLXNlZ21lbnQtYnV0dG9uLnNlZ21lbnQtYnV0dG9uLWNoZWNrZWQge1xuICAtLWJhY2tncm91bmQ6ICNlN2ViZWQ7XG4gIC0tYmFja2dyb3VuZC1jaGVja2VkOiAjMjg2ZWYyO1xuICAtLWJhY2tncm91bmQtaG92ZXI6ICMyODZlZjI7XG4gIC0tY29sb3ItY2hlY2tlZDogI2ZmZjtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/dashboard/edit-parceiro/edit-parceiro.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/dashboard/edit-parceiro/edit-parceiro.page.ts ***!
  \***************************************************************/
/*! exports provided: EditParceiroPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditParceiroPage", function() { return EditParceiroPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_validador_validador_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/validador/validador.service */ "./src/app/services/validador/validador.service.ts");
/* harmony import */ var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/auth/auth.service */ "./src/app/services/auth/auth.service.ts");
/* harmony import */ var _services_http_http_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/http/http.service */ "./src/app/services/http/http.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var src_app_services_generica_generica_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/generica/generica.service */ "./src/app/services/generica/generica.service.ts");
/* harmony import */ var src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/storage/storage.service */ "./src/app/services/storage/storage.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_services_util_util_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/util/util.service */ "./src/app/services/util/util.service.ts");











class Categoria {
}
let EditParceiroPage = class EditParceiroPage extends src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_8__["StorageService"] {
    constructor(navCtrl, formBuilder, val, authService, genericaService, util, httpService) {
        super(navCtrl, httpService);
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.val = val;
        this.authService = authService;
        this.genericaService = genericaService;
        this.util = util;
        this.httpService = httpService;
        // metodo pra alterna entre inputs do login e criar mesma pagina
        this.configs = {
            isSignIn: true,
            action: 'Carro',
            actionChange: 'Empresa'
        };
        // adicionando campo dinamicamente depedendo da acao
        this.nameControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
    }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
        });
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.createForm();
            this.getDadosCategorias();
        });
    }
    portChange(event) {
        console.log('categorianegocio id :', event.value);
    }
    // Cadastrar parceiro
    AtualizarDados() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(this.form.value);
            this.authService.signup(this.form.value).subscribe((data) => {
                if (data.status) {
                    // this.util.toast({message: data.mensagem, buttons: ['ok']}); //  msg de success aplicativo
                    // this.util.setParceiroStorage(data.token);
                    this.irPagina(`dashboard/perfil`);
                }
                else {
                    this.genericaService.alert({ message: data.mensagem, buttons: ['OK'] }); //  msg erro aplicativo
                    console.log(data.mensagem);
                }
            }, err => {
                this.genericaService.alert({ message: err.message, buttons: ['OK'] }); //  msg de erro api
                console.log('aqui', err);
            });
        });
    }
    // dados parceiro
    getDadosParceiro() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.httpService.getUsuario(this.tokenStorage)
                .subscribe((data) => {
                this.nomeParceiro = data.nome;
                this.emailParceiro = data.email;
                this.cpfParceiro = data.cpf;
                this.celularParceiro = data.celular;
                this.fotoParceiro = data.foto;
                this.tipoParceiro = data.tipo_parceiro;
                this.tokenJson = data.token;
                // verifica se token storage == token json
                console.log('storage -> ', this.tokenStorage, '===', 'json ->', this.tokenJson);
                if (this.tokenStorage != this.tokenJson || !this.tokenStorage) {
                    this.rootPagina(`login`);
                }
            }, err => {
                console.log(err);
            });
        });
    }
    // OcultaInputs(): void {
    //     this.configs.isSignIn = false;
    //     const {isSignIn} = this.configs;
    //     // verifica se e pra remover ou add inputs nameControl
    //     !isSignIn ? this.form.addControl('categorianegocio', this.nameControl) : this.form.removeControl('categorianegocio');
    // }
    //
    // voltaInputs(): void {
    //     this.configs.isSignIn = true;
    //     const {isSignIn} = this.configs;
    //     // verifica se e pra remover ou add inputs nameControl
    //     !isSignIn ? this.form.addControl('categorianegocio', this.nameControl) : this.form.removeControl('categorianegocio');
    // }
    // categorianegocios parceiro
    getDadosCategorias() {
        this.httpService.getCategoriasAnuncios()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1))
            .subscribe((data) => {
            this.categorianegocios = data;
            // console.log('categoria', this.categorianegocios);
        }, err => {
            console.log(err);
        });
    }
    createForm() {
        //  campos do builder para controle
        this.form = this.formBuilder.group({
            nome: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, this.val.nameValid],
            celular: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(14), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, this.val.emailValid],
        });
    }
};
EditParceiroPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["NavController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_validador_validador_service__WEBPACK_IMPORTED_MODULE_3__["ValidadorService"] },
    { type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: src_app_services_generica_generica_service__WEBPACK_IMPORTED_MODULE_7__["GenericaService"] },
    { type: src_app_services_util_util_service__WEBPACK_IMPORTED_MODULE_10__["UtilService"] },
    { type: _services_http_http_service__WEBPACK_IMPORTED_MODULE_5__["HttpService"] }
];
EditParceiroPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit-parceiro',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./edit-parceiro.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/edit-parceiro/edit-parceiro.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./edit-parceiro.page.scss */ "./src/app/dashboard/edit-parceiro/edit-parceiro.page.scss")).default]
    })
], EditParceiroPage);



/***/ })

}]);
//# sourceMappingURL=edit-parceiro-edit-parceiro-module-es2015.js.map