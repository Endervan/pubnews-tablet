(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["anuncio-anuncio-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/anuncio/anuncio.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/anuncio/anuncio.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n    <ion-toolbar id=\"azul-login-page\">\r\n        <div class=\"ion-text-center\">\r\n            <img class=\"pt8\" src=\"../../../assets/logo1.png\">\r\n        </div>\r\n    </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content [fullscreen]=\"true\">\r\n    <ion-grid>\r\n        <ion-row class=\"home\">\r\n            <ion-col>\r\n                <ion-row>\r\n                    <ion-col size=\"6\">\r\n                        <ion-text id=\"text\">\r\n                            Olá, <strong>{{ storageService.nomeParceiro   }}</strong> ! Seja bem vindo.\r\n                        </ion-text>\r\n                    </ion-col>\r\n                    <ion-col class=\"ion-text-center\" size=\"6\" *ngIf=\"anuncioService.qtd ; else total \">\r\n                        Atualizando anúncio {{ anuncioService.qtd}} aguarde ....\r\n                        <ion-progress-bar type=\"indeterminate\" buffer=\"0.15\"></ion-progress-bar>\r\n                    </ion-col>\r\n                    <ng-template #total>\r\n                        <ion-col size=\"6\" class=\"ion-text-end\">\r\n                            <ng-container *ngIf=\"anuncioService.statusAnuncioNow ;else atualizado \">\r\n                                SEM ANÚNCIOS\r\n                            </ng-container>\r\n                        </ion-col>\r\n                    </ng-template>\r\n                    <ng-template #atualizado>\r\n                        ANÚNCIOS ATUALIZADOS\r\n                    </ng-template>\r\n                </ion-row>\r\n                <div class=\"ion-text-center\">\r\n                    <h3>Anúncios exibidos</h3>\r\n                    <hr>\r\n                </div>\r\n                <ion-item class=\"\" lines=\"no-lines\">\r\n                    <ion-label>Hoje</ion-label>\r\n                    <ion-badge *ngIf=\"this.anuncioService.exibicoesHoje;else vazio\"\r\n                               slot=\"end\">{{this.anuncioService.exibicoesHoje }}</ion-badge>\r\n                    <ng-template #vazio slot=\"end\">\r\n                        <ion-badge slot=\"end\">0</ion-badge>\r\n                    </ng-template>\r\n                </ion-item>\r\n\r\n                <ion-item class=\"mt10\" lines=\"no-lines\">\r\n                    <ion-label>Mês</ion-label>\r\n                    <ion-badge *ngIf=\"this.anuncioService.exibicoesMes ;else vazio\"\r\n                               slot=\"end\">{{this.anuncioService.exibicoesMes }}</ion-badge>\r\n                    <ng-template #vazio slot=\"end\">\r\n                        <ion-badge slot=\"end\">0</ion-badge>\r\n                    </ng-template>\r\n                </ion-item>\r\n            </ion-col>\r\n        </ion-row>\r\n\r\n\r\n        <ion-row class=\"ion-justify-content-center social\">\r\n            <ion-col size=\"7\" class=\"ion-text-center\" *ngIf=\"anuncioService.statusAnuncioNow;else iniciar\">\r\n                <b>Não existe anúncios neste local ? Buscando novamente....</b>\r\n            </ion-col>\r\n            <ng-template #iniciar>\r\n                <ion-col size=\"7\" class=\"ion-text-center\">\r\n                    <ion-button shape=\"round\" (click)=\"this.storageService.irPagina('pagina-branco')\" expand=\"block\"\r\n                                [disabled]=\"anuncioService.qtd \">\r\n                        Iniciar Anuncios\r\n                    </ion-button>\r\n                </ion-col>\r\n            </ng-template>\r\n        </ion-row>\r\n\r\n    </ion-grid>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/dashboard/anuncio/anuncio-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/dashboard/anuncio/anuncio-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: AnuncioRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnuncioRoutingModule", function() { return AnuncioRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _anuncio_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./anuncio.page */ "./src/app/dashboard/anuncio/anuncio.page.ts");




const routes = [
    {
        path: '',
        component: _anuncio_page__WEBPACK_IMPORTED_MODULE_3__["Anuncio"],
    }
];
let AnuncioRoutingModule = class AnuncioRoutingModule {
};
AnuncioRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AnuncioRoutingModule);



/***/ }),

/***/ "./src/app/dashboard/anuncio/anuncio.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/dashboard/anuncio/anuncio.module.ts ***!
  \*****************************************************/
/*! exports provided: AnuncioModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnuncioModule", function() { return AnuncioModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _anuncio_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./anuncio.page */ "./src/app/dashboard/anuncio/anuncio.page.ts");
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../explore-container/explore-container.module */ "./src/app/explore-container/explore-container.module.ts");
/* harmony import */ var _anuncio_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./anuncio-routing.module */ "./src/app/dashboard/anuncio/anuncio-routing.module.ts");
/* harmony import */ var _ionic_native_video_player_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/video-player/ngx */ "./node_modules/@ionic-native/video-player/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _pagina_branco_pagina_branco_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../pagina-branco/pagina-branco.page */ "./src/app/pagina-branco/pagina-branco.page.ts");










let AnuncioModule = class AnuncioModule {
};
AnuncioModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"],
            _anuncio_routing_module__WEBPACK_IMPORTED_MODULE_7__["AnuncioRoutingModule"],
        ],
        declarations: [_anuncio_page__WEBPACK_IMPORTED_MODULE_5__["Anuncio"]],
        providers: [_ionic_native_video_player_ngx__WEBPACK_IMPORTED_MODULE_8__["VideoPlayer"], _pagina_branco_pagina_branco_page__WEBPACK_IMPORTED_MODULE_9__["PaginaBrancoPage"]],
        exports: []
    })
], AnuncioModule);



/***/ }),

/***/ "./src/app/dashboard/anuncio/anuncio.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/dashboard/anuncio/anuncio.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: transparent !important;\n}\n\n.home ion-text strong {\n  color: #286ef2;\n  font-size: 15px;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL2FudW5jaW8vQzpcXFdTXFxQdWJuZXdzXFxwdWJuZXdzLXRhYmxldC9zcmNcXGFwcFxcZGFzaGJvYXJkXFxhbnVuY2lvXFxhbnVuY2lvLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZGFzaGJvYXJkL2FudW5jaW8vYW51bmNpby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxvQ0FBQTtBQ0NGOztBREdBO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0FGIiwiZmlsZSI6InNyYy9hcHAvZGFzaGJvYXJkL2FudW5jaW8vYW51bmNpby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudHtcclxuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcblxyXG4uaG9tZSBpb24tdGV4dCBzdHJvbmd7XHJcbiAgY29sb3I6ICMyODZlZjI7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG4iLCJpb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbn1cblxuLmhvbWUgaW9uLXRleHQgc3Ryb25nIHtcbiAgY29sb3I6ICMyODZlZjI7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/dashboard/anuncio/anuncio.page.ts":
/*!***************************************************!*\
  !*** ./src/app/dashboard/anuncio/anuncio.page.ts ***!
  \***************************************************/
/*! exports provided: Anuncio */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Anuncio", function() { return Anuncio; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_util_util_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/util/util.service */ "./src/app/services/util/util.service.ts");
/* harmony import */ var _services_anuncio_anuncio_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/anuncio/anuncio.service */ "./src/app/services/anuncio/anuncio.service.ts");
/* harmony import */ var _services_http_http_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/http/http.service */ "./src/app/services/http/http.service.ts");
/* harmony import */ var _pagina_branco_pagina_branco_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../pagina-branco/pagina-branco.page */ "./src/app/pagina-branco/pagina-branco.page.ts");
/* harmony import */ var src_app_services_generica_generica_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/generica/generica.service */ "./src/app/services/generica/generica.service.ts");
/* harmony import */ var src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/storage/storage.service */ "./src/app/services/storage/storage.service.ts");








let Anuncio = class Anuncio {
    constructor(util, anuncioService, storageService, httpService, pagina, genericaService) {
        this.util = util;
        this.anuncioService = anuncioService;
        this.storageService = storageService;
        this.httpService = httpService;
        this.pagina = pagina;
        this.genericaService = genericaService;
    }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.storageService.parceiroStorage = yield this.storageService.getParceiroStorage('Parceiro');
            this.storageService.nomeParceiro = this.storageService.parceiroStorage.nome;
            yield this.pagina.modoDesligaInsomia();
            yield this.getExibicoes();
        });
    }
    ionViewDidEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // await this.util.verificaLicenca();
            yield this.util.tipoConexao();
        });
    }
    getExibicoes() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.httpService.getSaldoParceiro(this.storageService.tokenStorage)
                .subscribe((result) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                this.anuncioService.exibicoesHoje = result.views_dia;
                this.anuncioService.exibicoesMes = result.views_mes;
                if (!this.anuncioService.exibicoesHoje ||
                    !this.anuncioService.exibicoesMes) {
                    this.loading1 = yield this.genericaService.loading({
                        translucent: true,
                        message: 'Por favor, aguarde ....',
                        showBackdrop: false,
                    });
                    yield this.loading1.dismiss();
                }
            }), err => {
                this.loading1.dismiss();
                console.log(err);
            });
        });
    }
};
Anuncio.ctorParameters = () => [
    { type: _services_util_util_service__WEBPACK_IMPORTED_MODULE_2__["UtilService"] },
    { type: _services_anuncio_anuncio_service__WEBPACK_IMPORTED_MODULE_3__["AnuncioService"] },
    { type: src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_7__["StorageService"] },
    { type: _services_http_http_service__WEBPACK_IMPORTED_MODULE_4__["HttpService"] },
    { type: _pagina_branco_pagina_branco_page__WEBPACK_IMPORTED_MODULE_5__["PaginaBrancoPage"] },
    { type: src_app_services_generica_generica_service__WEBPACK_IMPORTED_MODULE_6__["GenericaService"] }
];
Anuncio = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab1',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./anuncio.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/anuncio/anuncio.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./anuncio.page.scss */ "./src/app/dashboard/anuncio/anuncio.page.scss")).default]
    })
], Anuncio);



/***/ })

}]);
//# sourceMappingURL=anuncio-anuncio-module-es2015.js.map