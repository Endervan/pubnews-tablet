(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["parceiro-spash-spash-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/parceiro/spash/spash.page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/parceiro/spash/spash.page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\r\n  <div class=\"container ion-text-center\">\r\n    <img src=\"assets/logo1.png\">\r\n    <p>version: <b>0.0.1</b></p>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/parceiro/spash/spash-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/parceiro/spash/spash-routing.module.ts ***!
  \********************************************************/
/*! exports provided: SpashPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpashPageRoutingModule", function() { return SpashPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _spash_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./spash.page */ "./src/app/parceiro/spash/spash.page.ts");




const routes = [
    {
        path: '',
        component: _spash_page__WEBPACK_IMPORTED_MODULE_3__["SpashPage"]
    }
];
let SpashPageRoutingModule = class SpashPageRoutingModule {
};
SpashPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SpashPageRoutingModule);



/***/ }),

/***/ "./src/app/parceiro/spash/spash.module.ts":
/*!************************************************!*\
  !*** ./src/app/parceiro/spash/spash.module.ts ***!
  \************************************************/
/*! exports provided: SpashPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpashPageModule", function() { return SpashPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _spash_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./spash-routing.module */ "./src/app/parceiro/spash/spash-routing.module.ts");
/* harmony import */ var _spash_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./spash.page */ "./src/app/parceiro/spash/spash.page.ts");







let SpashPageModule = class SpashPageModule {
};
SpashPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _spash_routing_module__WEBPACK_IMPORTED_MODULE_5__["SpashPageRoutingModule"]
        ],
        declarations: [_spash_page__WEBPACK_IMPORTED_MODULE_6__["SpashPage"]]
    })
], SpashPageModule);



/***/ }),

/***/ "./src/app/parceiro/spash/spash.page.scss":
/*!************************************************!*\
  !*** ./src/app/parceiro/spash/spash.page.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  background: #ffffff;\n}\n\nion-content p {\n  color: #2dd36f;\n}\n\n@keyframes scale-in-center {\n  0% {\n    transform: scale(0);\n    opacity: 1;\n  }\n  100% {\n    transform: scale(1);\n    opacity: 1;\n  }\n}\n\n.container {\n  animation: scale-in-center 2s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;\n}\n\n.container img {\n  height: 100px;\n  margin-top: 15%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFyY2Vpcm8vc3Bhc2gvQzpcXFdTXFxQdWJuZXdzXFxwdWJuZXdzLXRhYmxldC9zcmNcXGFwcFxccGFyY2Vpcm9cXHNwYXNoXFxzcGFzaC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhcmNlaXJvL3NwYXNoL3NwYXNoLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFBO0FDQ0Y7O0FERUE7RUFDRSxjQUFBO0FDQ0Y7O0FERTRKO0VBQTJCO0lBQThCLG1CQUFBO0lBQW1CLFVBQUE7RUNpQnRPO0VEakJnUDtJQUFnQyxtQkFBQTtJQUFtQixVQUFBO0VDc0JuUztBQUNGOztBRHRCQTtFQUMwRSx1RUFBQTtBQ3lCMUU7O0FEeEJFO0VBQ0UsYUFBQTtFQUNBLGVBQUE7QUMwQkoiLCJmaWxlIjoic3JjL2FwcC9wYXJjZWlyby9zcGFzaC9zcGFzaC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudHtcclxuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xyXG59XHJcblxyXG5pb24tY29udGVudCBwe1xyXG4gIGNvbG9yOiAjMmRkMzZmO1xyXG59XHJcblxyXG5ALXdlYmtpdC1rZXlmcmFtZXMgc2NhbGUtaW4tY2VudGVyezAley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKDApO3RyYW5zZm9ybTpzY2FsZSgwKTtvcGFjaXR5OjF9MTAwJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSgxKTt0cmFuc2Zvcm06c2NhbGUoMSk7b3BhY2l0eToxfX1Aa2V5ZnJhbWVzIHNjYWxlLWluLWNlbnRlcnswJXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSgwKTt0cmFuc2Zvcm06c2NhbGUoMCk7b3BhY2l0eToxfTEwMCV7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGUoMSk7dHJhbnNmb3JtOnNjYWxlKDEpO29wYWNpdHk6MX19XHJcbi5jb250YWluZXJ7XHJcbiAgLXdlYmtpdC1hbmltYXRpb246c2NhbGUtaW4tY2VudGVyIDJzIGN1YmljLWJlemllciguMjUsLjQ2LC40NSwuOTQpIGJvdGg7YW5pbWF0aW9uOnNjYWxlLWluLWNlbnRlciAycyBjdWJpYy1iZXppZXIoLjI1LC40NiwuNDUsLjk0KSBib3RoO1xyXG4gIGltZ3tcclxuICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxNSU7XHJcbiAgfVxyXG59XHJcbiIsImlvbi1jb250ZW50IHtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuaW9uLWNvbnRlbnQgcCB7XG4gIGNvbG9yOiAjMmRkMzZmO1xufVxuXG5ALXdlYmtpdC1rZXlmcmFtZXMgc2NhbGUtaW4tY2VudGVyIHtcbiAgMCUge1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgwKTtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDApO1xuICAgIG9wYWNpdHk6IDE7XG4gIH1cbiAgMTAwJSB7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDEpO1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMSk7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxufVxuQGtleWZyYW1lcyBzY2FsZS1pbi1jZW50ZXIge1xuICAwJSB7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDApO1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMCk7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxuICAxMDAlIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMSk7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgxKTtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG59XG4uY29udGFpbmVyIHtcbiAgLXdlYmtpdC1hbmltYXRpb246IHNjYWxlLWluLWNlbnRlciAycyBjdWJpYy1iZXppZXIoMC4yNSwgMC40NiwgMC40NSwgMC45NCkgYm90aDtcbiAgYW5pbWF0aW9uOiBzY2FsZS1pbi1jZW50ZXIgMnMgY3ViaWMtYmV6aWVyKDAuMjUsIDAuNDYsIDAuNDUsIDAuOTQpIGJvdGg7XG59XG4uY29udGFpbmVyIGltZyB7XG4gIGhlaWdodDogMTAwcHg7XG4gIG1hcmdpbi10b3A6IDE1JTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/parceiro/spash/spash.page.ts":
/*!**********************************************!*\
  !*** ./src/app/parceiro/spash/spash.page.ts ***!
  \**********************************************/
/*! exports provided: SpashPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpashPage", function() { return SpashPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_util_util_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/util/util.service */ "./src/app/services/util/util.service.ts");
/* harmony import */ var _services_storage_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/storage/storage.service */ "./src/app/services/storage/storage.service.ts");




let SpashPage = class SpashPage {
    constructor(util, storage) {
        this.util = util;
        this.storage = storage;
        setTimeout(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.storage.voltaPagina('login');
            // await this.util.verificaLicenca();
        }), 3000);
    }
    ngOnInit() {
    }
};
SpashPage.ctorParameters = () => [
    { type: _services_util_util_service__WEBPACK_IMPORTED_MODULE_2__["UtilService"] },
    { type: _services_storage_storage_service__WEBPACK_IMPORTED_MODULE_3__["StorageService"] }
];
SpashPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-spash',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./spash.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/parceiro/spash/spash.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./spash.page.scss */ "./src/app/parceiro/spash/spash.page.scss")).default]
    })
], SpashPage);



/***/ })

}]);
//# sourceMappingURL=parceiro-spash-spash-module-es2015.js.map