(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["perfil-perfil-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/perfil/perfil.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/perfil/perfil.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n    <ion-toolbar id=\"azul-login-page\">\r\n        <div class=\"ion-text-center\">\r\n            <img class=\"pt10\" src=\"../../../assets/logo1.png\">\r\n        </div>\r\n        <app-logout></app-logout>\r\n    </ion-toolbar>\r\n\r\n</ion-header>\r\n<ion-content [fullscreen]=\"true\">\r\n\r\n\r\n    <ion-grid class=\"perfil\">\r\n        <ion-row class=\"ion-justify-content-center\">\r\n            <ion-avatar>\r\n                <img class=\"pt10\" src=\"../../../assets/user.jpg\">\r\n            </ion-avatar>\r\n        </ion-row>\r\n\r\n        <ion-col size=\"12\" class=\"ion-text-center\">\r\n            <ion-title size=\"large\">{{ storageService.nomeParceiro }}</ion-title>\r\n            <h6 *ngIf=\"storageService.emailParceiro\"><b>Email:</b> {{ storageService.emailParceiro }}</h6>\r\n            <h6 *ngIf=\"storageService.celularParceiro\"><b>Contato:</b> {{ storageService.celularParceiro }}</h6>\r\n            <h6 *ngIf=\"storageService.cpfParceiro\"><b>Cpf:</b> {{ storageService.cpfParceiro }}</h6>\r\n            <h6 *ngIf=\"storageService.tipoParceiro\"><b>Tipo parceiro:</b> {{ storageService.tipoParceiro }}</h6>\r\n        </ion-col>\r\n    </ion-grid>\r\n\r\n\r\n<!--    <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\" (click)=\"editarParceiro()\">-->\r\n<!--        <ion-fab-button>-->\r\n<!--            <fa-icon [icon]=\"faUserEdit\" size=\"1x\"></fa-icon>-->\r\n<!--        </ion-fab-button>-->\r\n<!--    </ion-fab>-->\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/dashboard/perfil/perfil-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/dashboard/perfil/perfil-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: PerfilPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilPageRoutingModule", function() { return PerfilPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _perfil_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./perfil.page */ "./src/app/dashboard/perfil/perfil.page.ts");




const routes = [
    {
        path: '',
        component: _perfil_page__WEBPACK_IMPORTED_MODULE_3__["PerfilPage"],
    }
];
let PerfilPageRoutingModule = class PerfilPageRoutingModule {
};
PerfilPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], PerfilPageRoutingModule);



/***/ }),

/***/ "./src/app/dashboard/perfil/perfil.module.ts":
/*!***************************************************!*\
  !*** ./src/app/dashboard/perfil/perfil.module.ts ***!
  \***************************************************/
/*! exports provided: PerfilPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilPageModule", function() { return PerfilPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../explore-container/explore-container.module */ "./src/app/explore-container/explore-container.module.ts");
/* harmony import */ var _perfil_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./perfil.page */ "./src/app/dashboard/perfil/perfil.page.ts");
/* harmony import */ var _perfil_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./perfil-routing.module */ "./src/app/dashboard/perfil/perfil-routing.module.ts");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ "./node_modules/@fortawesome/angular-fontawesome/__ivy_ngcc__/fesm2015/angular-fontawesome.js");
/* harmony import */ var _logout_logout_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../logout/logout.component */ "./src/app/logout/logout.component.ts");











let PerfilPageModule = class PerfilPageModule {
};
PerfilPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _perfil_page__WEBPACK_IMPORTED_MODULE_7__["PerfilPage"] }]),
            _perfil_routing_module__WEBPACK_IMPORTED_MODULE_8__["PerfilPageRoutingModule"],
            _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_9__["FontAwesomeModule"],
        ],
        declarations: [_perfil_page__WEBPACK_IMPORTED_MODULE_7__["PerfilPage"], _logout_logout_component__WEBPACK_IMPORTED_MODULE_10__["LogoutComponent"]],
        exports: [_logout_logout_component__WEBPACK_IMPORTED_MODULE_10__["LogoutComponent"]]
    })
], PerfilPageModule);



/***/ }),

/***/ "./src/app/dashboard/perfil/perfil.page.scss":
/*!***************************************************!*\
  !*** ./src/app/dashboard/perfil/perfil.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9wZXJmaWwvcGVyZmlsLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/dashboard/perfil/perfil.page.ts":
/*!*************************************************!*\
  !*** ./src/app/dashboard/perfil/perfil.page.ts ***!
  \*************************************************/
/*! exports provided: PerfilPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilPage", function() { return PerfilPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_util_util_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/util/util.service */ "./src/app/services/util/util.service.ts");
/* harmony import */ var _services_http_http_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/http/http.service */ "./src/app/services/http/http.service.ts");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/storage/storage.service */ "./src/app/services/storage/storage.service.ts");






let PerfilPage = class PerfilPage {
    constructor(util, httpService, storageService) {
        this.util = util;
        this.httpService = httpService;
        this.storageService = storageService;
        this.faUserEdit = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_4__["faUserEdit"];
    }
    // verifica se usuario esta logado
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.storageService.parceiroStorage = yield this.storageService.getParceiroStorage('Parceiro');
            // populando dados storage
            this.storageService.nomeParceiro = this.storageService.parceiroStorage.nome;
            this.storageService.emailParceiro = this.storageService.parceiroStorage.email;
            this.storageService.cpfParceiro = this.storageService.parceiroStorage.cpf;
            this.storageService.tipoParceiro = this.storageService.parceiroStorage.tipo_Parceiro;
            this.storageService.celularParceiro = this.storageService.parceiroStorage.celular;
        });
    }
    editarParceiro() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.storageService.irPagina(`dashboard/edit-parceiro`);
        });
    }
};
PerfilPage.ctorParameters = () => [
    { type: _services_util_util_service__WEBPACK_IMPORTED_MODULE_2__["UtilService"] },
    { type: _services_http_http_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"] },
    { type: src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_5__["StorageService"] }
];
PerfilPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-perfil',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./perfil.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/perfil/perfil.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./perfil.page.scss */ "./src/app/dashboard/perfil/perfil.page.scss")).default]
    })
], PerfilPage);



/***/ }),

/***/ "./src/app/logout/logout.component.ts":
/*!********************************************!*\
  !*** ./src/app/logout/logout.component.ts ***!
  \********************************************/
/*! exports provided: LogoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutComponent", function() { return LogoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_util_util_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/util/util.service */ "./src/app/services/util/util.service.ts");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _services_generica_generica_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/generica/generica.service */ "./src/app/services/generica/generica.service.ts");
/* harmony import */ var _services_storage_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/storage/storage.service */ "./src/app/services/storage/storage.service.ts");






let LogoutComponent = class LogoutComponent {
    constructor(util, storageService, genericaService) {
        this.util = util;
        this.storageService = storageService;
        this.genericaService = genericaService;
        this.faSignOutAlt = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__["faSignOutAlt"];
    }
    logout() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.genericaService.alert({
                message: 'Deseja Sair Aplicação ? ',
                buttons: [
                    {
                        text: 'Sim',
                        handler: () => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                            // removendo dados parceiro offline
                            yield this.storageService.removeItem('Parceiro');
                            yield this.storageService.voltaPagina('/login');
                        })
                    },
                    'Nao'
                ],
            });
        });
    }
};
LogoutComponent.ctorParameters = () => [
    { type: _services_util_util_service__WEBPACK_IMPORTED_MODULE_2__["UtilService"] },
    { type: _services_storage_storage_service__WEBPACK_IMPORTED_MODULE_5__["StorageService"] },
    { type: _services_generica_generica_service__WEBPACK_IMPORTED_MODULE_4__["GenericaService"] }
];
LogoutComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-logout',
        template: `
        <ion-fab id="logout" vertical="top" horizontal="end" slot="fixed">
            <ion-fab-button>
                <fa-icon (click)="logout()" [icon]="faSignOutAlt" size="lg"></fa-icon>
            </ion-fab-button>
        </ion-fab>
    `,
    })
], LogoutComponent);



/***/ })

}]);
//# sourceMappingURL=perfil-perfil-module-es2015.js.map