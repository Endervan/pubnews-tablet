(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["licenca-licenca-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/licenca/licenca.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/licenca/licenca.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-header>\r\n    <ion-toolbar  class=\"ion-text-center\">\r\n      <ion-buttons slot=\"start\">\r\n        <ion-back-button (click)=\"limpaUser()\" defaultHref=\"/login\"></ion-back-button>\r\n      </ion-buttons>\r\n      <ion-title>licença de Uso</ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n</ion-header>\r\n\r\n<ion-content class=\"inputLogin ion-padding aviso-erro\">\r\n  <ion-grid>\r\n    <ion-row class=\"ion-justify-content-center\">\r\n      <ion-col size-md=\"12\">\r\n        <form id=\"erro\" [formGroup]=\"form\" (ngSubmit)=\"cadastraLicenca()\">\r\n          <ion-item>\r\n            <ion-label position=\"stack\">Codigo</ion-label>\r\n            <ion-input class=\"ion-text-uppercase\"\r\n                       name=\"licenca\"\r\n                       type=\"licenca\"\r\n                       formControlName=\"licenca\"\r\n                       [brmasker]=\"{mask:'0000-0000', len:9, userCaracters: true}\"\r\n                       clearInput clearOnEdit=\"false\">\r\n\r\n            </ion-input>\r\n          </ion-item>\r\n          <div  *ngIf=\"form.controls['licenca'].dirty && !form.controls['licenca'].valid\">\r\n            <p *ngIf=\"form.controls['licenca'].errors.InvalidEmail\">* Lincenca Invalido</p>\r\n          </div>\r\n          <ion-row class=\"ion-justify-content-center social\">\r\n            <ion-col size=\"4\" class=\"ion-text-center mt15 \">\r\n              <ion-button shape=\"round\" type=\"submit\" [disabled]=\"!form.valid\" expand=\"block\" >Enviar\r\n              </ion-button>\r\n            </ion-col>\r\n          </ion-row>\r\n        </form>\r\n\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n  </ion-grid>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/dashboard/licenca/licenca-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/dashboard/licenca/licenca-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: LicencaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LicencaPageRoutingModule", function() { return LicencaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _licenca_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./licenca.page */ "./src/app/dashboard/licenca/licenca.page.ts");




const routes = [
    {
        path: '',
        component: _licenca_page__WEBPACK_IMPORTED_MODULE_3__["LicencaPage"]
    }
];
let LicencaPageRoutingModule = class LicencaPageRoutingModule {
};
LicencaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LicencaPageRoutingModule);



/***/ }),

/***/ "./src/app/dashboard/licenca/licenca.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/dashboard/licenca/licenca.module.ts ***!
  \*****************************************************/
/*! exports provided: LicencaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LicencaPageModule", function() { return LicencaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _licenca_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./licenca-routing.module */ "./src/app/dashboard/licenca/licenca-routing.module.ts");
/* harmony import */ var _licenca_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./licenca.page */ "./src/app/dashboard/licenca/licenca.page.ts");
/* harmony import */ var br_mask__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! br-mask */ "./node_modules/br-mask/__ivy_ngcc__/dist/index.js");








let LicencaPageModule = class LicencaPageModule {
};
LicencaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _licenca_routing_module__WEBPACK_IMPORTED_MODULE_5__["LicencaPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            br_mask__WEBPACK_IMPORTED_MODULE_7__["BrMaskerModule"]
        ],
        declarations: [_licenca_page__WEBPACK_IMPORTED_MODULE_6__["LicencaPage"]]
    })
], LicencaPageModule);



/***/ }),

/***/ "./src/app/dashboard/licenca/licenca.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/dashboard/licenca/licenca.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9saWNlbmNhL2xpY2VuY2EucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/dashboard/licenca/licenca.page.ts":
/*!***************************************************!*\
  !*** ./src/app/dashboard/licenca/licenca.page.ts ***!
  \***************************************************/
/*! exports provided: LicencaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LicencaPage", function() { return LicencaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var src_app_services_generica_generica_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/generica/generica.service */ "./src/app/services/generica/generica.service.ts");
/* harmony import */ var src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/storage/storage.service */ "./src/app/services/storage/storage.service.ts");
/* harmony import */ var _services_http_http_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/http/http.service */ "./src/app/services/http/http.service.ts");
/* harmony import */ var _services_util_util_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/util/util.service */ "./src/app/services/util/util.service.ts");







let LicencaPage = class LicencaPage {
    constructor(formBuilder, storageService, httpService, genericaService, util) {
        this.formBuilder = formBuilder;
        this.storageService = storageService;
        this.httpService = httpService;
        this.genericaService = genericaService;
        this.util = util;
        //  campos do builder para controle
        this.form = formBuilder.group({
            licenca: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(9)]],
        });
    }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.util.tipoConexao();
        });
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
        });
    }
    cadastraLicenca() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const cod = this.form.value;
            yield this.httpService.cadastraLicenca(this.storageService.tokenStorage, cod.licenca)
                .subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                if (data.status === false) {
                    yield this.genericaService.toast({ message: data.mensagem + ' Tente outro Usúario' });
                }
                else {
                    yield this.storageService.setLicencaStorage('tokenLicenca', data.token);
                    yield this.storageService.irPagina('dashboard');
                    yield this.genericaService.toast({ message: data.mensagem, position: 'top' });
                }
            }), err => {
                console.log(err);
            });
        });
    }
    limpaUser() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.storageService.removeItem('Parceiro');
        });
    }
};
LicencaPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_4__["StorageService"] },
    { type: _services_http_http_service__WEBPACK_IMPORTED_MODULE_5__["HttpService"] },
    { type: src_app_services_generica_generica_service__WEBPACK_IMPORTED_MODULE_3__["GenericaService"] },
    { type: _services_util_util_service__WEBPACK_IMPORTED_MODULE_6__["UtilService"] }
];
LicencaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-licenca',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./licenca.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/licenca/licenca.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./licenca.page.scss */ "./src/app/dashboard/licenca/licenca.page.scss")).default]
    })
], LicencaPage);



/***/ })

}]);
//# sourceMappingURL=licenca-licenca-module-es2015.js.map