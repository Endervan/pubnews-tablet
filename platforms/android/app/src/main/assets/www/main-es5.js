function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == typeof h && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw new Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(typeof e + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw new Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == typeof i ? i : String(i); }

function _toPrimitive(t, r) { if ("object" != typeof t || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != typeof i) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports) {
    function webpackEmptyAsyncContext(req) {
      // Here Promise.resolve().then() is used instead of new Promise() to prevent
      // uncaught exception popping up in devtools
      return Promise.resolve().then(function () {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      });
    }

    webpackEmptyAsyncContext.keys = function () {
      return [];
    };

    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    /***/
  },

  /***/
  "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
  /*!*****************************************************************************************************************************************!*\
    !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
    \*****************************************************************************************************************************************/

  /*! no static exports found */

  /***/
  function node_modulesIonicCoreDistEsmLazyRecursiveEntryJs$IncludeEntryJs$ExcludeSystemEntryJs$(module, exports, __webpack_require__) {
    var map = {
      "./ion-action-sheet.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-action-sheet.entry.js", "common", 0],
      "./ion-alert.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-alert.entry.js", "common", 1],
      "./ion-app_8.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-app_8.entry.js", "common", 2],
      "./ion-avatar_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-avatar_3.entry.js", "common", 3],
      "./ion-back-button.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-back-button.entry.js", "common", 4],
      "./ion-backdrop.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-backdrop.entry.js", 5],
      "./ion-button_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-button_2.entry.js", "common", 6],
      "./ion-card_5.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-card_5.entry.js", "common", 7],
      "./ion-checkbox.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-checkbox.entry.js", "common", 8],
      "./ion-chip.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-chip.entry.js", "common", 9],
      "./ion-col_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-col_3.entry.js", 10],
      "./ion-datetime_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-datetime_3.entry.js", "common", 11],
      "./ion-fab_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-fab_3.entry.js", "common", 12],
      "./ion-img.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-img.entry.js", 13],
      "./ion-infinite-scroll_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2.entry.js", 14],
      "./ion-input.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-input.entry.js", "common", 15],
      "./ion-item-option_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item-option_3.entry.js", "common", 16],
      "./ion-item_8.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item_8.entry.js", "common", 17],
      "./ion-loading.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-loading.entry.js", "common", 18],
      "./ion-menu_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-menu_3.entry.js", "common", 19],
      "./ion-modal.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-modal.entry.js", "common", 20],
      "./ion-nav_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-nav_2.entry.js", "common", 21],
      "./ion-popover.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-popover.entry.js", "common", 22],
      "./ion-progress-bar.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-progress-bar.entry.js", "common", 23],
      "./ion-radio_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-radio_2.entry.js", "common", 24],
      "./ion-range.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-range.entry.js", "common", 25],
      "./ion-refresher_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-refresher_2.entry.js", "common", 26],
      "./ion-reorder_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-reorder_2.entry.js", "common", 27],
      "./ion-ripple-effect.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-ripple-effect.entry.js", 28],
      "./ion-route_4.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-route_4.entry.js", "common", 29],
      "./ion-searchbar.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-searchbar.entry.js", "common", 30],
      "./ion-segment_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-segment_2.entry.js", "common", 31],
      "./ion-select_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-select_3.entry.js", "common", 32],
      "./ion-slide_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-slide_2.entry.js", 33],
      "./ion-spinner.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-spinner.entry.js", "common", 34],
      "./ion-split-pane.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-split-pane.entry.js", 35],
      "./ion-tab-bar_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab-bar_2.entry.js", "common", 36],
      "./ion-tab_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab_2.entry.js", "common", 37],
      "./ion-text.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-text.entry.js", "common", 38],
      "./ion-textarea.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-textarea.entry.js", "common", 39],
      "./ion-toast.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toast.entry.js", "common", 40],
      "./ion-toggle.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toggle.entry.js", "common", 41],
      "./ion-virtual-scroll.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-virtual-scroll.entry.js", 42]
    };

    function webpackAsyncContext(req) {
      if (!__webpack_require__.o(map, req)) {
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      var ids = map[req],
          id = ids[0];
      return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function () {
        return __webpack_require__(id);
      });
    }

    webpackAsyncContext.keys = function webpackAsyncContextKeys() {
      return Object.keys(map);
    };

    webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
    module.exports = webpackAsyncContext;
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
  /*!**************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
    \**************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-app>\r\n  <ion-router-outlet></ion-router-outlet>\r\n</ion-app>\r\n";
    /***/
  },

  /***/
  "./src/app/app-routing.module.ts":
  /*!***************************************!*\
    !*** ./src/app/app-routing.module.ts ***!
    \***************************************/

  /*! exports provided: AppRoutingModule */

  /***/
  function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
      return AppRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _services_util_util_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./services/util/util.service */
    "./src/app/services/util/util.service.ts");

    var routes = [{
      path: '',
      redirectTo: 'spash',
      pathMatch: 'full'
    }, {
      path: '',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | dashboard-dashboard-module */
        "dashboard-dashboard-module").then(__webpack_require__.bind(null,
        /*! ./dashboard/dashboard.module */
        "./src/app/dashboard/dashboard.module.ts")).then(function (m) {
          return m.DashboardPageModule;
        });
      }
    }, {
      path: 'login',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | parceiro-login-login-module */
        [__webpack_require__.e("common"), __webpack_require__.e("parceiro-login-login-module")]).then(__webpack_require__.bind(null,
        /*! ./parceiro/login/login.module */
        "./src/app/parceiro/login/login.module.ts")).then(function (m) {
          return m.LoginPageModule;
        });
      }
    }, {
      path: 'cadastro',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | parceiro-cadastro-cadastro-module */
        [__webpack_require__.e("default~edit-parceiro-edit-parceiro-module~licenca-licenca-module~parceiro-cadastro-cadastro-module"), __webpack_require__.e("common"), __webpack_require__.e("parceiro-cadastro-cadastro-module")]).then(__webpack_require__.bind(null,
        /*! ./parceiro/cadastro/cadastro.module */
        "./src/app/parceiro/cadastro/cadastro.module.ts")).then(function (m) {
          return m.CadastroPageModule;
        });
      }
    }, {
      path: 'esqueceu-senha',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | parceiro-esqueceu-senha-esqueceu-senha-module */
        [__webpack_require__.e("common"), __webpack_require__.e("parceiro-esqueceu-senha-esqueceu-senha-module")]).then(__webpack_require__.bind(null,
        /*! ./parceiro/esqueceu-senha/esqueceu-senha.module */
        "./src/app/parceiro/esqueceu-senha/esqueceu-senha.module.ts")).then(function (m) {
          return m.EsqueceuSenhaPageModule;
        });
      }
    }, {
      path: 'pagina-branco',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | pagina-branco-pagina-branco-module */
        [__webpack_require__.e("common"), __webpack_require__.e("pagina-branco-pagina-branco-module")]).then(__webpack_require__.bind(null,
        /*! ./pagina-branco/pagina-branco.module */
        "./src/app/pagina-branco/pagina-branco.module.ts")).then(function (m) {
          return m.PaginaBrancoPageModule;
        });
      },
      canActivate: [_services_util_util_service__WEBPACK_IMPORTED_MODULE_3__["UtilService"]]
    }, {
      path: 'spash',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | parceiro-spash-spash-module */
        "parceiro-spash-spash-module").then(__webpack_require__.bind(null,
        /*! ./parceiro/spash/spash.module */
        "./src/app/parceiro/spash/spash.module.ts")).then(function (m) {
          return m.SpashPageModule;
        });
      }
    }];

    var AppRoutingModule = /*#__PURE__*/_createClass(function AppRoutingModule() {
      _classCallCheck(this, AppRoutingModule);
    });

    AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, {
        preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"]
      })],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AppRoutingModule);
    /***/
  },

  /***/
  "./src/app/app.component.scss":
  /*!************************************!*\
    !*** ./src/app/app.component.scss ***!
    \************************************/

  /*! exports provided: default */

  /***/
  function srcAppAppComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic-native/status-bar/ngx */
    "./node_modules/@ionic-native/status-bar/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _services_util_util_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./services/util/util.service */
    "./src/app/services/util/util.service.ts");
    /* harmony import */


    var _services_anuncio_anuncio_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./services/anuncio/anuncio.service */
    "./src/app/services/anuncio/anuncio.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

    var AppComponent = /*#__PURE__*/function () {
      function AppComponent(platform, statusBar, util, anuncioService, router) {
        _classCallCheck(this, AppComponent);

        this.platform = platform;
        this.statusBar = statusBar;
        this.util = util;
        this.anuncioService = anuncioService;
        this.router = router;
        this.initializeApp();
      }

      _createClass(AppComponent, [{
        key: "initializeApp",
        value: function initializeApp() {
          var _this = this;

          this.platform.ready().then(function () {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
              return _regeneratorRuntime().wrap(function _callee$(_context) {
                while (1) switch (_context.prev = _context.next) {
                  case 0:
                    this.statusBar.styleDefault();
                    this.router.navigateByUrl('spash');
                    this.anuncioService.orientacaoPaisagem();
                    _context.next = 5;
                    return this.util.solicitaPermissao();

                  case 5:
                  case "end":
                    return _context.stop();
                }
              }, _callee, this);
            }));
          });
        }
      }]);

      return AppComponent;
    }();

    AppComponent.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
      }, {
        type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_3__["StatusBar"]
      }, {
        type: _services_util_util_service__WEBPACK_IMPORTED_MODULE_4__["UtilService"]
      }, {
        type: _services_anuncio_anuncio_service__WEBPACK_IMPORTED_MODULE_5__["AnuncioService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
      }];
    };

    AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-root',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./app.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./app.component.scss */
      "./src/app/app.component.scss"))["default"]]
    })], AppComponent);
    /***/
  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic-native/status-bar/ngx */
    "./node_modules/@ionic-native/status-bar/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./app-routing.module */
    "./src/app/app-routing.module.ts");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/facebook/ngx */
    "./node_modules/@ionic-native/facebook/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/google-plus/ngx */
    "./node_modules/@ionic-native/google-plus/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var ionic_selectable__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ionic-selectable */
    "./node_modules/ionic-selectable/__ivy_ngcc__/esm2015/ionic-selectable.min.js");
    /* harmony import */


    var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @fortawesome/angular-fontawesome */
    "./node_modules/@fortawesome/angular-fontawesome/__ivy_ngcc__/fesm2015/angular-fontawesome.js");
    /* harmony import */


    var _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @fortawesome/fontawesome-svg-core */
    "./node_modules/@fortawesome/fontawesome-svg-core/index.es.js");
    /* harmony import */


    var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @fortawesome/free-solid-svg-icons */
    "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
    /* harmony import */


    var _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @ionic-native/downloader/ngx */
    "./node_modules/@ionic-native/downloader/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_video_player_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @ionic-native/video-player/ngx */
    "./node_modules/@ionic-native/video-player/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! @ionic-native/screen-orientation/ngx */
    "./node_modules/@ionic-native/screen-orientation/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! @ionic-native/ionic-webview/ngx */
    "./node_modules/@ionic-native/ionic-webview/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! @ionic-native/android-permissions/ngx */
    "./node_modules/@ionic-native/android-permissions/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_background_mode_ngx__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! @ionic-native/background-mode/ngx */
    "./node_modules/@ionic-native/background-mode/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_insomnia_ngx__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
    /*! @ionic-native/insomnia/ngx */
    "./node_modules/@ionic-native/insomnia/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
    /*! @ionic-native/file/ngx */
    "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js"); // Font Awesome


    _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_13__["library"].add(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__["fas"]);

    _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_13__["library"].add(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__["faDonate"]);

    _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_13__["library"].add(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__["faPlayCircle"]);

    _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_13__["library"].add(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__["faUser"]);

    _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_13__["library"].add(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__["faUserEdit"]);

    _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_13__["library"].add(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__["faSignOutAlt"]);

    var AppModule = /*#__PURE__*/_createClass(function AppModule() {
      _classCallCheck(this, AppModule);
    });

    AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]],
      entryComponents: [],
      imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(), _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"], ionic_selectable__WEBPACK_IMPORTED_MODULE_11__["IonicSelectableModule"], _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_12__["FontAwesomeModule"]],
      providers: [_ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_5__["StatusBar"], _ionic_native_background_mode_ngx__WEBPACK_IMPORTED_MODULE_21__["BackgroundMode"], _ionic_native_insomnia_ngx__WEBPACK_IMPORTED_MODULE_22__["Insomnia"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"], _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_9__["Facebook"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_23__["File"], _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_10__["GooglePlus"], _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"], _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_15__["Downloader"], _ionic_native_video_player_ngx__WEBPACK_IMPORTED_MODULE_16__["VideoPlayer"], _ionic_native_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_17__["ScreenOrientation"], _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_18__["WebView"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_19__["AndroidPermissions"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_20__["Geolocation"], {
        provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"],
        useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"]
      }],
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
    })], AppModule);
    /***/
  },

  /***/
  "./src/app/modal/modal.component.ts":
  /*!******************************************!*\
    !*** ./src/app/modal/modal.component.ts ***!
    \******************************************/

  /*! exports provided: ModalComponent */

  /***/
  function srcAppModalModalComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ModalComponent", function () {
      return ModalComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic-native/ionic-webview/ngx */
    "./node_modules/@ionic-native/ionic-webview/__ivy_ngcc__/ngx/index.js");

    var ModalComponent = /*#__PURE__*/function () {
      function ModalComponent(modalCtrl, webview) {
        _classCallCheck(this, ModalComponent);

        this.modalCtrl = modalCtrl;
        this.webview = webview;
      } // resolve caminho images dispositivo


      _createClass(ModalComponent, [{
        key: "pathForImage",
        value: function pathForImage(img) {
          if (img === null) {
            return '';
          } else {
            // return this.webview.convertFileSrc(img);
            // tava assim antes
            return this.webview.convertFileSrc(img);
          }
        }
      }, {
        key: "delay",
        value: function delay(ms) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee3() {
            var _this2 = this;

            return _regeneratorRuntime().wrap(function _callee3$(_context3) {
              while (1) switch (_context3.prev = _context3.next) {
                case 0:
                  return _context3.abrupt("return", new Promise(function (resolve) {
                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
                      return _regeneratorRuntime().wrap(function _callee2$(_context2) {
                        while (1) switch (_context2.prev = _context2.next) {
                          case 0:
                            return _context2.abrupt("return", setTimeout(resolve, ms));

                          case 1:
                          case "end":
                            return _context2.stop();
                        }
                      }, _callee2);
                    }));
                  }));

                case 1:
                case "end":
                  return _context3.stop();
              }
            }, _callee3);
          }));
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this3 = this;

          this.delay(this.time).then(function () {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee4() {
              return _regeneratorRuntime().wrap(function _callee4$(_context4) {
                while (1) switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.dismiss();

                  case 2:
                  case "end":
                    return _context4.stop();
                }
              }, _callee4, this);
            }));
          });
        }
      }, {
        key: "dismiss",
        value: function dismiss() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee5() {
            return _regeneratorRuntime().wrap(function _callee5$(_context5) {
              while (1) switch (_context5.prev = _context5.next) {
                case 0:
                  _context5.next = 2;
                  return this.modalCtrl.dismiss({
                    dismissed: true
                  });

                case 2:
                case "end":
                  return _context5.stop();
              }
            }, _callee5, this);
          }));
        }
      }]);

      return ModalComponent;
    }();

    ModalComponent.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_3__["WebView"]
      }];
    };

    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], ModalComponent.prototype, "fileImage", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], ModalComponent.prototype, "time", void 0);
    ModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-modal',
      template: "\n        <ion-content fullscreen  style=\"--background: transparent\">\n            <ion-col class=\"ion-no-padding\" size=\"12\" size-md=\"12\" size-xs=\"12\" size-lg=\"12\">\n                <ion-img id=\"modal_image\" [src]=\"this.pathForImage(fileImage)\"></ion-img>\n            </ion-col>\n        </ion-content>\n    "
    })], ModalComponent);
    /***/
  },

  /***/
  "./src/app/services/anuncio/anuncio.service.ts":
  /*!*****************************************************!*\
    !*** ./src/app/services/anuncio/anuncio.service.ts ***!
    \*****************************************************/

  /*! exports provided: AnuncioService */

  /***/
  function srcAppServicesAnuncioAnuncioServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AnuncioService", function () {
      return AnuncioService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @capacitor/core */
    "./node_modules/@capacitor/core/dist/esm/index.js");
    /* harmony import */


    var _ionic_native_background_mode_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic-native/background-mode/ngx */
    "./node_modules/@ionic-native/background-mode/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic-native/downloader/ngx */
    "./node_modules/@ionic-native/downloader/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic-native/file/ngx */
    "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic-native/ionic-webview/ngx */
    "./node_modules/@ionic-native/ionic-webview/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/screen-orientation/ngx */
    "./node_modules/@ionic-native/screen-orientation/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_video_player_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/video-player/ngx */
    "./node_modules/@ionic-native/video-player/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _modal_modal_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../../modal/modal.component */
    "./src/app/modal/modal.component.ts");
    /* harmony import */


    var _generica_generica_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../generica/generica.service */
    "./src/app/services/generica/generica.service.ts");
    /* harmony import */


    var _http_http_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../http/http.service */
    "./src/app/services/http/http.service.ts");
    /* harmony import */


    var _storage_storage_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ../storage/storage.service */
    "./src/app/services/storage/storage.service.ts");

    var _capacitor_core__WEBP = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"],
        Storage = _capacitor_core__WEBP.Storage,
        Filesystem = _capacitor_core__WEBP.Filesystem,
        Geolocation = _capacitor_core__WEBP.Geolocation,
        Network = _capacitor_core__WEBP.Network;

    var AnuncioService = /*#__PURE__*/function () {
      function AnuncioService(videoPlayerIonic, screenOrientation, file, modalController, webview, httpService, navCtrl, downloader, platform, backgroundMode, storageService, genericaService) {
        var _this4 = this;

        _classCallCheck(this, AnuncioService);

        this.videoPlayerIonic = videoPlayerIonic;
        this.screenOrientation = screenOrientation;
        this.file = file;
        this.modalController = modalController;
        this.webview = webview;
        this.httpService = httpService;
        this.navCtrl = navCtrl;
        this.downloader = downloader;
        this.platform = platform;
        this.backgroundMode = backgroundMode;
        this.storageService = storageService;
        this.genericaService = genericaService;
        this.qtd = 0;
        this.total = 0;
        this.velocidade = 0;
        this.statusAnuncioNow = false;
        this.atualizado = false;
        this.volume = 0.0; // botao volta para anuncios fazendo 2 click

        this.storageService.setStorageAnuncio(false);
        this.platform.backButton.subscribeWithPriority(10, function () {
          _this4.storageService.setStorageAnuncio(true);

          _this4.storageService.voltaPagina('dashboard').then();
        });
      } // buscando velocidade geolocation speed ja convertido por km/h


      _createClass(AnuncioService, [{
        key: "watchPosition",
        value: function watchPosition() {
          var _this5 = this;

          Geolocation.watchPosition({
            enableHighAccuracy: true
          }, function (position) {
            var novoValor = position.coords.speed * 3.6;
            _this5.velocidade = Math.round(novoValor);
          });
        }
      }, {
        key: "buscaAnuncioArea",
        value: function buscaAnuncioArea(lat, _long, token) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee9() {
            var _this6 = this;

            var loading;
            return _regeneratorRuntime().wrap(function _callee9$(_context9) {
              while (1) switch (_context9.prev = _context9.next) {
                case 0:
                  _context9.next = 2;
                  return this.genericaService.loading({
                    translucent: true,
                    message: 'Buscando Anuncios, Por favor, aguarde ....',
                    showBackdrop: false
                  });

                case 2:
                  loading = _context9.sent;
                  this.tipoConexao().then(function () {
                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this6, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee8() {
                      var _this7 = this;

                      return _regeneratorRuntime().wrap(function _callee8$(_context8) {
                        while (1) switch (_context8.prev = _context8.next) {
                          case 0:
                            if (!(this.newStatus.connected === true)) {
                              _context8.next = 6;
                              break;
                            }

                            // veiculo em movimento
                            this.watchPosition();
                            console.log(this.velocidade, 'km/h');

                            if (this.velocidade >= 4) {
                              this.httpService.getAnuncios(lat, _long, token).subscribe(function (data) {
                                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this7, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee6() {
                                  return _regeneratorRuntime().wrap(function _callee6$(_context6) {
                                    while (1) switch (_context6.prev = _context6.next) {
                                      case 0:
                                        _context6.next = 2;
                                        return this.playEdit(lat, _long, token, data);

                                      case 2:
                                        _context6.next = 4;
                                        return loading.dismiss();

                                      case 4:
                                      case "end":
                                        return _context6.stop();
                                    }
                                  }, _callee6, this);
                                }));
                              }, function (err) {
                                loading.dismiss();
                                console.log('nao achou dados getAnuncios', err);
                              }); // somente pelo celular
                            } else if (this.velocidade <= 3) {
                              this.httpService.getAnunciosDadosMoveis(lat, _long, token).subscribe(function (data) {
                                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this7, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee7() {
                                  return _regeneratorRuntime().wrap(function _callee7$(_context7) {
                                    while (1) switch (_context7.prev = _context7.next) {
                                      case 0:
                                        _context7.next = 2;
                                        return loading.dismiss();

                                      case 2:
                                        _context7.next = 4;
                                        return this.playEdit(lat, _long, token, data);

                                      case 4:
                                      case "end":
                                        return _context7.stop();
                                    }
                                  }, _callee7, this);
                                }));
                              }, function (err) {
                                loading.dismiss();
                                console.log('nao achou dados getAnunciosPubNews', err);
                              });
                            } // offline


                            _context8.next = 8;
                            break;

                          case 6:
                            _context8.next = 8;
                            return this.getTipoAnuncioPubnews();

                          case 8:
                          case "end":
                            return _context8.stop();
                        }
                      }, _callee8, this);
                    }));
                  });
                  _context9.next = 6;
                  return loading.dismiss();

                case 6:
                case "end":
                  return _context9.stop();
              }
            }, _callee9, this);
          }));
        } // play de acordo

      }, {
        key: "playEdit",
        value: function playEdit(lat, _long2, token, data) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee10() {
            var totalTempo, i, statusAnuncio;
            return _regeneratorRuntime().wrap(function _callee10$(_context10) {
              while (1) switch (_context10.prev = _context10.next) {
                case 0:
                  totalTempo = 0;

                  if (!(data.status !== false)) {
                    _context10.next = 29;
                    break;
                  }

                  i = 0;

                case 3:
                  if (!(i < Object.keys(data).length)) {
                    _context10.next = 24;
                    break;
                  }

                  _context10.next = 6;
                  return this.storageService.getParaAnuncio();

                case 6:
                  statusAnuncio = _context10.sent;

                  if (!(statusAnuncio === true)) {
                    _context10.next = 9;
                    break;
                  }

                  return _context10.abrupt("break", 24);

                case 9:
                  _context10.next = 11;
                  return this.storageService.vericaArquivoStorage(data[i].token);

                case 11:
                  this.anuncioStorage = _context10.sent;
                  _context10.next = 14;
                  return this.checandoFileExiste(_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].AndroidDataPatch, data[i].arquivo);

                case 14:
                  this.ExisteDisposito = _context10.sent;
                  totalTempo = +totalTempo + data[i].tempo_duracao;

                  if (!(this.anuncioStorage || this.ExisteDisposito)) {
                    _context10.next = 21;
                    break;
                  }

                  _context10.next = 19;
                  return this.exibirAnuncio(data[i]);

                case 19:
                  _context10.next = 21;
                  return this.armazenaExibicaoAnuncio(lat, _long2, token, data[i].token);

                case 21:
                  i++;
                  _context10.next = 3;
                  break;

                case 24:
                  if (!totalTempo) {
                    _context10.next = 27;
                    break;
                  }

                  _context10.next = 27;
                  return this.BuscarNovamente();

                case 27:
                  _context10.next = 32;
                  break;

                case 29:
                  console.log('aquii');
                  _context10.next = 32;
                  return this.getTipoAnuncioPubnews();

                case 32:
                case "end":
                  return _context10.stop();
              }
            }, _callee10, this);
          }));
        }
      }, {
        key: "exibirAnuncio",
        value: function exibirAnuncio(dados) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee11() {
            return _regeneratorRuntime().wrap(function _callee11$(_context11) {
              while (1) switch (_context11.prev = _context11.next) {
                case 0:
                  // caminho dispositivo completo no dispositivo e storage
                  if (this.ExisteDisposito === true && this.anuncioStorage) {
                    this.patchFile = _environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].AndroidDataPatch + dados.arquivo;
                    this.pathForImage(this.patchFile);
                  } else {
                    // monta imagem vindo servidor
                    this.patchFile = _environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].apiUrlArquivo + dados.arquivo;
                  }

                  this.time = dados.tempo_duracao; // pega time video vem json

                  if (!(dados.arquivo_is_image === 0)) {
                    _context11.next = 12;
                    break;
                  }

                  console.log(this.patchFile, 'play  tipo==', dados.arquivo_is_image);
                  _context11.next = 6;
                  return this.playVideo(this.patchFile);

                case 6:
                  _context11.next = 8;
                  return this.delay(this.time);

                case 8:
                  _context11.next = 10;
                  return this.closeVideo();

                case 10:
                  _context11.next = 17;
                  break;

                case 12:
                  console.log(this.patchFile, ' images tipo==', dados.arquivo_is_image);
                  _context11.next = 15;
                  return this.presentModal(this.patchFile, this.time);

                case 15:
                  _context11.next = 17;
                  return this.delay(this.time);

                case 17:
                case "end":
                  return _context11.stop();
              }
            }, _callee11, this);
          }));
        } // checando se existe anuncios no dispositivo

      }, {
        key: "checandoFileExiste",
        value: function checandoFileExiste(path, arquivo) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee14() {
            var _this8 = this;

            return _regeneratorRuntime().wrap(function _callee14$(_context14) {
              while (1) switch (_context14.prev = _context14.next) {
                case 0:
                  this.file.checkFile(path, arquivo).then(function (result) {
                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this8, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee12() {
                      return _regeneratorRuntime().wrap(function _callee12$(_context12) {
                        while (1) switch (_context12.prev = _context12.next) {
                          case 0:
                            this.retornoFile = result;
                            console.log(this.retornoFile, 'arquivo existe dispositivo'); // await this.storageService.setAnunciosStorage(token, arquivo, isImage, token, time, tipo);

                          case 2:
                          case "end":
                            return _context12.stop();
                        }
                      }, _callee12, this);
                    }));
                  })["catch"](function (err) {
                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this8, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee13() {
                      return _regeneratorRuntime().wrap(function _callee13$(_context13) {
                        while (1) switch (_context13.prev = _context13.next) {
                          case 0:
                            if (!(err.message === 'NOT_FOUND_ERR')) {
                              _context13.next = 5;
                              break;
                            }

                            this.retornoFile = false;
                            console.log(this.retornoFile, 'arquivo nao existe dispositivo');
                            _context13.next = 5;
                            return this.DonwloadAnuncios(_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].apiUrlArquivo + arquivo, arquivo);

                          case 5:
                          case "end":
                            return _context13.stop();
                        }
                      }, _callee13, this);
                    }));
                  });
                  return _context14.abrupt("return", this.retornoFile);

                case 2:
                case "end":
                  return _context14.stop();
              }
            }, _callee14, this);
          }));
        }
      }, {
        key: "getFileAndroid",
        value: function getFileAndroid(patch, arquivo) {
          this.file.getFile(patch, arquivo, {}).then(function (result) {
            console.log(result);
          })["catch"](function (err) {
            return console.log('file nao existe', err);
          });
        }
      }, {
        key: "AnunciosParceiroDownloder",
        value: function AnunciosParceiroDownloder(lat, _long3, token) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee16() {
            var _this9 = this;

            return _regeneratorRuntime().wrap(function _callee16$(_context16) {
              while (1) switch (_context16.prev = _context16.next) {
                case 0:
                  this.storageAnuncio = token;

                  if (token) {
                    this.httpService.getArquivosAnuncios(lat, _long3, token).subscribe(function (data) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this9, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee15() {
                        var i;
                        return _regeneratorRuntime().wrap(function _callee15$(_context15) {
                          while (1) switch (_context15.prev = _context15.next) {
                            case 0:
                              if (!(data.status !== false)) {
                                _context15.next = 18;
                                break;
                              }

                              i = 0;

                            case 2:
                              if (!(i < Object.keys(data).length)) {
                                _context15.next = 14;
                                break;
                              }

                              // //////// dados pra donwload //////////
                              this.arquivoAnunciojson = data[i].arquivo;
                              this.urlAnuncio = data[i].url_arquivo;
                              this.arquivoIsImage = data[i].arquivo_is_image;
                              this.tokenAnuncio = data[i].token;
                              this.tipoAnuncio = data[i].tipo_anuncio;
                              this.timeAnuncio = data[i].tempo_duracao; // verifica arquivos storage antes de downloader

                              _context15.next = 11;
                              return this.vericarArquivos();

                            case 11:
                              i++;
                              _context15.next = 2;
                              break;

                            case 14:
                              this.qtd = false;
                              this.atualizado = true;
                              _context15.next = 22;
                              break;

                            case 18:
                              this.statusAnuncioNow = true;
                              _context15.next = 21;
                              return this.BuscarNovamente();

                            case 21:
                              console.log('Nenhum anúncio disponível.');

                            case 22:
                            case "end":
                              return _context15.stop();
                          }
                        }, _callee15, this);
                      }));
                    }, function (err) {
                      console.log('nao achou dados', err);
                    });
                  } else {
                    console.log('error token invalido');
                  }

                case 2:
                case "end":
                  return _context16.stop();
              }
            }, _callee16, this);
          }));
        } // funcao downloader native

      }, {
        key: "DonwloadAnuncios",
        value: function DonwloadAnuncios(url, arquivo) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee18() {
            var _this10 = this;

            return _regeneratorRuntime().wrap(function _callee18$(_context18) {
              while (1) switch (_context18.prev = _context18.next) {
                case 0:
                  this.request = {
                    uri: url,
                    title: arquivo,
                    description: '',
                    mimeType: '',
                    visibleInDownloadsUi: false,
                    // notificationVisibility: NotificationVisibility.Visible, // define se mostra ou nao notificação
                    // destinationInExternalPublicDir: {dirType: 'Pubnews', subPath: arquivo},
                    destinationInExternalFilesDir: {
                      dirType: 'Pubnews',
                      subPath: arquivo
                    },
                    destinationUri: 'Pubnews'
                  };
                  this.downloader.download(this.request).then(function (location) {
                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this10, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee17() {
                      return _regeneratorRuntime().wrap(function _callee17$(_context17) {
                        while (1) switch (_context17.prev = _context17.next) {
                          case 0:
                            console.log('Baixando download em: ', location);

                          case 1:
                          case "end":
                            return _context17.stop();
                        }
                      }, _callee17);
                    }));
                  })["catch"](function (error) {
                    console.log('ERRO NO download', error);
                    return error;
                  });

                case 2:
                case "end":
                  return _context18.stop();
              }
            }, _callee18, this);
          }));
        } // verifica   storage e no dispositivo

      }, {
        key: "vericarArquivos",
        value: function vericarArquivos() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee20() {
            var _this11 = this;

            return _regeneratorRuntime().wrap(function _callee20$(_context20) {
              while (1) switch (_context20.prev = _context20.next) {
                case 0:
                  _context20.next = 2;
                  return this.storageService.getItemAnuncio(this.tokenAnuncio);

                case 2:
                  this.arquivoAnuncioStorage = _context20.sent;
                  _context20.next = 5;
                  return this.checandoFileExiste(_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].AndroidDataPatch, this.arquivoAnunciojson);

                case 5:
                  this.ExisteDisposito = _context20.sent;

                  if (!(this.arquivoAnuncioStorage !== this.arquivoAnunciojson)) {
                    _context20.next = 17;
                    break;
                  }

                  console.log('ativando downloder  storage !== json ', this.arquivoAnuncioStorage === this.arquivoAnunciojson);
                  this.qtd = this.qtd + 1;
                  _context20.next = 11;
                  return this.delay(this.timeAnuncio);

                case 11:
                  console.log("contador de donwloder", this.qtd); // arquivos baixados celular dinamicamente

                  this.tipoConexao().then(function () {
                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this11, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee19() {
                      return _regeneratorRuntime().wrap(function _callee19$(_context19) {
                        while (1) switch (_context19.prev = _context19.next) {
                          case 0:
                            if (!(this.newStatus.connectionType === 'wifi' && this.ExisteDisposito === false)) {
                              _context19.next = 5;
                              break;
                            }

                            _context19.next = 3;
                            return this.DonwloadAnuncios(this.urlAnuncio, this.arquivoAnunciojson);

                          case 3:
                            _context19.next = 9;
                            break;

                          case 5:
                            if (!(this.newStatus.connectionType === '4g' || this.newStatus.connectionType === '3g' && this.arquivoIsImage === '1' && this.ExisteDisposito === false)) {
                              _context19.next = 9;
                              break;
                            }

                            console.log('somente baixa se for image para cellular');
                            _context19.next = 9;
                            return this.DonwloadAnuncios(this.urlAnuncio, this.arquivoAnunciojson);

                          case 9:
                          case "end":
                            return _context19.stop();
                        }
                      }, _callee19, this);
                    }));
                  });
                  _context20.next = 15;
                  return this.storageService.setAnunciosStorage(this.tokenAnuncio, this.arquivoAnunciojson, this.arquivoIsImage, this.tokenAnuncio, this.timeAnuncio, this.tipoAnuncio);

                case 15:
                  _context20.next = 18;
                  break;

                case 17:
                  console.log('nao baixar storage == json', this.arquivoAnuncioStorage === this.arquivoAnunciojson);

                case 18:
                case "end":
                  return _context20.stop();
              }
            }, _callee20, this);
          }));
        }
      }, {
        key: "deletarArquivosCelular",
        value: function deletarArquivosCelular(patch) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee21() {
            return _regeneratorRuntime().wrap(function _callee21$(_context21) {
              while (1) switch (_context21.prev = _context21.next) {
                case 0:
                  _context21.next = 2;
                  return Filesystem.deleteFile({
                    path: "".concat(this.pathForImage(patch))
                  }).then(function (result) {
                    return console.log(result);
                  })["catch"](function (e) {
                    return console.log(e);
                  });

                case 2:
                case "end":
                  return _context21.stop();
              }
            }, _callee21, this);
          }));
        } // resolve caminho images dispositivo

      }, {
        key: "pathForImage",
        value: function pathForImage(img) {
          if (img === null) {
            return '';
          } else {
            // return this.webview.convertFileSrc(img);
            // tava assim antes
            return this.webview.convertFileSrc(img);
          }
        }
      }, {
        key: "presentModal",
        value: function presentModal(fileImage1, time1) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee22() {
            var modal;
            return _regeneratorRuntime().wrap(function _callee22$(_context22) {
              while (1) switch (_context22.prev = _context22.next) {
                case 0:
                  _context22.next = 2;
                  return this.modalController.create({
                    component: _modal_modal_component__WEBPACK_IMPORTED_MODULE_11__["ModalComponent"],
                    cssClass: 'my-custom-class-modal',
                    backdropDismiss: true,
                    componentProps: {
                      fileImage: fileImage1,
                      time: time1
                    }
                  });

                case 2:
                  modal = _context22.sent;
                  _context22.next = 5;
                  return modal.present();

                case 5:
                  return _context22.abrupt("return", _context22.sent);

                case 6:
                case "end":
                  return _context22.stop();
              }
            }, _callee22, this);
          }));
        } ///// Storage /////////////////////////

      }, {
        key: "delay",
        value: function delay(ms) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee24() {
            var _this12 = this;

            return _regeneratorRuntime().wrap(function _callee24$(_context24) {
              while (1) switch (_context24.prev = _context24.next) {
                case 0:
                  return _context24.abrupt("return", new Promise(function (resolve) {
                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this12, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee23() {
                      return _regeneratorRuntime().wrap(function _callee23$(_context23) {
                        while (1) switch (_context23.prev = _context23.next) {
                          case 0:
                            return _context23.abrupt("return", setTimeout(resolve, ms));

                          case 1:
                          case "end":
                            return _context23.stop();
                        }
                      }, _callee23);
                    }));
                  }));

                case 1:
                case "end":
                  return _context24.stop();
              }
            }, _callee24);
          }));
        }
      }, {
        key: "orientacaoPaisagem",
        value: function orientacaoPaisagem() {
          // orientacao paisagem
          this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE).then(function () {})["catch"](function (e) {
            return console.log(e);
          });
        }
      }, {
        key: "orientacaoMobile",
        value: function orientacaoMobile() {
          // destrava paisagem
          this.screenOrientation.unlock();
          console.log('ativado orientacaoMobile');
        }
      }, {
        key: "playVideo",
        value: function playVideo(url) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee25() {
            var options;
            return _regeneratorRuntime().wrap(function _callee25$(_context25) {
              while (1) switch (_context25.prev = _context25.next) {
                case 0:
                  options = {
                    volume: this.volume
                  };
                  this.videoPlayerIonic.play(url, options).then(function (r) {
                    return console.log('video completed', r);
                  })["catch"](function (err) {
                    return console.log(err, 'erro fazer play');
                  });

                case 2:
                case "end":
                  return _context25.stop();
              }
            }, _callee25, this);
          }));
        }
      }, {
        key: "MutePlayVideo",
        value: function MutePlayVideo() {
          this.volume = 0.0; // mute
        }
      }, {
        key: "closeVideo",
        value: function closeVideo() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee26() {
            return _regeneratorRuntime().wrap(function _callee26$(_context26) {
              while (1) switch (_context26.prev = _context26.next) {
                case 0:
                  this.videoPlayerIonic.close();

                case 1:
                case "end":
                  return _context26.stop();
              }
            }, _callee26, this);
          }));
        } // anuncio exibido

      }, {
        key: "armazenaExibicaoAnuncio",
        value: function armazenaExibicaoAnuncio(lat, _long4, tokenStorage, tokenAnuncio) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee28() {
            var _this13 = this;

            return _regeneratorRuntime().wrap(function _callee28$(_context28) {
              while (1) switch (_context28.prev = _context28.next) {
                case 0:
                  this.storageService.getTokenLicenca('tokenLicenca').then(function (licenca) {
                    _this13.httpService.armazenaExibicaoAnuncio(lat, _long4, tokenStorage, tokenAnuncio, licenca).subscribe(function (data) {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this13, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee27() {
                        return _regeneratorRuntime().wrap(function _callee27$(_context27) {
                          while (1) switch (_context27.prev = _context27.next) {
                            case 0:
                              if (data.status === true) {
                                console.log('anuncio contabilizado');
                              }

                            case 1:
                            case "end":
                              return _context27.stop();
                          }
                        }, _callee27);
                      }));
                    }, function (err) {
                      return console.log(err);
                    });
                  });

                case 1:
                case "end":
                  return _context28.stop();
              }
            }, _callee28, this);
          }));
        }
      }, {
        key: "backgroundModeNative",
        value: function backgroundModeNative() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee29() {
            return _regeneratorRuntime().wrap(function _callee29$(_context29) {
              while (1) switch (_context29.prev = _context29.next) {
                case 0:
                  this.backgroundMode.enable();

                case 1:
                case "end":
                  return _context29.stop();
              }
            }, _callee29, this);
          }));
        } //////////////////////////// retornando some arquivos offline sistema empresa \\\\\\\\\\\\\\\\\|||||||||||||||||||||||||||||||||

      }, {
        key: "getTipoAnuncioPubnews",
        value: function getTipoAnuncioPubnews() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee30() {
            var totalTempo, _yield$Storage$keys, keys, i, statusAnuncio, dado, dados, patchFile;

            return _regeneratorRuntime().wrap(function _callee30$(_context30) {
              while (1) switch (_context30.prev = _context30.next) {
                case 0:
                  totalTempo = 0;
                  _context30.next = 3;
                  return Storage.keys();

                case 3:
                  _yield$Storage$keys = _context30.sent;
                  keys = _yield$Storage$keys.keys;
                  i = 0;

                case 6:
                  if (!(i < keys.length)) {
                    _context30.next = 38;
                    break;
                  }

                  _context30.next = 9;
                  return this.storageService.getParaAnuncio();

                case 9:
                  statusAnuncio = _context30.sent;

                  if (!(statusAnuncio === true)) {
                    _context30.next = 12;
                    break;
                  }

                  return _context30.abrupt("break", 38);

                case 12:
                  _context30.next = 14;
                  return Storage.get({
                    key: keys[i]
                  });

                case 14:
                  dado = _context30.sent;
                  dados = JSON.parse(dado.value);

                  if (!(dados.tipo_anuncio === '2')) {
                    _context30.next = 35;
                    break;
                  }

                  totalTempo = +totalTempo + dados.tempo_duracao; // caminho dispositivo completo

                  patchFile = _environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].AndroidDataPatch + dados.arquivo;
                  this.pathForImage(patchFile);

                  if (!(dados.tipoArquivo === 0)) {
                    _context30.next = 30;
                    break;
                  }

                  console.log(patchFile, 'play offline ---- video tipo==', dados.tipoArquivo);
                  _context30.next = 24;
                  return this.playVideo(patchFile);

                case 24:
                  _context30.next = 26;
                  return this.delay(dados.tempo_duracao);

                case 26:
                  _context30.next = 28;
                  return this.closeVideo();

                case 28:
                  _context30.next = 35;
                  break;

                case 30:
                  console.log(patchFile, '  play offline =----- images tipo==', dados.tipoArquivo);
                  _context30.next = 33;
                  return this.presentModal(patchFile, dados.tempo_duracao);

                case 33:
                  _context30.next = 35;
                  return this.delay(dados.tempo_duracao);

                case 35:
                  i++;
                  _context30.next = 6;
                  break;

                case 38:
                  console.log('acabou loop offline', totalTempo);

                  if (!totalTempo) {
                    _context30.next = 42;
                    break;
                  }

                  _context30.next = 42;
                  return this.BuscarNovamente();

                case 42:
                case "end":
                  return _context30.stop();
              }
            }, _callee30, this);
          }));
        } // geoloaction Com Capacitor

      }, {
        key: "getPositicao",
        value: function getPositicao() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee31() {
            var coordinates;
            return _regeneratorRuntime().wrap(function _callee31$(_context31) {
              while (1) switch (_context31.prev = _context31.next) {
                case 0:
                  _context31.prev = 0;
                  _context31.next = 3;
                  return Geolocation.getCurrentPosition({
                    enableHighAccuracy: true
                  });

                case 3:
                  coordinates = _context31.sent;
                  this.latitude = coordinates.coords.latitude;
                  this.longitude = coordinates.coords.longitude; // return coordinates

                  _context31.next = 13;
                  break;

                case 8:
                  _context31.prev = 8;
                  _context31.t0 = _context31["catch"](0);
                  console.log('erro a busca coordenadas,', _context31.t0);
                  _context31.next = 13;
                  return this.getPositicao();

                case 13:
                case "end":
                  return _context31.stop();
              }
            }, _callee31, this, [[0, 8]]);
          }));
        }
      }, {
        key: "pauseAnuncio",
        value: function pauseAnuncio() {
          this.playAnuncio.unsubscribe();
        }
      }, {
        key: "tipoConexao",
        value: function tipoConexao() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee32() {
            var _this14 = this;

            var handler;
            return _regeneratorRuntime().wrap(function _callee32$(_context32) {
              while (1) switch (_context32.prev = _context32.next) {
                case 0:
                  handler = Network.addListener('networkStatusChange', function (status) {
                    _this14.newStatus = status;
                    console.log(' mudando para conexao  --anuncio.service networkStatusChange = ' + _this14.newStatus.connectionType);
                  });
                  _context32.next = 3;
                  return Network.getStatus();

                case 3:
                  this.newStatus = _context32.sent;
                  console.log(' conexao atual --anuncio.service getStatus = ' + this.newStatus.connectionType);

                case 5:
                case "end":
                  return _context32.stop();
              }
            }, _callee32, this);
          }));
        }
      }, {
        key: "BuscarNovamente",
        value: function BuscarNovamente() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee33() {
            return _regeneratorRuntime().wrap(function _callee33$(_context33) {
              while (1) switch (_context33.prev = _context33.next) {
                case 0:
                  _context33.next = 2;
                  return this.storageService.getTokenStorage('Parceiro');

                case 2:
                  this.tokenStorage = _context33.sent;
                  _context33.next = 5;
                  return this.getPositicao();

                case 5:
                  console.log('novas coordenadas', this.latitude, this.longitude, 'token ', this.tokenStorage);
                  _context33.next = 8;
                  return this.buscaAnuncioArea(this.latitude, this.longitude, this.tokenStorage);

                case 8:
                case "end":
                  return _context33.stop();
              }
            }, _callee33, this);
          }));
        }
      }]);

      return AnuncioService;
    }();

    AnuncioService.ctorParameters = function () {
      return [{
        type: _ionic_native_video_player_ngx__WEBPACK_IMPORTED_MODULE_8__["VideoPlayer"]
      }, {
        type: _ionic_native_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_7__["ScreenOrientation"]
      }, {
        type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_5__["File"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["ModalController"]
      }, {
        type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"]
      }, {
        type: _http_http_service__WEBPACK_IMPORTED_MODULE_13__["HttpService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["NavController"]
      }, {
        type: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_4__["Downloader"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["Platform"]
      }, {
        type: _ionic_native_background_mode_ngx__WEBPACK_IMPORTED_MODULE_3__["BackgroundMode"]
      }, {
        type: _storage_storage_service__WEBPACK_IMPORTED_MODULE_14__["StorageService"]
      }, {
        type: _generica_generica_service__WEBPACK_IMPORTED_MODULE_12__["GenericaService"]
      }];
    };

    AnuncioService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], AnuncioService);
    /***/
  },

  /***/
  "./src/app/services/generica/generica.service.ts":
  /*!*******************************************************!*\
    !*** ./src/app/services/generica/generica.service.ts ***!
    \*******************************************************/

  /*! exports provided: GenericaService */

  /***/
  function srcAppServicesGenericaGenericaServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GenericaService", function () {
      return GenericaService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

    var GenericaService = /*#__PURE__*/function () {
      function GenericaService(toastController, alertCtrl, loadingController) {
        _classCallCheck(this, GenericaService);

        this.toastController = toastController;
        this.alertCtrl = alertCtrl;
        this.loadingController = loadingController;
      } // mensagem de alerta atributos facultativo


      _createClass(GenericaService, [{
        key: "alert",
        value: function alert(options) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee34() {
            var alert;
            return _regeneratorRuntime().wrap(function _callee34$(_context34) {
              while (1) switch (_context34.prev = _context34.next) {
                case 0:
                  _context34.next = 2;
                  return this.alertCtrl.create(options);

                case 2:
                  alert = _context34.sent;
                  _context34.next = 5;
                  return alert.present();

                case 5:
                  return _context34.abrupt("return", alert);

                case 6:
                case "end":
                  return _context34.stop();
              }
            }, _callee34, this);
          }));
        } // loading processos

      }, {
        key: "loading",
        value: function loading(options) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee35() {
            var loading;
            return _regeneratorRuntime().wrap(function _callee35$(_context35) {
              while (1) switch (_context35.prev = _context35.next) {
                case 0:
                  _context35.next = 2;
                  return this.loadingController.create(Object.assign({
                    message: 'Carregando...'
                  }, options));

                case 2:
                  loading = _context35.sent;
                  _context35.next = 5;
                  return loading.present();

                case 5:
                  return _context35.abrupt("return", loading);

                case 6:
                case "end":
                  return _context35.stop();
              }
            }, _callee35, this);
          }));
        } // Toast processos

      }, {
        key: "toast",
        value: function toast(options) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee36() {
            var toast;
            return _regeneratorRuntime().wrap(function _callee36$(_context36) {
              while (1) switch (_context36.prev = _context36.next) {
                case 0:
                  _context36.next = 2;
                  return this.toastController.create(Object.assign({
                    position: 'bottom',
                    duration: 3000,
                    keyboardClose: true
                  }, options));

                case 2:
                  toast = _context36.sent;
                  _context36.next = 5;
                  return toast.present();

                case 5:
                  return _context36.abrupt("return", toast);

                case 6:
                case "end":
                  return _context36.stop();
              }
            }, _callee36, this);
          }));
        }
      }]);

      return GenericaService;
    }();

    GenericaService.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }];
    };

    GenericaService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], GenericaService);
    /***/
  },

  /***/
  "./src/app/services/http/http.service.ts":
  /*!***********************************************!*\
    !*** ./src/app/services/http/http.service.ts ***!
    \***********************************************/

  /*! exports provided: HttpService */

  /***/
  function srcAppServicesHttpHttpServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HttpService", function () {
      return HttpService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var HttpService = /*#__PURE__*/function () {
      function HttpService(http) {
        _classCallCheck(this, HttpService);

        this.http = http;
      } // get dados


      _createClass(HttpService, [{
        key: "getUsuario",
        value: function getUsuario(token) {
          var url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/parceiro/getusuario/' + token;
          console.log(url);
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
          return this.http.get(url, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(this.handleError));
          return;
        } // get categoria parceiro

      }, {
        key: "getCategoriasAnuncios",
        value: function getCategoriasAnuncios() {
          var url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/parceiro/getCategoriasAnuncios/';
          console.log(url);
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
          return this.http.get(url, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(this.handleError));
          return;
        } // busca pra fazer downloader

      }, {
        key: "getArquivosAnuncios",
        value: function getArquivosAnuncios(latitude, longitude, tokenParceiro) {
          var url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + 'parceiro/getArquivosAnuncios/' + latitude + '/' + longitude + '/' + tokenParceiro;
          console.log(url);
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
          return this.http.get(url, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(this.handleError));
          return;
        } // busca pra fazer downloader

      }, {
        key: "cadastraLicenca",
        value: function cadastraLicenca(tokenParceiro, codLicenca) {
          var url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + 'parceiro/cadastraLicenca/' + tokenParceiro + '/' + codLicenca;
          console.log(url);
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
          return this.http.get(url, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(this.handleError)); // return;
        } // verifica licença

      }, {
        key: "getVerificaLicenca",
        value: function getVerificaLicenca(tokenParceiro, tokenLicenca) {
          var url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + 'parceiro/verificalicenca/' + tokenParceiro + '/' + tokenLicenca;
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
          return this.http.get(url, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(this.handleError));
        } // busca vericar se ja tei celular

      }, {
        key: "getAnuncios",
        value: function getAnuncios(latitude, longitude, tokenParceiro) {
          var url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + 'parceiro/getAnuncios/' + latitude + '/' + longitude + '/' + tokenParceiro;
          console.log(url);
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
          return this.http.get(url, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(this.handleError));
        } // Busca somente imagens

      }, {
        key: "getAnunciosDadosMoveis",
        value: function getAnunciosDadosMoveis(latitude, longitude, tokenParceiro) {
          var url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + 'parceiro/getAnunciosDadosMoveis/' + latitude + '/' + longitude + '/' + tokenParceiro;
          console.log(url);
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
          return this.http.get(url, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(this.handleError));
        } // Busca somente  anuncio empresa pubnews

      }, {
        key: "getAnunciosPubNews",
        value: function getAnunciosPubNews() {
          var url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + 'parceiro/getAnunciosPubNews/';
          console.log(url);
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
          return this.http.get(url, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(this.handleError));
        } // get busca qnt anuncios exibidos

      }, {
        key: "armazenaExibicaoAnuncio",
        value: function armazenaExibicaoAnuncio(latitude, longitude, tokenParceiro, tokenAnuncio, tokenLicenca) {
          var url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + 'parceiro/armazenaExibicaoAnuncio/' + latitude + '/' + longitude + '/' + tokenParceiro + '/' + tokenAnuncio + '/' + tokenLicenca;
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
          return this.http.get(url, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(this.handleError));
        } // get busca saldos anuncios exibidos

      }, {
        key: "getSaldoParceiro",
        value: function getSaldoParceiro(tokenParceiro) {
          var url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + 'parceiro/getSaldoParceiro/' + tokenParceiro; // console.log(url);

          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
          return this.http.get(url, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(this.extractData), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(this.handleError));
          return;
        } // get generico
        // get(url) {
        //     const url = environment.apiUrl + `${metodo}/${tokenParceiro}`;
        //     console.log(url);
        //     const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
        //
        //     return this.http.get(url, httpOptions)
        //         .pipe(map(this.extractData),
        //             catchError(this.handleError));
        // }
        // cadastrar , login  e Recupera senha via post

      }, {
        key: "post",
        value: function post(serviceName, data) {
          var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]();
          var options = {
            header: headers,
            withCredentials: false
          }; // criando url global em environment

          var url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + serviceName;
          console.log('service --  HttpService ' + url);
          return this.http.post(url, JSON.stringify(data), options);
        } // exibindo erros personalizados

      }, {
        key: "handleError",
        value: function handleError(error) {
          if (error.error instanceof ErrorEvent) {
            console.error('Erro Ocorrido em :', error.error.message);
          } else {
            console.error("Codigo Retornado Backend ".concat(error.status, ", ") + "body era: ".concat(error.error));
          }

          return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])('Erros inesperado tente novamente.');
        }
      }, {
        key: "extractData",
        value: function extractData(res) {
          var body = res;
          return body || {};
        }
      }]);

      return HttpService;
    }();

    HttpService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]
      }];
    };

    HttpService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], HttpService);
    /***/
  },

  /***/
  "./src/app/services/storage/storage.service.ts":
  /*!*****************************************************!*\
    !*** ./src/app/services/storage/storage.service.ts ***!
    \*****************************************************/

  /*! exports provided: StorageService */

  /***/
  function srcAppServicesStorageStorageServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StorageService", function () {
      return StorageService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @capacitor/core */
    "./node_modules/@capacitor/core/dist/esm/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _http_http_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../http/http.service */
    "./src/app/services/http/http.service.ts");

    var Storage = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"].Storage;

    var StorageService = /*#__PURE__*/function () {
      function StorageService(navCtrl, httpService) {
        _classCallCheck(this, StorageService);

        this.navCtrl = navCtrl;
        this.httpService = httpService; // dados parceiros

        this.nomeParceiro = null;
        this.cpfParceiro = null;
        this.celularParceiro = null;
        this.emailParceiro = null;
        this.fotoParceiro = null;
        this.tipoParceiro = null;
        this.parceiroStorage = null; // vem dados storage

        this.timeImage = null;
      }

      _createClass(StorageService, [{
        key: "getParaAnuncio",
        value: function getParaAnuncio() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee37() {
            var dado, dados;
            return _regeneratorRuntime().wrap(function _callee37$(_context37) {
              while (1) switch (_context37.prev = _context37.next) {
                case 0:
                  _context37.next = 2;
                  return Storage.get({
                    key: 'paraAnuncio'
                  });

                case 2:
                  dado = _context37.sent;
                  dados = JSON.parse(dado.value);

                  if (!dados) {
                    _context37.next = 8;
                    break;
                  }

                  return _context37.abrupt("return", dados.paraAnuncio);

                case 8:
                  console.log('nao existe  anuncio celular');
                  return _context37.abrupt("return", false);

                case 10:
                case "end":
                  return _context37.stop();
              }
            }, _callee37);
          }));
        }
      }, {
        key: "setStorageAnuncio",
        value: function setStorageAnuncio(valor) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee38() {
            return _regeneratorRuntime().wrap(function _callee38$(_context38) {
              while (1) switch (_context38.prev = _context38.next) {
                case 0:
                  _context38.next = 2;
                  return Storage.set({
                    key: 'paraAnuncio',
                    value: JSON.stringify({
                      paraAnuncio: valor
                    })
                  });

                case 2:
                case "end":
                  return _context38.stop();
              }
            }, _callee38);
          }));
        }
      }, {
        key: "setAnunciosStorage",
        value: function setAnunciosStorage(keyAnuncio, arquivoAnuncio, tipo, tokenAnuncio, time, tipoAnuncio) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee39() {
            return _regeneratorRuntime().wrap(function _callee39$(_context39) {
              while (1) switch (_context39.prev = _context39.next) {
                case 0:
                  _context39.next = 2;
                  return Storage.set({
                    key: keyAnuncio,
                    value: JSON.stringify({
                      arquivo: arquivoAnuncio,
                      tipoArquivo: tipo,
                      token: tokenAnuncio,
                      tempo_duracao: time,
                      tipo_anuncio: tipoAnuncio
                    })
                  });

                case 2:
                case "end":
                  return _context39.stop();
              }
            }, _callee39);
          }));
        } // pegando key e value

      }, {
        key: "getItemAnuncio",
        value: function getItemAnuncio(chave) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee40() {
            var dado, dados;
            return _regeneratorRuntime().wrap(function _callee40$(_context40) {
              while (1) switch (_context40.prev = _context40.next) {
                case 0:
                  _context40.next = 2;
                  return Storage.get({
                    key: chave
                  });

                case 2:
                  dado = _context40.sent;
                  dados = JSON.parse(dado.value);

                  if (!dados) {
                    _context40.next = 8;
                    break;
                  }

                  return _context40.abrupt("return", dados.arquivo);

                case 8:
                  console.log('nao existe  anuncio celular');
                  return _context40.abrupt("return", false);

                case 10:
                case "end":
                  return _context40.stop();
              }
            }, _callee40);
          }));
        } // pegando somente itens tenha tipoAnuncio storage

      }, {
        key: "vericaArquivoStorage",
        value: function vericaArquivoStorage(arquivo) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee41() {
            var arquivoStorage;
            return _regeneratorRuntime().wrap(function _callee41$(_context41) {
              while (1) switch (_context41.prev = _context41.next) {
                case 0:
                  _context41.next = 2;
                  return this.getItemAnuncio(arquivo);

                case 2:
                  arquivoStorage = _context41.sent;

                  if (!arquivoStorage) {
                    _context41.next = 7;
                    break;
                  }

                  return _context41.abrupt("return", arquivoStorage);

                case 7:
                  return _context41.abrupt("return", false);

                case 8:
                case "end":
                  return _context41.stop();
              }
            }, _callee41, this);
          }));
        }
      }, {
        key: "getTokenLicenca",
        value: function getTokenLicenca(chave) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee42() {
            var dado, dados;
            return _regeneratorRuntime().wrap(function _callee42$(_context42) {
              while (1) switch (_context42.prev = _context42.next) {
                case 0:
                  _context42.next = 2;
                  return Storage.get({
                    key: chave
                  });

                case 2:
                  dado = _context42.sent;
                  dados = JSON.parse(dado.value);

                  if (!dados) {
                    _context42.next = 8;
                    break;
                  }

                  return _context42.abrupt("return", dados.licenca);

                case 8:
                  return _context42.abrupt("return", false);

                case 9:
                case "end":
                  return _context42.stop();
              }
            }, _callee42);
          }));
        }
      }, {
        key: "setParceiroStorage",
        value: function setParceiroStorage(tokenParceiro, nomeParceiro, cpfParceiro, celularParceiro, emailParceiro, fotoParceiro, tipoParceiro) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee43() {
            return _regeneratorRuntime().wrap(function _callee43$(_context43) {
              while (1) switch (_context43.prev = _context43.next) {
                case 0:
                  _context43.next = 2;
                  return Storage.set({
                    key: 'Parceiro',
                    value: JSON.stringify({
                      token: tokenParceiro,
                      nome: nomeParceiro,
                      cpf: cpfParceiro,
                      celular: celularParceiro,
                      email: emailParceiro,
                      foto: fotoParceiro,
                      tipo_Parceiro: tipoParceiro
                    })
                  });

                case 2:
                case "end":
                  return _context43.stop();
              }
            }, _callee43);
          }));
        }
      }, {
        key: "setLicencaStorage",
        value: function setLicencaStorage(tokenLicenca, token) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee44() {
            return _regeneratorRuntime().wrap(function _callee44$(_context44) {
              while (1) switch (_context44.prev = _context44.next) {
                case 0:
                  _context44.next = 2;
                  return Storage.set({
                    key: tokenLicenca,
                    value: JSON.stringify({
                      licenca: token
                    })
                  });

                case 2:
                case "end":
                  return _context44.stop();
              }
            }, _callee44);
          }));
        } // buscando dados array somente token

      }, {
        key: "getTokenStorage",
        value: function getTokenStorage(chave) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee45() {
            var dado, dados;
            return _regeneratorRuntime().wrap(function _callee45$(_context45) {
              while (1) switch (_context45.prev = _context45.next) {
                case 0:
                  _context45.next = 2;
                  return Storage.get({
                    key: chave
                  });

                case 2:
                  dado = _context45.sent;
                  dados = JSON.parse(dado.value);

                  if (!dados) {
                    _context45.next = 8;
                    break;
                  }

                  return _context45.abrupt("return", dados.token);

                case 8:
                  console.log('sem token valido');

                case 9:
                case "end":
                  return _context45.stop();
              }
            }, _callee45);
          }));
        }
      }, {
        key: "getParceiroStorage",
        value: function getParceiroStorage(chave) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee46() {
            var dado, dados;
            return _regeneratorRuntime().wrap(function _callee46$(_context46) {
              while (1) switch (_context46.prev = _context46.next) {
                case 0:
                  _context46.next = 2;
                  return Storage.get({
                    key: chave
                  });

                case 2:
                  dado = _context46.sent;
                  dados = JSON.parse(dado.value);

                  if (!dados) {
                    _context46.next = 8;
                    break;
                  }

                  return _context46.abrupt("return", dados);

                case 8:
                  console.log('nao exite dados offline'); // todo retira dps  qnd volta back
                  // await this.voltaPagina('login');

                  return _context46.abrupt("return", false);

                case 10:
                case "end":
                  return _context46.stop();
              }
            }, _callee46);
          }));
        } // set dado individual

      }, {
        key: "setItem",
        value: function setItem(dadoKey, dadoValue) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee47() {
            return _regeneratorRuntime().wrap(function _callee47$(_context47) {
              while (1) switch (_context47.prev = _context47.next) {
                case 0:
                  _context47.next = 2;
                  return Storage.set({
                    key: dadoKey,
                    value: dadoValue
                  });

                case 2:
                case "end":
                  return _context47.stop();
              }
            }, _callee47);
          }));
        } // pegando dado

      }, {
        key: "getItem",
        value: function getItem(dadoKey) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee48() {
            var dados, dadosStorage;
            return _regeneratorRuntime().wrap(function _callee48$(_context48) {
              while (1) switch (_context48.prev = _context48.next) {
                case 0:
                  _context48.next = 2;
                  return Storage.get({
                    key: dadoKey
                  });

                case 2:
                  dados = _context48.sent;
                  dadosStorage = dados.value;
                  return _context48.abrupt("return", dadosStorage);

                case 5:
                case "end":
                  return _context48.stop();
              }
            }, _callee48);
          }));
        } // removendo dado

      }, {
        key: "removeItem",
        value: function removeItem(dado) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee49() {
            return _regeneratorRuntime().wrap(function _callee49$(_context49) {
              while (1) switch (_context49.prev = _context49.next) {
                case 0:
                  _context49.next = 2;
                  return Storage.remove({
                    key: dado
                  });

                case 2:
                case "end":
                  return _context49.stop();
              }
            }, _callee49);
          }));
        } // limpando  Tudo Storage

      }, {
        key: "clearStorage",
        value: function clearStorage() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee50() {
            return _regeneratorRuntime().wrap(function _callee50$(_context50) {
              while (1) switch (_context50.prev = _context50.next) {
                case 0:
                  _context50.next = 2;
                  return Storage.clear();

                case 2:
                case "end":
                  return _context50.stop();
              }
            }, _callee50);
          }));
        } // anuncio exibido json

      }, {
        key: "getAnuncios",
        value: function getAnuncios(lat, _long5, token) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee52() {
            var _this15 = this;

            return _regeneratorRuntime().wrap(function _callee52$(_context52) {
              while (1) switch (_context52.prev = _context52.next) {
                case 0:
                  this.httpService.getAnuncios(lat, _long5, token).subscribe(function (data) {
                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this15, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee51() {
                      return _regeneratorRuntime().wrap(function _callee51$(_context51) {
                        while (1) switch (_context51.prev = _context51.next) {
                          case 0:
                            console.log(data);

                          case 1:
                          case "end":
                            return _context51.stop();
                        }
                      }, _callee51);
                    }));
                  }, function (err) {
                    return console.log(err);
                  });

                case 1:
                case "end":
                  return _context52.stop();
              }
            }, _callee52, this);
          }));
        } // pegando dado somente pubnews tipo anuncio 2 uso rede movel

      }, {
        key: "getItemPubnews",
        value: function getItemPubnews(dadoKey) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee53() {
            var dados, dadosStorage;
            return _regeneratorRuntime().wrap(function _callee53$(_context53) {
              while (1) switch (_context53.prev = _context53.next) {
                case 0:
                  _context53.next = 2;
                  return Storage.get({
                    key: dadoKey
                  });

                case 2:
                  dados = _context53.sent;
                  dadosStorage = JSON.parse(dados.value);
                  console.log(dadosStorage.tipo_anuncio);
                  return _context53.abrupt("return", dadosStorage.tipo_anuncio);

                case 6:
                case "end":
                  return _context53.stop();
              }
            }, _callee53);
          }));
        }
      }, {
        key: "keys",
        value: function keys() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee54() {
            var _yield$Storage$keys2, keys, i, teste;

            return _regeneratorRuntime().wrap(function _callee54$(_context54) {
              while (1) switch (_context54.prev = _context54.next) {
                case 0:
                  _context54.next = 2;
                  return Storage.keys();

                case 2:
                  _yield$Storage$keys2 = _context54.sent;
                  keys = _yield$Storage$keys2.keys;
                  i = 0;

                case 5:
                  if (!(i < Object.keys.length)) {
                    _context54.next = 13;
                    break;
                  }

                  _context54.next = 8;
                  return this.getItem(keys[i]);

                case 8:
                  teste = _context54.sent;
                  console.log("keys aqui ", teste);

                case 10:
                  i++;
                  _context54.next = 5;
                  break;

                case 13:
                case "end":
                  return _context54.stop();
              }
            }, _callee54, this);
          }));
        } // sair aplicativo

      }, {
        key: "logout",
        value: function logout() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee55() {
            return _regeneratorRuntime().wrap(function _callee55$(_context55) {
              while (1) switch (_context55.prev = _context55.next) {
                case 0:
                  _context55.next = 2;
                  return Storage.clear();

                case 2:
                case "end":
                  return _context55.stop();
              }
            }, _callee55);
          }));
        } // ###########################################################################################################
        // desmonta pilha e volta  pagina

      }, {
        key: "voltaPagina",
        value: function voltaPagina(pagina) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee56() {
            return _regeneratorRuntime().wrap(function _callee56$(_context56) {
              while (1) switch (_context56.prev = _context56.next) {
                case 0:
                  _context56.next = 2;
                  return this.navCtrl.navigateBack(pagina);

                case 2:
                case "end":
                  return _context56.stop();
              }
            }, _callee56, this);
          }));
        } // desmonta pilha

      }, {
        key: "popPagina",
        value: function popPagina(pagina) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee57() {
            return _regeneratorRuntime().wrap(function _callee57$(_context57) {
              while (1) switch (_context57.prev = _context57.next) {
                case 0:
                  _context57.next = 2;
                  return this.navCtrl.navigateBack(pagina);

                case 2:
                case "end":
                  return _context57.stop();
              }
            }, _callee57, this);
          }));
        } // monta pilha e vai proxima pagina

      }, {
        key: "irPagina",
        value: function irPagina(pagina) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee58() {
            return _regeneratorRuntime().wrap(function _callee58$(_context58) {
              while (1) switch (_context58.prev = _context58.next) {
                case 0:
                  _context58.next = 2;
                  return this.navCtrl.navigateForward(pagina);

                case 2:
                case "end":
                  return _context58.stop();
              }
            }, _callee58, this);
          }));
        }
      }, {
        key: "Pop",
        value: function Pop() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee59() {
            return _regeneratorRuntime().wrap(function _callee59$(_context59) {
              while (1) switch (_context59.prev = _context59.next) {
                case 0:
                  _context59.next = 2;
                  return this.navCtrl.pop();

                case 2:
                case "end":
                  return _context59.stop();
              }
            }, _callee59, this);
          }));
        } //  vai  pagina root

      }, {
        key: "rootPagina",
        value: function rootPagina(pagina) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee60() {
            return _regeneratorRuntime().wrap(function _callee60$(_context60) {
              while (1) switch (_context60.prev = _context60.next) {
                case 0:
                  _context60.next = 2;
                  return this.navCtrl.navigateRoot(pagina);

                case 2:
                  return _context60.abrupt("return");

                case 3:
                case "end":
                  return _context60.stop();
              }
            }, _callee60, this);
          }));
        }
      }]);

      return StorageService;
    }();

    StorageService.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _http_http_service__WEBPACK_IMPORTED_MODULE_4__["HttpService"]
      }];
    };

    StorageService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], StorageService);
    /***/
  },

  /***/
  "./src/app/services/util/util.service.ts":
  /*!***********************************************!*\
    !*** ./src/app/services/util/util.service.ts ***!
    \***********************************************/

  /*! exports provided: UtilService */

  /***/
  function srcAppServicesUtilUtilServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UtilService", function () {
      return UtilService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @capacitor/core */
    "./node_modules/@capacitor/core/dist/esm/index.js");
    /* harmony import */


    var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic-native/android-permissions/ngx */
    "./node_modules/@ionic-native/android-permissions/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_background_mode_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic-native/background-mode/ngx */
    "./node_modules/@ionic-native/background-mode/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _anuncio_anuncio_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../anuncio/anuncio.service */
    "./src/app/services/anuncio/anuncio.service.ts");
    /* harmony import */


    var _generica_generica_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../generica/generica.service */
    "./src/app/services/generica/generica.service.ts");
    /* harmony import */


    var _http_http_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../http/http.service */
    "./src/app/services/http/http.service.ts");
    /* harmony import */


    var _storage_storage_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../storage/storage.service */
    "./src/app/services/storage/storage.service.ts");

    var _capacitor_core__WEBP2 = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"],
        Network = _capacitor_core__WEBP2.Network,
        Storage = _capacitor_core__WEBP2.Storage,
        Geolocation = _capacitor_core__WEBP2.Geolocation;

    var UtilService = /*#__PURE__*/function () {
      function UtilService(navCtrl, backgroundMode, httpService, storageService, androidPermissions, anuncioService, genericaService) {
        _classCallCheck(this, UtilService);

        this.navCtrl = navCtrl;
        this.backgroundMode = backgroundMode;
        this.httpService = httpService;
        this.storageService = storageService;
        this.androidPermissions = androidPermissions;
        this.anuncioService = anuncioService;
        this.genericaService = genericaService;
      } // guarda route usuario autenticados token storage armazenado


      _createClass(UtilService, [{
        key: "canActivate",
        value: function canActivate(route) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee61() {
            return _regeneratorRuntime().wrap(function _callee61$(_context61) {
              while (1) switch (_context61.prev = _context61.next) {
                case 0:
                  // this.storageService.tokenStorage = await this.storageService.getTokenStorage('Parceiro');
                  this.storageService.tokenStorage = true;

                  if (!this.storageService.tokenStorage) {
                    _context61.next = 5;
                    break;
                  }

                  return _context61.abrupt("return", true);

                case 5:
                  _context61.next = 7;
                  return this.storageService.rootPagina("login");

                case 7:
                  return _context61.abrupt("return", false);

                case 8:
                case "end":
                  return _context61.stop();
              }
            }, _callee61, this);
          }));
        } ///// Storage /////////////////////////
        // setando dados array
        // geoloaction Com Capacitor

      }, {
        key: "getPositicao",
        value: function getPositicao() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee63() {
            var _this16 = this;

            var coordinates;
            return _regeneratorRuntime().wrap(function _callee63$(_context63) {
              while (1) switch (_context63.prev = _context63.next) {
                case 0:
                  _context63.prev = 0;
                  _context63.next = 3;
                  return Geolocation.getCurrentPosition({
                    enableHighAccuracy: true
                  });

                case 3:
                  coordinates = _context63.sent;
                  this.latitude = coordinates.coords.latitude;
                  this.longitude = coordinates.coords.longitude; // return coordinates

                  _context63.next = 12;
                  break;

                case 8:
                  _context63.prev = 8;
                  _context63.t0 = _context63["catch"](0);
                  console.log('erro a busca coordenadas,', _context63.t0);

                  if (_context63.t0.code === 3) {
                    this.getPositicao().then(function () {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this16, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee62() {
                        return _regeneratorRuntime().wrap(function _callee62$(_context62) {
                          while (1) switch (_context62.prev = _context62.next) {
                            case 0:
                              _context62.next = 2;
                              return this.solicitaPermissao();

                            case 2:
                            case "end":
                              return _context62.stop();
                          }
                        }, _callee62, this);
                      }));
                    });
                  }

                case 12:
                case "end":
                  return _context63.stop();
              }
            }, _callee63, this, [[0, 8]]);
          }));
        }
      }, {
        key: "tipoConexao",
        value: function tipoConexao() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee66() {
            var _this17 = this;

            var handler, status;
            return _regeneratorRuntime().wrap(function _callee66$(_context66) {
              while (1) switch (_context66.prev = _context66.next) {
                case 0:
                  handler = Network.addListener('networkStatusChange', function (status) {
                    console.log(status.connectionType); // if (status.connectionType === 'wifi') {

                    console.log('rede mudando para wi fi');

                    _this17.getPositicao().then(function () {
                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this17, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee64() {
                        return _regeneratorRuntime().wrap(function _callee64$(_context64) {
                          while (1) switch (_context64.prev = _context64.next) {
                            case 0:
                              console.log('coods', this.latitude, this.longitude);
                              _context64.next = 3;
                              return this.anuncioService.AnunciosParceiroDownloder(this.latitude, this.longitude, this.storageService.tokenStorage);

                            case 3:
                            case "end":
                              return _context64.stop();
                          }
                        }, _callee64, this);
                      }));
                    })["catch"](function () {
                      return console.log('error ao conectar wi fi  ');
                    });
                  });
                  _context66.next = 3;
                  return Network.getStatus();

                case 3:
                  status = _context66.sent;
                  // if (status.connectionType === 'wifi' || status.connectionType === 'cellular') {
                  console.log(' conexao atual  = ' + status.connectionType);
                  this.getPositicao().then(function () {
                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this17, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee65() {
                      return _regeneratorRuntime().wrap(function _callee65$(_context65) {
                        while (1) switch (_context65.prev = _context65.next) {
                          case 0:
                            _context65.next = 2;
                            return this.anuncioService.AnunciosParceiroDownloder(this.latitude, this.longitude, this.storageService.tokenStorage);

                          case 2:
                            console.log('coords getstatus', this.latitude, this.longitude);

                          case 3:
                          case "end":
                            return _context65.stop();
                        }
                      }, _callee65, this);
                    }));
                  })["catch"](function () {
                    return console.log('error ao conectar wi fi  ');
                  }); // }

                case 6:
                case "end":
                  return _context66.stop();
              }
            }, _callee66, this);
          }));
        }
      }, {
        key: "verificaLicenca",
        value: function verificaLicenca() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee67() {
            var _this18 = this;

            var licenca;
            return _regeneratorRuntime().wrap(function _callee67$(_context67) {
              while (1) switch (_context67.prev = _context67.next) {
                case 0:
                  _context67.next = 2;
                  return this.storageService.getTokenStorage('Parceiro');

                case 2:
                  this.storageService.tokenStorage = _context67.sent;
                  _context67.next = 5;
                  return this.storageService.getTokenLicenca('tokenLicenca');

                case 5:
                  licenca = _context67.sent;
                  this.httpService.getVerificaLicenca(this.storageService.tokenStorage, licenca).subscribe(function (data) {
                    if (data.status === false) {
                      console.log('sem licenca');

                      _this18.storageService.voltaPagina('licenca');
                    } else if (data.status === true || _this18.storageService.tokenStorage) {
                      console.log('autenticado  com token e licenca');

                      _this18.storageService.irPagina('dashboard');
                    } else {
                      console.log('usuario sem acesso');

                      _this18.storageService.voltaPagina('login');
                    }
                  });

                case 7:
                case "end":
                  return _context67.stop();
              }
            }, _callee67, this);
          }));
        } // ativando backgroundMode

      }, {
        key: "nativeSegundoPlano",
        value: function nativeSegundoPlano() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee68() {
            return _regeneratorRuntime().wrap(function _callee68$(_context68) {
              while (1) switch (_context68.prev = _context68.next) {
                case 0:
                  this.backgroundMode.enable();
                  this.backgroundMode.on('activate').subscribe(function () {
                    var array = [1, 2];

                    for (var _i = 0, _array = array; _i < _array.length; _i++) {
                      var a = _array[_i];
                      console.log(a);
                    }
                  });

                case 2:
                case "end":
                  return _context68.stop();
              }
            }, _callee68, this);
          }));
        }
      }, {
        key: "solicitaPermissao",
        value: function solicitaPermissao() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/_regeneratorRuntime().mark(function _callee69() {
            var _this19 = this;

            var permissao1, permissao2;
            return _regeneratorRuntime().wrap(function _callee69$(_context69) {
              while (1) switch (_context69.prev = _context69.next) {
                case 0:
                  permissao1 = this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(function (result) {
                    _this19.androidPermissions.requestPermission(_this19.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
                  }, function (err) {
                    return _this19.androidPermissions.requestPermission(_this19.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
                  });
                  permissao2 = this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(function (result) {
                    _this19.androidPermissions.requestPermission(_this19.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION);
                  }, function (err) {
                    return _this19.androidPermissions.requestPermission(_this19.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION);
                  });
                  Promise.all([permissao1, permissao2]).then();

                case 3:
                case "end":
                  return _context69.stop();
              }
            }, _callee69, this);
          }));
        }
      }]);

      return UtilService;
    }();

    UtilService.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
      }, {
        type: _ionic_native_background_mode_ngx__WEBPACK_IMPORTED_MODULE_4__["BackgroundMode"]
      }, {
        type: _http_http_service__WEBPACK_IMPORTED_MODULE_8__["HttpService"]
      }, {
        type: _storage_storage_service__WEBPACK_IMPORTED_MODULE_9__["StorageService"]
      }, {
        type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_3__["AndroidPermissions"]
      }, {
        type: _anuncio_anuncio_service__WEBPACK_IMPORTED_MODULE_6__["AnuncioService"]
      }, {
        type: _generica_generica_service__WEBPACK_IMPORTED_MODULE_7__["GenericaService"]
      }];
    };

    UtilService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], UtilService);
    /***/
  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    }); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    var environment = {
      production: false,
      // apiUrl: 'https://www.pubnews.com.br/sistema/',
      // apiUrl: 'https://www.masmidia.net/clientes/pubnews/',
      apiUrl: 'https://app.publienews.com.br/',
      apiUrlArquivo: 'https://app.publienews.com.br/uploads/',
      // apiUrl: 'https://pubnews.sitehomeweb.com.br/',
      storagePatch: 'file:///storage/emulated/0/Pubnews/',
      AndroidDataPatch: 'file:///storage/emulated/0/Android/data/com.masmidia.myionicfacebook/files/Pubnews/',
      storage: 'file:///storage/emulated/0/'
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser-dynamic */
    "./node_modules/@angular/platform-browser-dynamic/__ivy_ngcc__/fesm2015/platform-browser-dynamic.js");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
    }

    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
      return console.log(err);
    });
    /***/
  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! C:\WS\Pubnews\pubnews-tablet\src\main.ts */
    "./src/main.ts");
    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map